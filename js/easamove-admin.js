/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function ajaxcalls(base_url, cont, datas) {
    $.ajax({
        type: "POST",
        url: base_url + "master/" + cont + "/insert",
        data: datas,
        success: function (data) {
            alert('Saved Successfully');
        }
    });
}
function getCountry(base_url, cont) {
    $.ajax({
        type: "POST",
        url: base_url + "master/" + cont + "/getCountry",
        success: function (data) {
            $('.data-append').html(data);
            var $table = $("#datatb").DataTable({
                bRetrieve: true
            });
            $("input[type='search']").addClass('form-control');
            $("select[name='datatb_length']").addClass('form-control');
        }
    });
}
function getState(base_url, cont) {
    $.ajax({
        type: "POST",
        url: base_url + "master/" + cont + "/getState",
        success: function (data) {
            $('.data-append').html(data);
            var $table = $("#datatb").DataTable({
                bRetrieve: true
            });
            $("input[type='search']").addClass('form-control');
            $("select[name='datatb_length']").addClass('form-control');
        }
    });
}
function getCity(base_url, cont) {
    $.ajax({
        type: "POST",
        url: base_url + "master/" + cont + "/getCity",
        success: function (data) {
            $('.data-append').html(data);
            var $table = $("#datatb").DataTable({
                bRetrieve: true
            });
            $("input[type='search']").addClass('form-control');
            $("select[name='datatb_length']").addClass('form-control');
        }
    });
}

function getEnquiryType(base_url, cont) {
    $.ajax({
        type: "POST",
        url: base_url + "master/" + cont + "/getEnquirytype",
        success: function (data) {
            $('.data-append').html(data);
            var $table = $("#datatb").DataTable({
                bRetrieve: true
            });
            $("input[type='search']").addClass('form-control');
            $("select[name='datatb_length']").addClass('form-control');
        }
    });
}
function getAlertType(base_url, cont) {
    $.ajax({
        type: "POST",
        url: base_url + "master/" + cont + "/getAlerttype",
        success: function (data) {
            $('.data-append').html(data);
            var $table = $("#datatb").DataTable({
                bRetrieve: true
            });
            $("input[type='search']").addClass('form-control');
            $("select[name='datatb_length']").addClass('form-control');
        }
    });
}
function getDecorativeCondition(base_url, cont) {
    $.ajax({
        type: "POST",
        url: base_url + "master/" + cont + "/getDecorativecondition",
        success: function (data) {
            $('.data-append').html(data);
            var $table = $("#datatb").DataTable({
                bRetrieve: true
            });
            $("input[type='search']").addClass('form-control');
            $("select[name='datatb_length']").addClass('form-control');
        }
    });
}

function getParkingType(base_url, cont) {
    $.ajax({
        type: "POST",
        url: base_url + "master/" + cont + "/getParkingtype",
        success: function (data) {
            $('.data-append').html(data);
            var $table = $("#datatb").DataTable({
                bRetrieve: true
            });
            $("input[type='search']").addClass('form-control');
            $("select[name='datatb_length']").addClass('form-control');
        }
    });
}

function getAgentType(base_url, cont) {
    $.ajax({
        type: "POST",
        url: base_url + "master/" + cont + "/getAgenttype",
        success: function (data) {
            $('.data-append').html(data);
            var $table = $("#datatb").DataTable({
                bRetrieve: true
            });
            $("input[type='search']").addClass('form-control');
            $("select[name='datatb_length']").addClass('form-control');
        }
    });
}
function getCentralheatingType(base_url, cont) {
    $.ajax({
        type: "POST",
        url: base_url + "master/" + cont + "/getCentralheatingType",
        success: function (data) {
            $('.data-append').html(data);
            var $table = $("#datatb").DataTable({
                bRetrieve: true
            });
            $("input[type='search']").addClass('form-control');
            $("select[name='datatb_length']").addClass('form-control');
        }
    });
}

//property master

function ajaxcallsproperty(base_url, cont, datas) {
    $.ajax({
        type: "POST",
        url: base_url + "propertymaster/" + cont + "/insert",
        data: datas,
        success: function (data) {
            alert('Saved Successfully');
        }
    });
}


function getSitestatus(base_url, cont) {
    $.ajax({
        type: "POST",
        url: base_url + "propertymaster/" + cont + "/getSitestatus",
        success: function (data) {
            $('.data-append').html(data);
            var $table = $("#datatb").DataTable({
                bRetrieve: true
            });
            $("input[type='search']").addClass('form-control');
            $("select[name='datatb_length']").addClass('form-control');
        }
    });
}

function getListingstatus(base_url, cont) {
    $.ajax({
        type: "POST",
        url: base_url + "propertymaster/" + cont + "/getListingstatus",
        success: function (data) {
            $('.data-append').html(data);
            var $table = $("#datatb").DataTable({
                bRetrieve: true
            });
            $("input[type='search']").addClass('form-control');
            $("select[name='datatb_length']").addClass('form-control');
        }
    });
}
function getCategory(base_url, cont) {
    $.ajax({
        type: "POST",
        url: base_url + "propertymaster/" + cont + "/getCategory",
        success: function (data) {
            $('.data-append').html(data);
            var $table = $("#datatb").DataTable({
                bRetrieve: true
            });
            $("input[type='search']").addClass('form-control');
            $("select[name='datatb_length']").addClass('form-control');
        }
    });
}
function getFeatures(base_url, cont) {
    $.ajax({
        type: "POST",
        url: base_url + "propertymaster/" + cont + "/getFeatures",
        success: function (data) {
            $('.data-append').html(data);
            var $table = $("#datatb").DataTable({
                bRetrieve: true
            });
            $("input[type='search']").addClass('form-control');
            $("select[name='datatb_length']").addClass('form-control');
        }
    });
}

function getPropertytype(base_url, cont) {
    $.ajax({
        type: "POST",
        url: base_url + "propertymaster/" + cont + "/getPropertytype",
        success: function (data) {
            $('.data-append').html(data);
            var $table = $("#datatb").DataTable({
                bRetrieve: true
            });
            $("input[type='search']").addClass('form-control');
            $("select[name='datatb_length']").addClass('form-control');
        }
    });
}

function getPricemodifier(base_url, cont) {
    $.ajax({
        type: "POST",
        url: base_url + "propertymaster/" + cont + "/getPricemodifier",
        success: function (data) {
            $('.data-append').html(data);
            var $table = $("#datatb").DataTable({
                bRetrieve: true
            });
            $("input[type='search']").addClass('form-control');
            $("select[name='datatb_length']").addClass('form-control');
        }
    });
}

function getRentalfrequencies(base_url, cont) {
    $.ajax({
        type: "POST",
        url: base_url + "propertymaster/" + cont + "/getRentalfrequencies",
        success: function (data) {
            $('.data-append').html(data);
            var $table = $("#datatb").DataTable({
                bRetrieve: true
            });
            $("input[type='search']").addClass('form-control');
            $("select[name='datatb_length']").addClass('form-control');
        }
    });
}


function getTenures(base_url, cont) {
    $.ajax({
        type: "POST",
        url: base_url + "propertymaster/" + cont + "/getTenures",
        success: function (data) {
            $('.data-append').html(data);
            var $table = $("#datatb").DataTable({
                bRetrieve: true
            });
            $("input[type='search']").addClass('form-control');
            $("select[name='datatb_length']").addClass('form-control');
        }
    });
}
function getFurnishes(base_url, cont) {
    $.ajax({
        type: "POST",
        url: base_url + "propertymaster/" + cont + "/getFurnishes",
        success: function (data) {
            $('.data-append').html(data);
            var $table = $("#datatb").DataTable({
                bRetrieve: true
            });
            $("input[type='search']").addClass('form-control');
            $("select[name='datatb_length']").addClass('form-control');
        }
    });
}

function getNatureoferrors(base_url, cont) {
    $.ajax({
        type: "POST",
        url: base_url + "propertymaster/" + cont + "/getNatureoferrors",
        success: function (data) {
            $('.data-append').html(data);
            var $table = $("#datatb").DataTable({
                bRetrieve: true
            });
            $("input[type='search']").addClass('form-control');
            $("select[name='datatb_length']").addClass('form-control');
        }
    });
}
function getPropertyrelationships(base_url, cont) {
    $.ajax({
        type: "POST",
        url: base_url + "propertymaster/" + cont + "/getPropertyrelationships",
        success: function (data) {
            $('.data-append').html(data);
            var $table = $("#datatb").DataTable({
                bRetrieve: true
            });
            $("input[type='search']").addClass('form-control');
            $("select[name='datatb_length']").addClass('form-control');
        }
    });
}

function getFeedbackcategory(base_url, cont) {
    $.ajax({
        type: "POST",
        url: base_url + "propertymaster/" + cont + "/getFeedbackcategory",
        success: function (data) {
            $('.data-append').html(data);
            var $table = $("#datatb").DataTable({
                bRetrieve: true
            });
            $("input[type='search']").addClass('form-control');
            $("select[name='datatb_length']").addClass('form-control');
        }
    });
}

function getFeedbacksubcategory(base_url, cont) {
    $.ajax({
        type: "POST",
        url: base_url + "propertymaster/" + cont + "/getFeedbacksubcategory",
        success: function (data) {
            $('.data-append').html(data);
            var $table = $("#datatb").DataTable({
                bRetrieve: true
            });
            $("input[type='search']").addClass('form-control');
            $("select[name='datatb_length']").addClass('form-control');
        }
    });
}


//admin


function ajaxadmin(base_url, cont, datas) {
    $.ajax({
        type: "POST",
        url: base_url + "/" + cont + "/insert",
        data: datas,
        success: function (data) {
            alert('Saved Successfully');
        }
    });
}
function getAdminuser(base_url, cont) {
    $.ajax({
        type: "POST",
        url: base_url +"/" + cont + "/getAdminuser",
        success: function (data) {
            $('.data-append').html(data);
            var $table = $("#datatb").DataTable({
                bRetrieve: true
            });
            $("input[type='search']").addClass('form-control');
            $("select[name='datatb_length']").addClass('form-control');
        }
    });
}
function randanstring(){
        var chars = "ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz";
	var string_length = 8;
	var randomstring = '';
	for (var i=0; i<string_length; i++) {
		var rnum = Math.floor(Math.random() * chars.length);
		randomstring += chars.substring(rnum,rnum+1);
	}
        return randomstring;
    }
$('.btn-danger').click(function(){
    $("input[name='id']").val('0');
            $('form')[0].reset();
            var password=randanstring();
        $("#Password").val(password);
});

function ajaxAgents(base_url, cont, datas) {
     $('.submit').text('Processing');
    $.ajax({
        type: "POST",
        url: base_url + "agent/" + cont + "/insert",
        data: datas,
         mimeType: "multipart/form-data",
        contentType: false,
        cache: false,
       processData: false,
        success: function (data) {
           
            alert('Saved Successfully');
             $("input[name='id']").val('0');
        $('#add-to-listing-partial-form')[0].reset();
        window.location.href="<?php echo base_url();?>agent/agent"; 
        }
    });
}
function getAgent(base_url, cont) {
    $.ajax({
        type: "POST",
        url: base_url +"agent/" + cont + "/getAgent",
        success: function (data) {
            $('.data-append').html(data);
            var $table = $("#datatb").DataTable({
                bRetrieve: true
            });
            $("input[type='search']").addClass('form-control');
            $("select[name='datatb_length']").addClass('form-control');
        }
    });
}