<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once dirname(__FILE__) . '/tcpdf/tcpdf.php';
class Pdf extends TCPDF
{
    function __construct()
    {
        parent::__construct();
    }
     public function Header() {
        // Logo
        $image_file = './images/logo_img.png'; // *** Very IMP: make sure this image is available on given path on your server
        $this->Image($image_file,15,6,'40', '15', 'png', '', 'T', false, 300, 'R', false, false, 0,     false, false, false);
        // Set font
        $this->SetFont('helvetica', 'L', 8);
    
        // Line break
        $this->Ln();        
        $this->Cell(294, 0, 'EASAMOVE PROPERTY', 0, false, 'L', 0, '', 0, false, 'M', 'M');
       
        // We need to adjust the x and y positions of this text ... first two parameters
        
    }

    // Page footer
    public function Footer() {
        // Position at 25 mm from bottom
        $this->SetY(-15);
        // Set font
        $this->SetFont('helvetica', 'I', 8);
        
        $this->Cell(0, 0, 'EASAMOVE', 0, 0, 'C');
        $this->Ln();
        
        // Page number
        $this->Cell(0, 10, 'Page '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
    }
}
/*Author:Tutsway.com */  
/* End of file Pdf.php */
/* Location: ./application/libraries/Pdf.php */