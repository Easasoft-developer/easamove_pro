<?php
//============================================================+
// File name   : example_061.php
// Begin       : 2010-05-24
// Last Update : 2014-01-25
//
// Description : Example 061 for TCPDF class
//               XHTML + CSS
//
// Author: Nicola Asuni
//
// (c) Copyright:
//               Nicola Asuni
//               Tecnick.com LTD
//               www.tecnick.com
//               info@tecnick.com
//============================================================+

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: XHTML + CSS
 * @author Nicola Asuni
 * @since 2010-05-25
 */

// Include the main TCPDF library (search for installation path).
require_once('tcpdf_include.php');

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Nicola Asuni');
$pdf->SetTitle('');
$pdf->SetSubject('TCPDF Tutorial');
$pdf->SetKeywords('Inovice');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
	require_once(dirname(__FILE__).'/lang/eng.php');
	$pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set font
$pdf->SetFont('helvetica', '', 10);

// add a page
$pdf->AddPage();

/* NOTE:
 * *********************************************************
 * You can load external XHTML using :
 *
 * $html = file_get_contents('/path/to/your/file.html');
 *
 * External CSS files will be automatically loaded.
 * Sometimes you need to fix the path of the external CSS.
 * *********************************************************
 */

// define some HTML content with style
$html = '<html xmlns="http://www.w3.org/1999/xhtml" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Zmeco Quotation Rejected</title>
</head>
<body style="font-size:10px;font-family:Arial, Helvetica, sans-serif">
<table width="100%" cellpadding="4" cellspacing="1">
  <tr>
    <td style="vertical-align: top;">AL ZUMORD METAL ENG. LLC<br />
      P.O. Box : 25203 Ind. Area<br />
      No. 12,Near Cricket Stadium,<br />
      Sharjah - U.A.E., </td>
    <td><img src="order.png" width="75"/></td>
	<td align="right">
		<img src="logo.png" alt="logo"/>
	</td>
  </tr>
</table>
  <br/>
  <br/>
  <table width="100%" cellpadding="4" cellspacing="1">
  <tr>
    <td>Customer name<br />
      abc street,<br />
      chennai-India,<br />
      P.O. Box : 600024. </td>
    <td></td>
    <td align="right">
	<table cellpadding="4" cellspacing="1">
	<tr><td>Invoice #</td><td>ZMECO-003</td></tr>
	<tr><td>Date</td><td>26-Jun-2015</td></tr>
	<tr><td><strong>Amount Due</strong></td><td><strong>1000 AED</strong></td></tr>
	</table>
	</td>
  </tr>
</table>
<br/>
<br/>
<table bgcolor="#E1E1E1" width="100%" cellpadding="4" cellspacing="1">
  
    <tr style="color: #fff;">
      <td width="20%" bgcolor="#E1E1E1" style="color: #000000; font-weight: bold; ">ITEM</td>
      <td width="30%" bgcolor="#E1E1E1" style="color: #000000; font-weight: bold; ">DESCRIPTION</td>
      <td width="15%" bgcolor="#E1E1E1" style="color: #000000; font-weight: bold; ">UNIT COST</td>
      <td width="20%" bgcolor="#E1E1E1" style="color: #000000; font-weight: bold; ">QTY</td>
      <td width="15%" bgcolor="#E1E1E1" style="color: #000000; font-weight: bold; ">LINE TOTAL</td>
    </tr>  
    <tr>
      <td bgcolor="#FFFFFF" >Aluminum</td>
      <td bgcolor="#FFFFFF" >aluminum</td>
      <td align="center" bgcolor="#FFFFFF" >1</td>
      <td align="center" bgcolor="#FFFFFF" >230</td>
      <td align="center" bgcolor="#FFFFFF" >230</td>
    </tr>
	 <tr>
      <td bgcolor="#FFFFFF" >Aluminum</td>
      <td bgcolor="#FFFFFF" >aluminum</td>
      <td align="center" bgcolor="#FFFFFF" >1</td>
      <td align="center" bgcolor="#FFFFFF" >230</td>
      <td align="center" bgcolor="#FFFFFF" >230</td>
    </tr>
    <tr>
      <td colspan="3" rowspan="4" bgcolor="#FFFFFF">&nbsp;</td>
      <td bgcolor="#FFFFFF" >Subtotal (AED)</td>
      <td align="center" bgcolor="#FFFFFF" >900.00</td>
    </tr>
    <tr>
      <td bgcolor="#FFFFFF" >Production charge (AED)</td>
      <td align="center" bgcolor="#FFFFFF" >1500</td>
    </tr>
    <tr>
      <td bgcolor="#FFFFFF" >Discount (AED)</td>
      <td align="center" bgcolor="#FFFFFF" >80</td>
    </tr>
    <tr style="font-weight: bold; ">
      <td bgcolor="#FFFFFF" >Estimate Total (AED)</td>
      <td align="center" bgcolor="#FFFFFF" >7110.00</td>
    </tr>
</table>
<br/>
<br/>
<table width="100%" cellpadding="4" cellspacing="1">
  <tr>
    <td><strong>Terms &amp; Conditions</strong></td>
  </tr>
  <tr>
    <td>Make all checks payable to Infoquest JLT<br/>
      The Service will be completely available only on receipt of 100% payment<br/>
      No refund will be given in case of cancellation of order.<br/>
      This is a computer-generated invoice and does not require any signature.</td>
  </tr>
</table>
</body>
</html>';
// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');

// reset pointer to the last page
$pdf->lastPage();

// ---------------------------------------------------------

//Close and output PDF document
$pdf->Output('example_061.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+
