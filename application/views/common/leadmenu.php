<?php
$actualurl="http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$seg = $this->uri->segment(4);
$seg3 = $this->uri->segment(3);
?>
<nav class="subnav" role="navigation">
  <div class="container">

    <div class="row">
      <div class="col-sm-12">
        <ul class="list-inline">
          <li <?php echo (base_url('Lead_controller')==$actualurl)?'class=active':''; ?>><a href="<?php echo base_url('Lead_controller');?>">Summary</a></li>
          <li <?php echo (base_url('Lead_controller/forsale')==$actualurl)?'class=active':''; ?>><a href="<?php echo base_url('Lead_controller/forsale');?>">For sale leads</a></li>
          <li <?php echo (base_url('Lead_controller/rent')==$actualurl)?'class=active':''; ?>><a href="<?php echo base_url('Lead_controller/rent');?>">To rent leads</a></li>
          <li <?php echo (base_url('Lead_controller/valuation')==$actualurl)?'class=active':''; ?>><a href="<?php echo base_url('Lead_controller/valuation');?>">Valuation leads</a></li>  
       </ul>
      </div>
    </div>
    
  </div>
</nav>