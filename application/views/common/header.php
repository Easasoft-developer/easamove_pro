<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$this->load->library('session');
$svar = $this->session->userdata['agentuser'];
$name = $svar['name'];
$actualurl="http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
//$access = $svar['access'];
$seg = $this->uri->segment(4);
$seg3 = $this->uri->segment(3);

$agentIdUpload = $this->session->userdata['agentuser']['id'];
$agentNameUpload = $this->session->userdata['agentuser']['name'];


?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>EASAPRO</title>

    <!-- Bootstrap -->
    <link href="<?php echo base_url();?>css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>css/main.css" rel="stylesheet">
    <link href="<?php echo base_url();?>css/easa.pro.helpers.css" rel="stylesheet">
    <link href="<?php echo base_url();?>css/responsive.css" rel="stylesheet">
    <link href="<?php echo base_url();?>css/jquery-te-1.4.0.css" rel="stylesheet">
    <link rel="icon" href="<?php echo base_url(); ?>images/favicon.png" type="image/png">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
<link href="<?php echo base_url(); ?>css/datepicker.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>css/daterangepicker.css" rel="stylesheet">
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>js/jquery.ui.timepicker.css?v=0.3.3" type="text/css" />  
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="<?php echo base_url(); ?>js/jquery.min.js"></script>
  </head>
  <body class="dashboard">
    <section id="container">
      <input type="hidden" value="<?php echo $agentIdUpload; ?>" id="agentIdUpload">
      <input type="hidden" value="<?php echo $agentNameUpload; ?>" id="agentNameUpload">
      <header>
      <nav class="menubar navbar navbar-default">
        <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
              <span class="sr-only">Toggle navigation</span>
                 <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>

              <!--logo start-->
              <a href="<?php echo base_url('dashboard');?>" class="navbar-brand">EASAMOVE</a>
              <!--logo end-->
            </div>
              <!-- Collect the nav links, forms, and other content for toggling -->
              <div class="menus collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                  <ul class="nav navbar-nav">
                      <li <?php echo (base_url()==$actualurl || base_url('dashboard')==$actualurl)?'class=active':''; ?>><a href="<?php echo base_url('dashboard');?>">Dashboard <span class="sr-only">(current)</span></a></li>
                      <li <?php echo (base_url('properties/Property')==$actualurl || base_url('properties/property/rentlist')==$actualurl || base_url('properties/property/rent')==$actualurl || base_url('properties/property/sale')==$actualurl || base_url('properties/property/showmap/1')==$actualurl || base_url('properties/property/showmap/2')==$actualurl || base_url('properties/property/propEdit/'.$seg)==$actualurl || base_url('properties/property/proprentEdit/'.$seg)==$actualurl || base_url('properties/Property/rentlist/'.$seg)==$actualurl || base_url('properties/Property/index/'.$seg)==$actualurl || base_url('properties/Property/rentlist')==trim($actualurl,"/") || base_url('properties/Property/index')==trim($actualurl,"/"))?'class=active':''; ?>><a href="<?php echo base_url('properties/Property');?>">Properties</a>                       
                      </li>
                      <li <?php echo (base_url('settings')==$actualurl || base_url('settings/usermanage')==$actualurl || base_url('settings/useradd')==$actualurl || base_url('settings/branchManage')==$actualurl || base_url('settings/addbranch')==$actualurl || base_url('settings/userEdit/'.$seg3)==$actualurl || base_url('settings/branchEdit/'.$seg3)==$actualurl || base_url('settings/branchManage/'.$seg3)==$actualurl || base_url('settings/usermanage/'.$seg3)==$actualurl || base_url('settings/branchManage/')==trim($actualurl,"/") || base_url('settings/usermanage/')==trim($actualurl,"/"))?'class=active':''; ?>><a href="<?php echo base_url('settings/usermanage');?>">Settings</a>                     
                      </li>
                      <li <?php echo (base_url('Lead_controller')==$actualurl || base_url('Lead_controller/forsale')==$actualurl || base_url('Lead_controller/rent')==$actualurl || base_url('Lead_controller/valuation')==$actualurl)?'class=active':''; ?>><a href="<?php echo base_url() ?>Lead_controller">Leads</a>                     
                      </li>
                      <li <?php echo (base_url('Lead_controller/letting')==$actualurl)?'class=active':''; ?>><a href="<?php echo base_url() ?>Lead_controller/letting">Letting Information</a>                     
                      </li>
                      <li <?php echo (base_url('reports/reports')==$actualurl)?'class=active':''; ?>><a href="<?php echo base_url('reports/reports');?>">Reports</a></li>                     
                  </ul>
                  <ul class="nav navbar-nav navbar-right">
                    
                    <!-- user login dropdown start-->
                      <li class="dropdown">
                          <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                              <!-- <img alt="" src="images/avatar.png"> -->
                              <span class="username"><?php echo $name;?></span>
                              <b class="caret"></b>
                          </a>
                          <ul class="dropdown-menu extended logout">
                             <div class="log-arrow-up"></div>
                             <li><a href="<?php echo base_url('myaccount/myaccount'); ?>"><i class="fa fa-cog"></i> Account Settings</a></li>
                             <li><a href="<?php echo base_url('myaccount/myaccount/changepassword'); ?>"><i class="fa fa-unlock"></i> Change password</a></li>
                             
                             <li><a href="<?php echo base_url('account/logout');?>"><i class="fa fa-sign-out"></i> Log Out</a></li>
                         </ul>
                      </li>
                      <!-- user login dropdown end -->
                  </ul>

              </div>            
          </div>          
          </nav>
          

      </header>
