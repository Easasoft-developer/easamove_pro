<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$actualurl="http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$seg = $this->uri->segment(4);
?>
<nav class="subnav" role="navigation">
    <div class="container">

        <div class="row">
            <div class="col-sm-12">
                <ul class="list-inline">
                    <li <?php echo (base_url('properties/Property')==$actualurl || base_url('properties/property/showmap/1')==$actualurl || base_url('properties/property/sale')==$actualurl || base_url('properties/property/propEdit/'.$seg)==$actualurl || base_url('properties/Property/index/'.$seg)==$actualurl || base_url('properties/Property/index')==trim($actualurl,"/"))?'class=active':''; ?>><a href="<?php echo base_url('properties/Property'); ?>">For Sale <span class="badge"></span></a></li>
                    <li <?php echo (base_url('properties/property/rentlist')==$actualurl || base_url('properties/property/showmap/2')==$actualurl || base_url('properties/property/rent')==$actualurl || base_url('properties/property/proprentEdit/'.$seg)==$actualurl || base_url('properties/Property/rentlist/'.$seg)==$actualurl || base_url('properties/Property/rentlist')==trim($actualurl,"/"))?'class=active':''; ?>><a href="<?php echo base_url('properties/property/rentlist'); ?>">To Rent <span class="badge"></span></a></li>
                </ul>
            </div>
        </div>

    </div>
</nav>