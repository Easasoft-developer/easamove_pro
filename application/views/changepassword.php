<?php 
$actualurl = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

?>
<div class="col-md-2 side-menu-tab">
    <div class="row side-menu-heading">
        <div class="col-md-12">
            <!--<h2 class="pl20 mt10 fwb mb20">Admin</h2>-->
        </div>
    </div>
    <ul class="tabs">
        <li <?php echo (base_url('adminUser') == $actualurl) ? 'class=active' : ''; ?>>
            <a href="<?php echo base_url(); ?>adminUser">Manage User</a>
            <span></span>
        </li>

    </ul>
    <ul class="tabs">
        <li <?php echo (base_url('myprofile') == $actualurl) ? 'class=active' : ''; ?>>
            <a href="<?php echo base_url(); ?>account/myprofile">My Profile</a>
            <span></span>
        </li>

    </ul>
</div>
<div class="col-md-9">
    <h3 class="masters_title">Change Password</h3>


    <div class="">
        <form method="post" id="changepassword">
            <div class="row mb10 mt10">

                <div class="col-md-12 pl5 pr0">
                    <div class="form-group tar clearfix mb5">
                        <label class="col-sm-4 control-label" for="name">New Password</label>
                        <div class="col-sm-3">
                            <input type="text" id="newpassword" name="newpassword" maxlength="20" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group tar clearfix mb5">
                        <label class="col-sm-4 control-label" for="Email">Confirm Password</label>
                        <div class="col-sm-3">
                            <input type="text" id="cpassword" name="cpassword" maxlength="20" class="form-control" />
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6 mt10 tar">
                            <button class="btn btn-danger reset" type="button"><i class="fa fa-times pr5"></i>Cancel</button>
                            <button class="btn btn-primary submit" type="button"><i class="fa fa-save pr5"></i>Submit</button>
                        </div>
                    </div>   

                </div>
            </div>  
        </form>

    </div>

</div>
<script>
    var base_url = '<?php echo base_url(); ?>';
    var cont = 'AdminUser';

    $(document).ready(function () {
        
    });
    $('.submit').on('click', function () {
        $('.error').remove();
        var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;

//        alert(echeck);

        if ($("input[name='newpassword']").val() == '') {
            $("input[name='newpassword']").after('<span class="error">Please fill this field</span>');
        } else if ($("input[name='cpassword']").val() == '') {
            $("input[name='cpassword']").after('<span class="error">Please fill this field</span>');
        } else if ($("input[name='newpassword']").val() != $("input[name='cpassword']").val()) {
            $("input[name='cpassword']").after('<span class="error">Password mismatch</span>');

        } else {

            var datas = $('#changepassword').serialize();
            $.ajax({
                type: "POST",
                url: base_url + "/" + cont + "/resetPassword",
                data: datas,
                success: function (data) {
                    alert('Password Changed Successfully');
                    window.location.href='<?php echo base_url('Account/logout');?>'
                }
            });

            $('#changepassword')[0].reset();

        }
    });


</script>
