<style type="text/css">
    .panel-heading {
  padding: 25px 15px;
  border-bottom: 1px solid #ebebeb; }
  .panel-body {
    padding: 15px;
}
.successUpdate{
    position: absolute;
    right: 30px;
    width: 50%;
    top: 13px;
  }
</style>
<section id="main-content" class="main-content">
<section class="surround1">



<div class="container-fluid user-settings">
  <div class="row">
      <div class="col-xs-12">
        <div class="panel">
          <div class="panel-heading">
            

            <div class="row">
              <div class="col-sm-12">
                <h3>Leads Summary
                <!-- <button type="button" onclick="print();" class="btn btn-default pull-right">Print</button> -->
                </h3>
              </div>
            </div>
          </div>

          <div id="top_x_div" style="width: 900px; height: 500px; margin: 30px 17px;"></div>



        </div>
      </div>    
  </div>
</div>
  
</section>        
</section>

<script type="text/javascript" src="https://www.google.com/jsapi?autoload={'modules':[{'name':'visualization','version':'1.1','packages':['bar']}]}"></script>
<script type="text/javascript">
  // barsVisualization must be global in our script tag to be able
  // to get and set selection.
  google.setOnLoadCallback(drawStuff);

      function drawStuff() {
        var data = new google.visualization.arrayToDataTable([
          ['Leads name', 'emails'],
          ["For sale emails", <?php echo $saleCount; ?>],
          ["To rent emails", <?php echo $rentCount; ?>],
          ["Valuation emails", <?php echo $valuationCount; ?>]
        ]);

        var options = {
          title: 'Leads Summary',
          width: 1200,
          legend: { position: 'none' },
          bars: 'horizontal', // Required for Material Bar Charts.
          axes: {
            x: {
              0: { side: 'top', label: ''} // Top x-axis.
            }
          },
          bar: { groupWidth: '20%' },
        };

        var chart = new google.charts.Bar(document.getElementById('top_x_div'));
        chart.draw(data, options);
      };

      function print(){
        $('#top_x_div').print();
      }

</script>