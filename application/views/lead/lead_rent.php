<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//$actualurl="http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

?>
<style type="text/css">
    .panel-heading {
  padding: 25px 15px;
  border-bottom: 1px solid #ebebeb; }
  .panel-body {
    padding: 15px;
}
.successUpdate{
    position: absolute;
    right: 30px;
    width: 50%;
    top: 13px;
  }
</style>
<section id="main-content" class="main-content">
<section class="surround1">



<div class="container-fluid user-settings">
  <div class="row">
      <div class="col-xs-12">
        <div class="panel">
          <div class="panel-heading">
            

            <div class="row">
              <div class="col-sm-12">
                <h3>Torent leads</h3>
              </div>
            </div>
            <div class="row sel-sear-btn">
              
              <div class="col-md-6 col-sm-12 col-xs-10 pr0">
                <div class="form-group">
                <input type="text" class="form-control search-prop" onchange="getEventDate(this.value);" id="config-demo" name="search" placeholder="Search here">
              </div>
              </div>
              <div class="col-md-6 col-sm-12 hidden-xs extraoption">
                  <div class="btn-group">
                  <button type="button" class="btn btn-default" onclick="downloadpdf()"><i class="fa fa-print"></i> <span class="hidden-xs">Print</span></button>
                  <button type="button" class="btn btn-default" onclick="downloadpdf()"><i class="glyphicon glyphicon-download-alt"></i> <span class="hidden-xs">Save as PDF</span></button>
                  <button type="button" class="btn btn-default" onclick="exportbranch()"><i class="fa fa-external-link"></i> <span class="hidden-xs">Export to Excel</span></button>
                
                  </div>
              </div>
              
              <div class="btn-group col-xs-2 hidden-sm hidden-md hidden-lg extraoption">
                <button type="button" class="btn dropdown-toggle pull-right" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="fa fa-ellipsis-v fa-lg"></i>
                  <span class="sr-only">Toggle Dropdown</span>
                </button>
                <ul class="dropdown-menu">
                  <li><a href="javascript:;" onclick="downloadpdf()"><i class="fa fa-print"></i> <span>Print</span></a></li>
                  <li><a href="javascript:;" onclick="downloadpdf()"><i class="glyphicon glyphicon-download-alt"></i> <span>Save as PDF</span></a></li>
                  <li><a href="javascript:;" onclick="exportbranch()"><i class="fa fa-external-link"></i> <span>Export to Excel</span></a></li>
                </ul>
              </div>
            </div>

          </div>
          <div class="">
            <div class="table-responsive">
            <table class="table table-striped table-hover" id="no-more-tables">
                <thead>
                <tr>
                    <th>Date</th>
                    <th>Customer details</th>
                    <th>Customer status</th>
                    <th>Property</th>
                    <th>Message</th>
                        
                </tr>
                </thead>
                <tbody>
                  
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>    
  </div>
</div>
  
</section>        
</section>

<script>
var base_url = '<?php echo base_url(); ?>';
    function exportbranch() {
        var date = $('#config-demo').val();
        // var chan = chan.replace(/\\/g, ".");
        // console.log(chan);
        var url = base_url + "Lead_controller/exportLeads/rent";
        window.open(url, '_blank');

    }

    function downloadpdf() {
      var date = $('#config-demo').val();
        //var url = base_url + "settings/exportbranch";
        var userid = "<?php echo $this->session->userdata['agentuser']['id']; ?>";
      var today = new Date();
      var dd = today.getDate();
      var mm = today.getMonth() + 1; //January is 0!

      var yyyy = today.getFullYear();
      if (dd < 10) {
          dd = '0' + dd
      }
      if (mm < 10) {
          mm = '0' + mm
      }
      var today = yyyy + '-' + mm + '-' + dd;
      var openurl = base_url + '/images/pdf/leadsprint_' + userid + '_' + today + '.pdf';
        $.ajax({
            url: base_url + "Lead_controller/leadsprint",
            data: "date=" + date +'&type=rent',
            type: "POST",
            success: function (data) {
                window.open(openurl, '_blank');
            }
        });

    }
    function print(){
      var date = $('#config-demo').val();
      console.log(date);
      //var url = base_url + "settings/exportbranch";
      var userid = "<?php echo $this->session->userdata['agentuser']['id']; ?>";
      var today = new Date();
      var dd = today.getDate();
      var mm = today.getMonth() + 1; //January is 0!

      var yyyy = today.getFullYear();
      if (dd < 10) {
          dd = '0' + dd
      }
      if (mm < 10) {
          mm = '0' + mm
      }
      var today = yyyy + '-' + mm + '-' + dd;
      var openurl = base_url + '/images/pdf/leadsprint_' + userid + '_' + today + '.pdf';
      $.ajax({
          url: base_url + "Lead_controller/leadsprint",
          type: "POST",
          data: "date=" + date +'&type=rent',
          success: function (data) {
            var w = window.open(openurl);
            if (navigator.appName == 'Microsoft Internet Explorer') window.print();
            else w.print();
          }
      });
    }

    function getEventDate(id){
      console.log(id);
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>/Lead_controller/searchLeads",
            data: "date=" + id +'&type=rent',
            dataType: "html",
            success: function (data) {
              if(data != '1'){
                $('tbody').empty().append(data);
                $('.extraoption').show();
              }else{
                $('tbody').empty().append('<tr id="no-data"><td colspan="12">No Record Found</td></tr>');
                $('.extraoption').hide();
              }
            }
        });
    }
</script>
