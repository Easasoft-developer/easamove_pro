<?php
include("common/header.php");
$leadcount = $saleCount + $rentCount + $valuationCount;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?>
<section class="main-content">
        <section class="surround">

          <div class="row">
          <div class="panel">
              <div class="panel-body">
              <?php 
              if(!empty($salecount)){
                ?>
                <div style="padding: 20px;" class="col-sm-3">
                  <div id="donut_sale"></div>
                  
                </div>
                <?php
              }
              ?>
              <?php 
              if(!empty($rentcount)){
                ?>
                <div style="padding: 20px;" class="col-sm-3">
                   <div id="donut_rent"></div>
                </div>
                <?php
              }
              ?>
                 
                
                <?php if(!empty($soldHouse)){ ?>
                <div style="padding: 20px;" class="col-sm-3">
                   <div id="donut_sold"></div>
                </div>
                <?php } ?>
                <?php 
                if(!empty($$leadcount)){
                  ?>
                  <div style="padding: 20px;" class="col-sm-3">
                     <div id="donut_lead"></div>
                  </div>
                  <?php
                }
                ?>
                <div style="padding: 20px;" class="col-sm-3">
                   <div id="donut_lead"></div>
                </div> 
              </div>
          </div>
            
          </div>


          <div class="row emails-calls">
            <div class="col-md-5 col-sm-12">
              <div class="panel">
                <div class="panel-heading">
                  <div class="row">
                    <div class="col-sm-6">
                      <h3>Branch Details</h3>
                    </div>
                    <div class="col-sm-6 put-right">
                      <a href="<?php echo base_url(); ?>settings/branchManage"><img src="<?php echo base_url(); ?>images/view-all.png"></a>
                    </div>
                  </div>
                </div>

                  <div class="table-responsive">
                    <table class="table table-striped table-hover">
                        <thead class="branch">
                        <tr>
                            <th>S.No</th>
                            <th>Branch Name</th>
                            <th>Branch Address</th>
                        </tr>
                        </thead>
                        
                        
                      </table>
                    </div>
                
              </div>
            </div>

            <div class="col-md-7 col-sm-12">
              <div class="panel">
                <div class="panel-heading">

                  <div class="row">
                    <div class="col-sm-6">
                      <h3>Properties</h3>
                    </div>
                    <div class="col-sm-6 put-right">
                      <a href="<?php echo base_url(); ?>properties/Property"><img src="<?php echo base_url(); ?>images/view-all.png"></a>
                    </div>
                  </div>
                  
                </div>

                <div class="table-responsive">
                    <table class="table table-striped table-hover">
                        <thead class="prop">
                        <tr>
                            <th>S.No</th>
                            <th>Property Type</th>
                            <th>Property Address</th>
                            <th>Property Listed</th>
                            <th>Property Modified</th>
                        </tr>
                        </thead>
                        
                        
                      </table>
                    </div>
                
              </div>
            </div>
          </div>
          
          

          

        </section>        
      </section>
      
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script>
    var base_url = '<?php echo base_url(); ?>';
    getProp(base_url);
    getBranch(base_url);
    console.log($.fn.jquery);
    function getProp(base_url) {
//        $('table .data-append').remove();
//        $('table .foot').remove();
        $.ajax({
            type: "POST",
            url: base_url + "properties/property/getdashboardprop",
            success: function (data) {
//                console.log(data);
                if (data != '') {
                    $('thead.prop').after(data);
                } else {
                    $('thead.prop').after('No Data');
                }
            }
        });
    }
    function getBranch(base_url) {
//        $('table .data-append').remove();
//        $('table .foot').remove();
        $.ajax({
            type: "POST",
            url: base_url + "properties/property/getBranch",
            success: function (data) {
                console.log(data);
                if (data != '') {

                    $('thead.branch').after(data);
                } else {
                    $('thead.branch').after('No Data');
                }
            }
        });
    }

//chart
    google.load("visualization", "1", {packages:["corechart"]});
    google.setOnLoadCallback(drawChart);
    google.setOnLoadCallback(drawChartrent);
    google.setOnLoadCallback(drawChartsold);
    google.setOnLoadCallback(drawChartlead);
    function drawChart() {

    var data = google.visualization.arrayToDataTable([
      ['Effort', 'Amount given'],
      ['No of Sale Property', <?php echo $salecount; ?>],
    ]);

    var options = {
      backgroundColor:'#efefef',
      // backgroundColor: {fill:'#CCC', stroke:'#FF9000',strokeWidth:'6'},
      // backgroundColor: {'blue'},
      pieHole: 0.85,
      pieSliceText: 'value',
      pieSliceBorderColor: 'transparent',
      pieSliceTextStyle: {
        color: '#777',
        fontSize: 40,
      },
      pieStartAngle:'135',
      legend: {position: 'bottom', textStyle: {color: '#555555', fontSize: 15}},
      
      chartArea: { width:'75%',height:'75%', backgroundColor: {
                fill:'#C0C',
                stroke: '#4322c0',
                strokeWidth: 3
            }
        },
      width:300,
      height:250,
      tooltip: { text: 'value' },
      slices: {
        0: { color: '#ff8f02'}
      },
      sliceVisibilityThreshold: 0.8,
      colors: ['#e0440e']

    };

    var chart = new google.visualization.PieChart(document.getElementById('donut_sale'));
    chart.draw(data, options);
    }

    //for rent
    function drawChartrent() {

    var data = google.visualization.arrayToDataTable([
      ['Effort', 'Amount given'],
      ['No of Rent Property',  <?php echo $rentcount; ?>],
    ]);

    var options = {
      backgroundColor:'#efefef',
      // backgroundColor: {fill:'#CCC', stroke:'#FF9000',strokeWidth:'6'},
      // backgroundColor: {'blue'},
      pieHole: 0.85,
      pieSliceText: 'value',
      pieSliceBorderColor: 'transparent',
      pieSliceTextStyle: {
        color: '#777',
        fontSize: 40,
      },
      pieStartAngle:'135',
      legend: {position: 'bottom', textStyle: {color: '#555555', fontSize: 15}},
      
      chartArea: { width:'75%',height:'75%', backgroundColor: {
                fill:'#C0C',
                stroke: '#4322c0',
                strokeWidth: 3
            }
        },
      width:300,
      height:250,
      tooltip: { text: 'value' },
      slices: {
        0: { color: '#ffce55'}
      },
      sliceVisibilityThreshold: 0.6,
      colors: ['#e0440e']

    };

    var chart = new google.visualization.PieChart(document.getElementById('donut_rent'));
    chart.draw(data, options);
    }

    //for sold
    function drawChartsold() {

    var data = google.visualization.arrayToDataTable([
      ['Effort', 'Amount given'],
      ['No of Sold Property', <?php echo $soldHouse; ?>],
    ]);

    var options = {
      backgroundColor:'#efefef',
      // backgroundColor: {fill:'#CCC', stroke:'#FF9000',strokeWidth:'6'},
      // backgroundColor: {'blue'},
      pieHole: 0.85,
      pieSliceText: 'value',
      pieSliceBorderColor: 'transparent',
      pieSliceTextStyle: {
        color: '#777',
        fontSize: 40,
      },
      pieStartAngle:'135',
      legend: {position: 'bottom', textStyle: {color: '#555555', fontSize: 15}},
      
      chartArea: { width:'75%',height:'75%', backgroundColor: {
                fill:'#C0C',
                stroke: '#4322c0',
                strokeWidth: 3
            }
        },
      width:300,
      height:250,
      tooltip: { text: 'value' },
      slices: {
        0: { color: '#a0d468'}
      },
      sliceVisibilityThreshold: 0.6,
      colors: ['#e0440e']

    };

    var chart = new google.visualization.PieChart(document.getElementById('donut_sold'));
    chart.draw(data, options);
    }

    //for lead
    function drawChartlead() {

    var data = google.visualization.arrayToDataTable([
      ['Effort', 'Amount given'],
      ['No of Leads', <?php echo $leadcount; ?>],
    ]);

    var options = {
      backgroundColor:'#efefef',
      // backgroundColor: {fill:'#CCC', stroke:'#FF9000',strokeWidth:'6'},
      // backgroundColor: {'blue'},
      pieHole: 0.85,
      pieSliceText: 'value',
      pieSliceBorderColor: 'transparent',
      pieSliceTextStyle: {
        color: '#777',
        fontSize: 40,
      },
      pieStartAngle:'135',
      legend: {position: 'bottom', textStyle: {color: '#555555', fontSize: 15}},
      
      chartArea: { width:'75%',height:'75%', backgroundColor: {
                fill:'#C0C',
                stroke: '#4322c0',
                strokeWidth: 3
            }
        },
      width:250,
      height:250,
      tooltip: { text: 'value' },
      slices: {
        0: { color: '#2dc3e8'}
      },
      sliceVisibilityThreshold: 0.6,
      colors: ['#e0440e']

    };

    var chart = new google.visualization.PieChart(document.getElementById('donut_lead'));
    chart.draw(data, options);
    }



</script>
<?php
include("common/footer.php");
?>

