<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
﻿<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>EASAPRO</title>

        <!-- Bootstrap -->
        <link href="<?php echo base_url(); ?>css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>css/main.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>css/responsive.css" rel="stylesheet">
        <link rel="icon" href="<?php echo base_url(); ?>images/favicon.png" type="image/png">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

        <link rel="stylesheet" href="<?php echo base_url(); ?>css/datatables.css" />
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="log">
        <div class="login-bg">
            <div class="panel login text-center">
            <div class="pull-right"><a href="<?php echo base_url('Account/login'); ?>"><i style="font-size: 18px;" class="fa fa-arrow-circle-left"></i></a></div>
                <div class="panel-heading "> 
                    <h1><img src="<?php echo base_url(); ?>images/logo_img.png" style="width: 200px;
"></h1>
                    <h4 class="panel-title">Forgot Password</h4>    
                </div>
                <div class="panel-body">


                    <form role="form" method="post" action="<?php echo base_url('Account/forgotpassword'); ?>">
                        <?php if ($this->session->flashdata('forgot')) { ?>
                            <div class="alert alert-success"> <?= $this->session->flashdata('forgot') ?> </div>
                        <?php } ?>
                        <div class="form-group">
                            <label class="sr-only" for="user">Email address</label>
                            <input type="text" placeholder="Enter email" name="UserEmail" value="" id="UserEmail" class="form-control">
                        </div> 

                        <div class="text-center">
                            <button class="btn btn-warning btn-sm" type="submit">Submit</button>
                        </div>
                    </form>
                </div>
                </form>
            </div>
        </div>


        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="<?php echo base_url(); ?>js/bootstrap.min.js"></script>

    </body>
</html>
