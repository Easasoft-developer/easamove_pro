<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
﻿<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>EASAPRO</title>

        <!-- Bootstrap -->
        <link href="<?php echo base_url(); ?>css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>css/main.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>css/custom_class.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>css/responsive.css" rel="stylesheet">
        <link rel="icon" href="<?php echo base_url(); ?>images/favicon.png" type="image/png">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

        <link rel="stylesheet" href="<?php echo base_url(); ?>css/datatables.css" />
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="log">
        <div class="login-bg">
            <div class="panel login text-center">
                <div class="panel-heading "> 
                    <h1><img src="<?php echo base_url(); ?>images/logo_img.png" style="width: 200px;
"></h1>
                </div>
                <div class="panel-body">
                    <?php
                    if(isset($message)){

                    echo "<div class='control-group errorcls alertForError' style='padding-bottom:10px;'>".$message."</div>";   

                    }
                    if(!isset($messageId)){
                    ?>
                    <form id="resetPassword" method="POST" action="<?php echo base_url('Account/reset'); ?>" name="resetPassword" class="col-md-12 col-sm-12 col-xs-12 col-lg-12 pb10" onsubmit="return validate();">
                        <input type="hidden" name="fkpassword" value="<?php if(isset($getId)) { echo $getId; } ?>">
                        <?php if ($this->session->flashdata('logincheck')) { ?>
                            <div class="alert alert-danger"> <?= $this->session->flashdata('logincheck') ?> </div>
                        <?php } ?>
                        <div class="form-group">
                            <label class="sr-only" for="user">New password</label>
                            <input type="password" name="newpassword" id="newpassword" placeholder="Enter New Password" class="form-control txtEmailPass" value="">
                        </div> 

                        <div class="form-group">
                            <label class="sr-only" for="password">Confirm password</label>
                            <input type="password" name="confirmpassword" id="confirmpassword" placeholder="Re-enter your Password" class="form-control txtEmailPass" value="<?php $this->input->post('confirmpassword') ?>">
                        </div>
                        <button type="submit" class="btn btn-warning btn-sm btnLogin" name="reset" id="reset">Change password</button>

                    </form>
                    <?php }else{
    ?>
                    <div class="pb mb pt alert alertForSuccess   fadeoutthis" role="alert">
                         <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> 
                        <span><?=$messageId?>
                          
                        </span>
                    </div>
<?php
} ?>
                </div>
            </div>
        </div>


        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="<?php echo base_url(); ?>js/bootstrap.min.js"></script>
        <script>
           function validate(){
               
                $('.error').remove();
               if ($("input[name='newpassword']").val() == '') {
                    $("input[name='newpassword']").after('<span class="error" style="color:red">Please fill this field</span>');
                     return false;
                } else if ($("input[name='confirmpassword']").val() == '') {
                    $("input[name='confirmpassword']").after('<span class="error" style="color:red">Please fill this field</span>');
                     return false;
                } else {
                    return true;
                }
                 return false;
            }
        </script>
    </body>
</html>




