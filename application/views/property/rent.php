<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//print_r($property);
$aid = $this->session->userdata['agentuser']['id'];
?>
<style type="text/css">
    .panel-heading {
  padding: 25px 15px;
  border-bottom: 1px solid #ebebeb; }
  .panel-body {
    padding: 15px;
}
</style>
<section class="surround1"> 
    <form id="myForm" method="post" enctype="multipart/form-data" action="<?php echo base_url(); ?>properties/property/addrentProp" onsubmit="return submitValidationForm();">
    <input type="hidden" id="id" name="id" value="<?php echo isset($property[0]->property_id) ? $property[0]->property_id : '0'; ?>" />
        <div class="container-fixed add-listing">
<!-- property info -->
        <div class="panel">
          <div class="panel-heading">
            <div class="row">
              <div class="col-sm-6">
                <h3>Add Property Listing To Rent</h3>
              </div>
              <div class="col-sm-6">
                <div class="save-cancel">
                  <button type="submit" class="btn btn-default submit">Save</button>
                  <a href="<?php echo base_url(); ?>properties/property/rentlist" class="btn btn-default">Cancel</a>
                </div>
              </div>
            </div>           
            
          </div>

          <div class="panel-title">
            Property Information
          </div>
          

          <div class="panel-body">
            <div class="row">
              <div class="col-sm-6">
                
                  <div class="form-group">
                    <label for="p-status">Property Site Status <span style="color: red;">*</span></label>
                    <select id="p-status" name="p-status" class="form-control">
                        <option value="0">- Select -</option>
                        <?php
                        foreach ($sitestatus as $val) {
                            $check = '';
                            if (isset($property)) {
                                if ($property[0]->property_pstatus == $val->sitestatus_id) {
                                    $check = 'selected="selected"';
                                }
                            }
                            echo "<option $check value=" . $val->sitestatus_id . ">" . $val->sitestatus_name . "</option>";
                        }
                        ?>

                    </select>
                  </div>
                  <div class="form-group">
                    <label for="l-status">Listing Status<span style="color: red;">*</span></label>
                    <select id="l-status" name="l-status" class="form-control">
                        <option value="0">- Select -</option>
                        <?php
                        foreach ($liststatus as $val) {
                            $check = '';
                            if (isset($property)) {
                                if ($property[0]->property_lstatus == $val->listingstatus_id) {
                                    $check = 'selected="selected"';
                                }
                            }
                            echo "<option $check value=" . $val->listingstatus_id . ">" . $val->listingstatus_name . "</option>";
                        }
                        ?>
                    </select>
                  </div>

                  <div class="form-group">
                    <label for="p-type">Property Type<span style="color: red;">*</span></label>
                    <select id="p-type" name="p-type"  class="form-control">
                        <option value="0">- Select -</option>
                        <?php
                        foreach ($proptype as $val) {
                            $check = '';
                            if (isset($property)) {
                                if ($property[0]->property_ptype == $val->propertytype_id) {
                                    $check = 'selected="selected"';
                                }
                            }
                            echo "<option $check value=" . $val->propertytype_id . ">" . $val->propertytype_name . "</option>";
                        }
                        ?>
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="tenure">Tenure <span style="color: red;">*</span></label>
                    <select id="tenure"  name="tenure"  class="form-control">
                        <option value="0">- Select -</option>
                        <?php
                        foreach ($tenure as $val) {
                            $check = '';
                            if (isset($property)) {
                                if ($property[0]->property_tenure == $val->tenures_id) {
                                    $check = 'selected="selected"';
                                }
                            }
                            echo "<option $check value=" . $val->tenures_id . ">" . $val->tenures_name . "</option>";
                        }
                        ?>
                    </select>
                  </div>

                  <div class="form-group">
                    <label for="p-transact">Property transaction type</label>
                    <select id="p-transact" name="p-transact"  class="form-control">
                        <option value="0">- Select -</option>
                        <?php
                        foreach ($transtype as $val) {
                            $check = '';
                            if (isset($property)) {
                                if ($property[0]->property_ptransact == $val->transactiontype_id) {
                                    $check = 'selected="selected"';
                                }
                            }
                            echo "<option $check value=" . $val->transactiontype_id . ">" . $val->transactiontype_name . "</option>";
                        }
                        ?>
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="build-age">Building Age</label>
                    <input id="build-age" name="build-age" maxlength="20" placeholder="Please enter age of the building" type="text" class="form-control" value="<?php echo isset($property[0]->property_buildage) ? $property[0]->property_buildage : ''; ?>">
                  </div>
                  <div class="form-group">
                    <label for="build-const">Building Condition</label>
                    <select id="build-const" name="build-const"  class="form-control">
                        <option value="0">- Select -</option>
                        <?php
                        foreach ($buildcont as $val) {
                            $check = '';
                            if (isset($property)) {
                                if ($property[0]->property_buildconst == $val->buildcondition_id) {
                                    $check = 'selected="selected"';
                                }
                            }
                            echo "<option $check value=" . $val->buildcondition_id . ">" . $val->buildcondition_name . "</option>";
                        }
                        ?>
                    </select>
                  </div>
                  

                
              </div>

              <div class="col-sm-6">
                  <div class="form-group">
                    <label for="let-type">Letting Type</label>
                    <select id="let-type" name="let-type"  class="form-control">
                        <option value="0">- Select -</option>
                        <?php
                        foreach ($lettype as $val) {
                            $check = '';
                            if (isset($property)) {
                                if ($property[0]->property_lettype == $val->lettingtype_id) {
                                    $check = 'selected="selected"';
                                }
                            }
                            echo "<option $check value=" . $val->lettingtype_id . ">" . $val->lettingtype_name . "</option>";
                        }
                        ?>
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="dateavail">Date Available</label>
                    <input id="dateavail" name="dateavail" type="text" placeholder="Please select date" class="form-control" value="<?php echo isset($property[0]->property_dateavailable) ? $property[0]->property_dateavailable : ''; ?>">
                  </div>
                  <div class="form-group">
                    <label for="contractlen">Contract length</label>
                    <input id="contractlen" name="contractlen" type="text" placeholder="Please enter contract length" maxlength="10" class="form-control" value="<?php echo isset($property[0]->property_contractlen) ? $property[0]->property_contractlen : ''; ?>">
                  </div>
                  <div class="form-group">
                    <label for="depositamount">Deposit Amount</label>
                    <input id="depositamount" name="depositamount" type="text" placeholder="Please enter deposit amount" maxlength="10" class="form-control" value="<?php echo isset($property[0]->property_depositamount) ? $property[0]->property_depositamount : ''; ?>">
                  </div>
                  <!-- <div class="form-group">
                    <label for="tenancyfee">Tenancy fees</label>
                    <input id="tenancyfee" name="tenancyfee" type="text" placeholder="Please enter tenancy fees" maxlength="10" class="form-control" value="<?php echo isset($property[0]->property_tenancyfee) ? $property[0]->property_tenancyfee : ''; ?>">
                  </div> -->
                  <div class="form-group">
                    <label for="let-info">Considerable information</label>
                    <select id="let-info1" name="let-info1" class="form-control">
                        <option value="0">- Select -</option>
                        <?php
                        foreach ($considerinfo as $val) {
                            $check = '';
                            if (isset($property)) {
                                if ($property[0]->property_considerinfo == $val->considerinfo_id) {
                                    $check = 'selected="selected"';
                                }
                            }
                            echo "<option $check value=" . $val->considerinfo_id . ">" . $val->considerinfo_name . "</option>";
                        }
                        ?>
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="fees-aply">Fees apply</label>
                    <select id="fees-aply" name="fees-aply" class="form-control" onchange="getFeesapplyDetail(this.value);">
                        <option value="0">- Select -</option>
                        <?php
                        foreach ($feesapply as $val) {
                            $check = '';
                            if (isset($property)) {
                                if ($property[0]->property_feesapply == $val->feesapply_id) {
                                    $check = 'selected="selected"';
                                }
                            }
                            echo "<option $check value=" . $val->feesapply_id . ">" . $val->feesapply_name . "</option>";
                        }
                        ?>
                    </select>
                  </div>
                  

                
              </div>

            </div>
            <div class="row feesapply_detail" <?php if($property[0]->property_feesapply == 3 ) { echo "style='display:block'"; }else{ echo "style='display:none'"; } ?>>
              <div class="col-sm-12">
                <label for="feesapply_details">Fees apply detail</label>
                <textarea id="feesapply_details" class="form-control ckeditor" name="feesapply_details" maxlength="1000" rows="5"><?php echo getAgentLettingInformation($aid); ?></textarea>
                <p class="help-block">Please correct your changes here.</p>                    
              </div>
            </div>
          </div>
          
        </div>  
<!-- property address -->
        <div class="panel">          

          <div class="panel-title">

          <div class="row">
            <div class="col-sm-6">
              Property Address
            </div>
            <div class="col-sm-6 hidden-xs">
              Drag the marker to correct location
            </div>
          </div>   
          
            
          </div>
          

          <div class="panel-body">
            <div class="row">
              <div class="col-sm-6">

              <div class="form-group">
                  <label for="p-name">Property name / Property number<span style="color: red;">*</span></label>
                                                  
                    <input id="p-name" name="p-name" placeholder="Please enter property name"  type="text" maxlength="100" class="form-control" value="<?php echo isset($property[0]->property_name) ? $property[0]->property_name : ''; ?>">
                  
                </div>
                <div class="form-group">
                  <label for="s-name">Street Name <span style="color: red;">*</span></label>
                  
                    <input id="s-name" name="s-name" placeholder="Please enter Address" type="text" maxlength="250"  class="form-control" value="<?php echo isset($property[0]->property_sname) ? $property[0]->property_sname : ''; ?>">
                  
                </div>
                <div class="form-group clearfix">
                  <label for="city" class="p0 col-md-4">City <span style="color: red;">*</span></label>
                  <div class="p0 col-md-8">
                    <input id="city" name="city" type="text" placeholder="Please enter city name" maxlength="100" class="form-control" value="<?php echo isset($property[0]->property_city) ? $property[0]->property_city : ''; ?>">
                  </div>
                </div>
                <div class="form-group clearfix">
                  <label for="county" class="p0 col-md-4">County <span style="color: red;">*</span></label>
                <div class="p0 col-md-8">
                    <input id="county" name="county" placeholder="Please enter county" type="text" class="form-control" value="<?php echo isset($property[0]->property_county) ? $property[0]->property_county : ''; ?>">
                 </div> 
                </div>
                <div class="form-group clearfix">
                  <label for="postal-c" class="p0 col-md-4">Postal Code <span style="color: red;">*</span></label>
                <div class="p0 col-md-8">
                    <input id="postal-c" name="postal-c" placeholder="Please enter postal code (Ex.BB2 4LG)" type="text" maxlength="10" class="form-control" value="<?php echo isset($property[0]->property_postalc) ? $property[0]->property_postalc : ''; ?>">
                 </div> 
                </div>
                
                <!-- <div class="form-group clearfix">
                  <label for="country" class="p0 col-md-4">Country <span style="color: red;">*</span></label>
                <div class="p0 col-md-8">
                    <input id="country" name="country" placeholder="Please enter country" type="text" class="form-control" value="<?php echo isset($property[0]->property_country) ? $property[0]->property_country : ''; ?>">
                 </div> 
                </div> -->
                <div class="form-group">
                  <label for="own-refer" class="p0 col-md-4">Owner Reference <span style="color: red;">*</span></label>
                  <div class="p0 col-md-8">
                    <input id="own-refer" name="own-refer" placeholder="Please enter reference no" type="text" maxlength="50" class="form-control" value="<?php echo isset($property[0]->property_ownrefer) ? $property[0]->property_ownrefer : ''; ?>">
                 </div> 
                    
                  
                </div>  
                
              </div>

              <div class="col-sm-6">

              <h5 class="visible-xs"><strong>Drag the marker to correct location</strong></h5>
                
                <div id="map" style="height: 346px;"></div>
                <div><input type="hidden" id="Latitude"  name="Latitude" value="<?php echo isset($property[0]->property_lat) ? $property[0]->property_lat : '0'; ?>" readonly/>
                <input type="hidden" id="Longitude"  name="Longitude" value="<?php echo isset($property[0]->property_long) ? $property[0]->property_long : '0'; ?>" readonly/></div>
              </div>

            </div>
          </div>
          
        </div> 

<!-- property  -->
        <div class="panel">          

          <div class="panel-title">
            Property price / Description
          </div>
          
<!-- features  -->
          <div class="panel-body">
            <div class="row">
              <div class="col-sm-6">
                
                  <div class="form-group">
                    <label for="price">Price <span style="color: red;">*</span></label>
                    <input id="price" name="price" placeholder="Please enter price (ex.£10000)" maxlength="10"  onkeypress="return (event.which >= 48 && event.which <= 57) || event.which == 8 || event.which == 46||event.which == 13" type="text" class="form-control" value="<?php echo isset($property[0]->property_price) ? $property[0]->property_price : ''; ?>">
                  </div>
                  <div class="form-group">
                    <label for="price-mod">Price Modifier <span style="color: red;">*</span></label>
                    <select id="price-mod"  name="price-mod" class="form-control">
                        <option value="0">- Select -</option>
                        <?php
                        foreach ($pricemod as $val) {
                            $check = '';
                            if (isset($property)) {
                                if ($property[0]->property_pricemod == $val->pricemodifier_id) {
                                    $check = 'selected="selected"';
                                }
                            }
                            echo "<option $check value=" . $val->pricemodifier_id . ">" . $val->pricemodifier_name . "</option>";
                        }
                        ?>
                    </select>
                  </div>

                  <div class="form-group">
                    <label for="reception">Receptions</label>
                    <select id="reception" name="reception" class="form-control">
                        <option value="0">N/A</option>
                        <?php
                        for ($i = 1; $i <= 10; $i++) {
                            $check = '';
                            if (isset($property)) {
                                if ($property[0]->property_reception == $i) {
                                    $check = 'selected="selected"';
                                }
                            }
                            echo "<option $check value=" . $i . ">" . $i . "</option>";
                        }
                        ?>
                    </select>
                  </div>

                  <div class="form-group">
                    <label for="heat">Heating Type</label>
                    <select id="heat"  name="heat" class="form-control">
                        <option value="0">- Select -</option>
                        <?php
                        foreach ($heattype as $val) {
                            $check = '';
                            if (isset($property)) {
                                if ($property[0]->property_heat == $val->heatingtype_id) {
                                    $check = 'selected="selected"';
                                }
                            }
                            echo "<option $check value=" . $val->heatingtype_id . ">" . $val->heatingtype_name . "</option>";
                        }
                        ?>
                    </select>
                  </div>              

                
              </div>

              <div class="col-sm-6">
                
                  <div class="form-group">
                    <label for="floor">Floors</label>
                    
                      <select id="floor" name="floor" class="form-control">
                            <option value="0">N/A</option>
                            <?php
                            for ($i = 1; $i <= 10; $i++) {
                                $check = '';
                                if (isset($property)) {
                                    if ($property[0]->property_floors == $i) {
                                        $check = 'selected="selected"';
                                    }
                                }
                                echo "<option $check value=" . $i . ">" . $i . "</option>";
                            }
                            ?>
                        </select>
                    
                  </div>
                  
                  <div class="form-group">
                    <label for="access">Accessibility</label>
                    
                    <select id="access"   name="access" class="form-control">
                        <option value="0">- Select -</option>
                        <?php
                        foreach ($access as $val) {
                            $check = '';
                            if (isset($property)) {
                                if ($property[0]->property_access == $val->accessibility_id) {
                                    $check = 'selected="selected"';
                                }
                            }
                            echo "<option $check value=" . $val->accessibility_id . ">" . $val->accessibility_name . "</option>";
                        }
                        ?>
                    </select>
                    
                  </div>

                  <div class="form-group">
                    <label for="furnish">Furnishing</label>                    
                      <select id="furnish"  name="furnish" class="form-control">
                            <option value="0">- Select -</option>
                            <?php
                            foreach ($furnish as $val) {
                                $check = '';
                                if (isset($property)) {
                                    if ($property[0]->property_furnish == $val->furnishes_id) {
                                        $check = 'selected="selected"';
                                    }
                                }
                                echo "<option $check value=" . $val->furnishes_id . ">" . $val->furnishes_name . "</option>";
                            }
                            ?>
                        </select>                   
                  </div>
                  <div class="form-group">
                    <label for="bedroom">Bedrooms</label>
                    <select id="bedroom" name="bedroom" class="form-control">
                        <option value="0">N/A</option>
                        <?php
                        for ($i = 1; $i <= 10; $i++) {
                            $check = '';
                            if (isset($property)) {
                                if ($property[0]->property_bedroom == $i) {
                                    $check = 'selected="selected"';
                                }
                            }
                            echo "<option $check value=" . $i . ">" . $i . "</option>";
                        }
                        ?>
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="bathroom">Bathrooms</label>
                    <select id="bathroom" name="bathroom" class="form-control">
                        <option value="0">N/A</option>
                        <?php
                        for ($i = 1; $i <= 10; $i++) {
                            $check = '';
                            if (isset($property)) {
                                if ($property[0]->property_bathroom == $i) {
                                    $check = 'selected="selected"';
                                }
                            }
                            echo "<option $check value=" . $i . ">" . $i . "</option>";
                        }
                        ?>
                    </select>
                  </div>  
                
              </div>

            </div>
            <div class="row tries">
              <div class="col-sm-12">
                <label for="full-describe">Summary</label>
                    <textarea id="summary" class="form-control" name="summary" maxlength="255" onkeyup="countChar(this)" rows="5"><?php echo isset($property[0]->property_summary) ? $property[0]->property_summary : ''; ?></textarea>
                    <p class="help-block">Characters remaining: 255</p>                    
              </div>

              <div class="col-sm-12">
                <label for="full-describe">Full Description <span style="color: red;">*</span></label>
                <textarea id="full-describe" class="form-control ckeditor"  name="full-describe" maxlength="4000" onkeyup="countChar1(this)" rows="5"><?php echo isset($property[0]->property_fulldesc) ? $property[0]->property_fulldesc : ''; ?></textarea>
                <!-- <p class="help-block">Characters remaining: 4000</p> -->
              </div>
            </div>  
            <div>
              <hr>
            </div>

            <div class="row">
              <div class="col-sm-12 addmorefeature">
                <span class="">Property Features</span>
              </div>
            </div>
            <div class="row">
            <?php if(!empty($outspace)){ ?>
              <div class="col-sm-4 col-md-3">
                <h5><strong>Outside Space</strong></h5>
                <?php
                foreach ($outspace as $val) {
                    $check = '';
                    if (isset($property)) {
                        $arr = explode(',', $property[0]->property_outspace);
                        if (in_array($val->outsidespace_id, $arr)) {
                            $check = 'checked';
                        }
                    }
                    echo "<div class='checkbox'><label><input type='checkbox' name='out-space[]' $check value=" . $val->outsidespace_id . " class=''>" . $val->outsidespace_name . "</label></div>";
                }
                ?>                               
              </div>
              <?php }
              if(!empty($parking)){
               ?>

              <div class="col-sm-4 col-md-3">
                <h5><strong>Parking</strong></h5>
                <?php
                foreach ($parking as $val) {
                    $check = '';
                    if (isset($property)) {
                        $arr = explode(',', $property[0]->property_parking);
                        if (in_array($val->parking_id, $arr)) {
                            $check = 'checked';
                        }
                    }
                    echo "<div class='checkbox'><label><input type='checkbox' name='parking[]' $check value=" . $val->parking_id . " class=''>" . $val->parking_name . "</label></div>";
                }
                ?>
                
              </div>
              <?php }
              if(!empty($splfeature)){
               ?>

              <div class="col-sm-4 col-md-3">
                <h5><strong>Specical Features</strong></h5>
                <?php
                foreach ($splfeature as $val) {
                    $check = '';
                    if (isset($property)) {
                        $arr = explode(',', $property[0]->property_specialfeat);
                        if (in_array($val->specialfeature_id, $arr)) {
                            $check = 'checked';
                        }
                    }
                    echo "<div class='checkbox'><label><input type='checkbox' name='special-feat[]' $check value=" . $val->specialfeature_id . ">" . $val->specialfeature_name . "</label></div>";
                }
                ?>
              </div>
              <?php } ?>
            </div>
<!-- add more features  -->
            <div class="row">
              <div class="col-sm-12">


              <div class="row">
                <div class="col-sm-12 addmorefeature">
                  <span class="">Add Extra Features</span>
                </div>
              </div>        
              


              <div class="row mt15 addFeat">
                <?php if (!isset($property[0]->property_id)){ ?>
                <div class="col-sm-6" id="addedfeature">
                <a href="" class="removeIconFeature close" data-dismiss="alert"><img src="<?php echo base_url(); ?>images/close-icon.png" class="removefeat"></a>
                  <div class="form-group">
                    <label for="key-feat">Add your features</label>                  
                    <input id="key-feat" name="key-feattitle[]" placeholder="Features" type="text" maxlength="50" class="form-control">
                  </div>
                  </div>
                <?php }else{
                    if (!empty($keyfeat)){
                        foreach ($keyfeat as $feat) {
                    ?>
                    <div class="col-sm-6" id="addedfeature">
                    <a href="" class="removeIconFeature close" data-dismiss="alert"><img src="<?php echo base_url(); ?>images/close-icon.png" class="removefeat"></a>
                    <div class="form-group">
                        <label for="key-feat">Add your features</label>                  
                        <input id="key-feat" name="key-feattitle[]" placeholder="Features" value="<?php echo $feat->keyfeature_title; ?>" type="text" maxlength="50" class="form-control">
                      </div>
                      </div>
                    <?php 
                }
                    }else{
                    ?>
                    <div class="col-sm-6" id="addedfeature">
                    <a href="" class="removeIconFeature close" data-dismiss="alert"><img src="<?php echo base_url(); ?>images/close-icon.png" class="removefeat"></a>
                    <div class="form-group">
                        <label for="key-feat">Add your features</label>                  
                        <input id="key-feat" name="key-feattitle[]" placeholder="Features" type="text" maxlength="50" class="form-control">
                      </div>
                      </div>
                    <?php
                }
                } ?>
                </div>


              <div class="row">
                <div class="col-sm-2"><button type="button" id="addfeature" class="addfeature btn btn-default">+ Add More Feature</button></div>
              </div>              
              <p style="margin-top: 10px;color: #777;">* You can add maximum 10 features</p>               
                
              </div>
            </div>
          </div>
          
        </div>

<!-- property description -->
        <!-- <div class="panel">
          <div class="panel-title">
            Property Description
          </div>

          <div class="panel-body">            
                      
            
          </div>
        </div>   -->

        <!-- Media -->
        <div class="panel media">
          <div class="panel-title">
            Property Media
          </div>
        
          <div class="panel-body">

            <div class="row">
              <div class="col-sm-12">
                  <label class="btn btn-default">
                        <i class="fa fa-plus"></i>
        <!--<i class="glyphicon glyphicon-plus"></i>-->
                        Upload Photos...
                        <input type="file" id="fileupload" name="fileupload[]" class="pimage" multiple="multiple" style="display:none;">
                    </label>
              </div>
            </div>
            <div class="row mt15">
            <div id="dvPreview"></div>
            </div>
            <div class="row mt15">
              <?php
                if (!isset($property[0]->property_id)) {
                    ?>

                    <?php
                } else {
                    foreach ($images as $img) {
                        ?>  
              <div class="col-sm-4 col-md-3 alert draggable-element">
                <figure>
                <input type="hidden" name="propimg[]" value="<?php echo $img->propimg_l; ?>"/>
                  <a><img src="<?php echo base_url(); ?>images/mainimg/<?php echo $img->propimg_l; ?>" alt="<?php echo $img->propimg_m; ?>" class="img-responsive" draggable="false">
                       
                  </a>
                  <input class="form-control" type="text" name="caption[]" value="<?php echo $img->propimg_caption; ?>" placeholder="Caption">
                  <span class="savename hidden">Photos 1</span>
                  <a href="" class="close-icon close" data-dismiss="alert"><img src="<?php echo base_url(); ?>images/close-icon.png"></a>
                </figure>
              </div>
               <?php }
               } ?>  
            </div>                  
                                    
            
          </div>
        </div>    


        <!-- document -->
        <div class="panel media document">
          <div class="panel-title">
            Property Document
          </div>

          <div class="panel-body">

            <div class="row">
              <div class="col-sm-12">
              <p><strong>Floor Plan</strong></p>
                  <input class="upload" type="file">
                  <a href="javascript:void(0)" class="btn btn-primary btn-xs floorplan_function" id="floorplan_file" data-fil="file" onclick="documentFunction('file', 'floorplan')">Add Files</a>

                  <a href="javascript:void(0)" class="btn btn-primary btn-xs floorplan_function" id="floorplan_link" data-fil="link" onclick="documentFunction('link', 'floorplan')">Add Links</a> 
              </div>
            </div>
            <div class="row mt15 floorplan_append">
              
            </div>
            <div class="row mt15">
                <?php
                if (isset($contents)) {
                    foreach ($contents as $cnelem) {
                        if ($cnelem->propcontent_type == 'floor' && $cnelem->propcontent_link == 0) {
                            ?>
                            <div class="floorplan_slot col-md-6 alert mb20 classFunction" data-funt="floorplan">
                                <div class="floorplan_slot_content" data-funt="floorplan"><a href="" class="removeIcon close-icon close" data-dismiss="alert"><img src="<?php echo base_url(); ?>images/close-icon.png"></a>
                                    <div class="form-horizontal">
                                        <div class="form-group m0 contentdiv">
                                            <input type="hidden" name="floorplanold[]" value="<?php echo $cnelem->propcontent_id; ?>"/>
                                            <label class="btn btn-default pull-left"><i class="glyphicon glyphicon-plus"></i>
                                                Choose file<input type="file" name="floorplan-file[]" onchange="readURL(this)"  style="display:none;"></label>
                                            <div class="contentview pull-left"  style="margin: 6px 0 0 10px;"><p><?php echo $cnelem->propcontent_image; ?></p></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php
                        } else if ($cnelem->propcontent_type == 'floor' && $cnelem->propcontent_link == 1) {
                            ?>
                            <div class="floorplan_slot col-md-6 mb20 classFunction" data-funt="floorplan">
                                <div class="floorplan_slot_content" data-funt="floorplan"><a href="" class="removeIcon close-icon close" data-dismiss="alert"><img src="<?php echo base_url(); ?>images/close-icon.png"></a>
                                <div class="input-group">
                                  <label class="sr-only" for="">Links</label>
                                  <input type="hidden" name="floorplanold[]" value="<?php echo $cnelem->propcontent_id; ?>"/>
                                  <div class="input-group-addon"><i class="fa fa-link"></i></div>
                                  <input type="text" name="floorplan-link[]" value="<?php echo $cnelem->propcontent_desc; ?>"class="form-control" onchange="readURL(this)" placeholder="Add Link">
                                  <a href="" class="removeIcon close-icon close" data-dismiss="alert"><img src="<?php echo base_url(); ?>images/close-icon.png"></a>
                                </div>
                                </div>
                            </div>
                            <?php
                        }
                    }
                }
                ?>
            </div>
                        
            
          </div> 
<!-- brochure -->

          <div class="panel-body">

            <div class="row">
              <div class="col-sm-12">
                <p><strong>Property Brochure</strong></p>
                <a href="javascript:void(0)" class="btn btn-primary btn-xs floorplan_function" id="broucher_file" data-fil="file" onclick="documentFunction('file', 'broucher')">Add Files</a>
                <a href="javascript:void(0)" class="btn btn-primary btn-xs floorplan_function" id="broucher_link" data-fil="link" onclick="documentFunction('link', 'broucher')">Add Links</a> 
              </div>
            </div>

            <div class="row mt15 broucher_append">
              
            </div>
            <div class="row mt15">
                <?php
                if (isset($contents)) {
                    foreach ($contents as $cnelem) {
                        if ($cnelem->propcontent_type == 'broucher' && $cnelem->propcontent_link == 0) {
                            ?>
                            <div class="broucher_slot col-md-6 mb20 classFunction" data-funt="broucher">
                                <div class="broucher_slot_content" data-funt="broucher"><a href="" class="removeIcon close-icon close" data-dismiss="alert"><img src="<?php echo base_url(); ?>images/close-icon.png"></a>
                                    <div class="form-horizontal">
                                        <div class="form-group m0 contentdiv">
                                            <input type="hidden" name="broucherold[]" value="<?php echo $cnelem->propcontent_id; ?>"/>
                                            <div class="pull-left"><i class="fs-large fa fa-file-image-o"></i></div>
                                            <label class="btn btn-default pull-left"><i class="glyphicon glyphicon-plus"></i>
                                                Choose file<input type="file" name="broucher-file[]" onchange="readURL(this)"  style="display:none;"></label>
                                            <div class="contentview pull-left"  style="margin: 6px 0 0 10px;"><p><?php echo $cnelem->propcontent_image; ?></p></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php
                        } else if ($cnelem->propcontent_type == 'broucher' && $cnelem->propcontent_link == 1) {
                            ?>
                            <div class="floorplan_slot col-md-6 mb20 classFunction" data-funt="floorplan">
                                <div class="broucher_slot_content" data-funt="broucher"><a href="" class="removeIcon close-icon close" data-dismiss="alert"><img src="<?php echo base_url(); ?>images/close-icon.png"></a>
                                <div class="input-group">
                                  <label class="sr-only" for="">Links</label>
                                  <input type="hidden" name="broucherold[]" value="<?php echo $cnelem->propcontent_id; ?>"/>
                                  <div class="input-group-addon"><i class="fa fa-link"></i></div>
                                  <input type="text" name="broucher-link[]" value="<?php echo $cnelem->propcontent_desc; ?>"class="form-control" onchange="readURL(this)" placeholder="Add Link">
                                  <a href="" class="removeIcon close-icon close" data-dismiss="alert"><img src="<?php echo base_url(); ?>images/close-icon.png"></a>
                                </div>
                                </div>
                            </div>
                            <?php
                        }
                    }
                }
                ?>
            </div>
          </div>

          <!-- virtual tour -->
          
          <div class="panel-body">

            <div class="row">
              <div class="col-sm-12">
              <p><strong>Property Virtual Tour</strong></p>
                  <a href="javascript:void(0)" class="btn btn-primary btn-xs floorplan_function" id="vitural_link" data-fil="link" onclick="documentFunction('link', 'vitural')">Add Links</a>
              </div>
            </div>
            <div class="row mt15 vitural_append">
              
            </div>
            <div class="row mt15">
                <?php
                if (isset($contents)) {
                    foreach ($contents as $cnelem) {
                        if ($cnelem->propcontent_type == 'virtual' && $cnelem->propcontent_link == 0) {
                            ?>
                            <div class="vitural_slot col-md-6 mb20 classFunction" data-funt="vitural">
                                <div class="vitural_slot_content" data-funt="vitural"><a href="" class="removeIcon close-icon close" data-dismiss="alert"><img src="<?php echo base_url(); ?>images/close-icon.png"></a>
                                    <div class="form-horizontal">
                                        <div class="form-group m0 contentdiv">
                                            <input type="hidden" name="virtualold[]" value="<?php echo $cnelem->propcontent_id; ?>"/>
                                            <div class="pull-left"><i class="fs-large fa fa-file-image-o"></i></div>
                                            <label class="btn btn-default pull-left"><i class="glyphicon glyphicon-plus"></i>
                                                Choose file<input type="file" name="vitural-file[]" onchange="readURL(this)"  style="display:none;"></label>
                                            <div class="contentview pull-left"  style="margin: 6px 0 0 10px;"><p><?php echo $cnelem->propcontent_image; ?></p></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php
                        } else if ($cnelem->propcontent_type == 'virtual' && $cnelem->propcontent_link == 1) {
                            ?>
                            <div class="vitural_slot col-md-6 mb20 classFunction" data-funt="vitural">
                                <div class="vitural_slot_content" data-funt="vitural"><a href="" class="removeIcon close-icon close" data-dismiss="alert"><img src="<?php echo base_url(); ?>images/close-icon.png"></a>
                                <div class="input-group">
                                  <label class="sr-only" for="">Links</label>
                                  <input type="hidden" name="virtualold[]" value="<?php echo $cnelem->propcontent_id; ?>"/>
                                  <div class="input-group-addon"><i class="fa fa-link"></i></div>
                                  <input type="text" name="vitural-link[]" value="<?php echo $cnelem->propcontent_desc; ?>"class="form-control" onchange="readURL(this)" placeholder="Add Link">
                                  <a href="" class="removeIcon close-icon close" data-dismiss="alert"><img src="<?php echo base_url(); ?>images/close-icon.png"></a>
                                </div>
                                </div>
                            </div>
                            <?php
                        }
                    }
                }
                ?>
            </div> 
            
                        
            
          </div>

           <!-- EPC -->
          
          <div class="panel-body">

            <div class="row">
              <div class="col-sm-12">
              <p><strong>Property EPC</strong></p>
                  <a href="javascript:void(0)" class="btn btn-primary btn-xs floorplan_function" id="epc_file" data-fil="file" onclick="documentFunction('file', 'epc')">Add Files</a>
              </div>
            </div>  
            <div class="row mt15 epc_append">
              
            </div>
              <div class="row mt15">
                  <?php
                if (isset($contents)) {
                    foreach ($contents as $cnelem) {
                        if ($cnelem->propcontent_type == 'epc' && $cnelem->propcontent_link == 0) {
                            ?>
                            <div class="epc_slot col-md-6 mb20 classFunction" data-funt="epc">
                                <div class="epc_slot_content" data-funt="epc"><a href="" class="removeIcon close-icon close" data-dismiss="alert"><img src="<?php echo base_url(); ?>images/close-icon.png"></a>
                                    <div class="form-horizontal">
                                        <div class="form-group m0 contentdiv">
                                            <input type="hidden" name="epcold[]" value="<?php echo $cnelem->propcontent_id; ?>"/>
                                            <div class="pull-left"><i class="fs-large fa fa-file-image-o"></i></div>
                                            <label class="btn btn-default pull-left"><i class="glyphicon glyphicon-plus"></i>
                                                Choose file<input type="file" name="epc-file[]" onchange="readURL(this)"  style="display:none;"></label>
                                            <div class="contentview pull-left"  style="margin: 6px 0 0 10px;"><p><?php echo $cnelem->propcontent_image; ?></p></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php
                        } else if ($cnelem->propcontent_type == 'epc' && $cnelem->propcontent_link == 1) {
                            ?>
                            <div class="epc_slot col-md-6 mb20 classFunction" data-funt="epc">
                              <div class="epc_slot_content" data-funt="epc"><a href="" class="removeIcon close-icon close" data-dismiss="alert"><img src="<?php echo base_url(); ?>images/close-icon.png"></a> 
                                <div class="input-group">
                                  <label class="sr-only" for="">Links</label>
                                  <input type="hidden" name="epcold[]" value="<?php echo $cnelem->propcontent_id; ?>"/>
                                  <div class="input-group-addon"><i class="fa fa-link"></i></div>
                                  <input type="text" name="epc-link[]" value="<?php echo $cnelem->propcontent_desc; ?>"class="form-control" onchange="readURL(this)" placeholder="Add Link">
                                  <a href="" class="removeIcon close-icon close" data-dismiss="alert"><img src="<?php echo base_url(); ?>images/close-icon.png"></a>
                                </div>
                              </div>  
                            </div>
                            <?php
                        }
                    }
                }
                ?>
              </div>            
          </div>


          <div class="panel-title">
            <div class="save-cancel">
              <button type="submit" class="btn btn-default submit">Save</button>
              <a href="<?php echo base_url(); ?>properties/property/rentlist" class="btn btn-default">Cancel</a>
            </div>
          </div>


        </div>   
        </div>
        </form>
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                      <!--<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->

                    </div>
                    <div class="modal-body" style="text-align: center">
                        <img  src="<?php echo base_url(); ?>images/ajax-loader.gif"/>
                        <h1>Please Wait ....</h1>
                    </div>

                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div>
      </section> 
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?libraries=places"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>

<script src="<?php echo base_url(); ?>js/bootstrap-datepicker.js"></script>

<script>
var baseurl = "<?php echo base_url(); ?>";
$(document).ready(function () {
    var dateToday = new Date(); 
    $('#dateavail').datepicker({minDate: dateToday});
});


var datascnt = "<?php
foreach ($content as $val) {
echo "<option value=" . $val->content_id . ">" . $val->content_name . "</option>";
}
?>";
// $('#myForm').submit(function (e) {
// // Stop the form from actually submitting.
//     e.preventDefault();
//     var formData = new FormData(this);
//     for (var i = 0, file; file = filesArray[i]; i++) {
//         formData.append('file[]', file);
//     }

// // Send the ajax request.
//     $.ajax({
//         url: baseurl + "properties/property/addrentProp",
//         type: 'POST',
//         data: formData,
//         mimeType: "multipart/form-data",
//         processData: false,
//         contentType: false,
//         success: function (response) {
//             $('#myModal').modal('hide');
//             window.location.href = baseurl + "properties/property/rentlist";
//         }
//     });
// });

function getFeesapplyDetail(val){
  if(val == 3){
    $('.feesapply_detail').show();
  }else{
    $('.feesapply_detail').hide();
  }
}
</script>
<script src="<?php echo base_url(); ?>js/prop_script.js"></script>