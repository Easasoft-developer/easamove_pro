<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//print_r($this->session->userdata());
//echo $forata['typestatus'];
$access = $this->session->userdata['agentuser']['access'];
if (!empty($forata)) {
    $_SESSION["typestatus"] = (isset($forata['typestatus'])) ? $forata['typestatus'] : $_SESSION["typestatus"];
    $_SESSION["searchtype"] = (isset($forata['searchtype'])) ? $forata['searchtype'] : $_SESSION["searchtype"];
} else {
    $_SESSION["typestatus"] = "";
    $_SESSION["searchtype"] = "";
}
?>
<style type="text/css">
    .panel-heading {
        padding: 25px 15px;
        border-bottom: 1px solid #ebebeb; }
    .successUpdate{
        position: absolute;
        right: 30px;
        width: 50%;
        top: 13px;
    }
</style>
<!-- main content -->
<section id="main-content" class="main-content">
    <section class="surround1">



        <div class="container-fluid property_listing">
            <div class="row">
                <div class="col-xs-12">
                    <div class="panel">
                        <div class="panel-heading">


                            <div class="row">
                                <div class="col-sm-12">
                                    <h3>Properties For Sale</h3>
                                </div>
                            </div>
                            <?php
                            if ($this->session->flashdata('propertyAdd')) {
                                ?>
                                <div class="successUpdate alert alert-success" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                    <i class="fa fa-check"></i> <?php echo $this->session->flashdata('propertyAdd'); ?>
                                </div>
                                <?php
                            }
                            ?>
                            <form id="submitForm" method="post" action="<?php echo base_url(); ?>properties/Property">
                                <div class="row sel-sear-btn">
                                    <div class="col-lg-onehalf col-md-2 col-sm-2 col-xs-12">
                                        <div class="form-group">
                                            <select class="form-control" onchange="SubmitFormElement();" name="status">
                                                <option value="All" <?php if ($_SESSION["typestatus"] == 'All') {
                                echo "selected='selected'";
                            } ?>>All</option>
                                                <option value="Public" <?php if ($_SESSION["typestatus"] == 'Public') {
                                echo "selected='selected'";
                            } ?>>Public</option>
                                                <option value="Private" <?php if ($_SESSION["typestatus"] == 'Private') {
                                echo "selected='selected'";
                            } ?>>Private</option>
                                                <option value="Deleted" <?php if ($_SESSION["typestatus"] == 'Deleted') {
                                echo "selected='selected'";
                            } ?>>Deleted</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-5 col-sm-10 col-xs-10 pr0 pl0">
                                        <div class="form-group">
                                            <input type="text" class="form-control search-prop" name="search" onchange="SubmitFormElement();" placeholder="Search here" value="<?php echo $_SESSION["searchtype"]; ?>">
                                        </div>
                                    </div>
                                    <div class="p0 col-md-1 col-sm-2">
                                        <a class="s-color search btn btn-default search_show" href="javascript:;" onclick="SubmitFormElement();" style="font-size: 14px;border-left: 0;">Search</a>
                                    </div>
                                    <div class="col-lg-fivehalf col-md-12 hidden-xs pull-right">
                                        <div class="btn-group">
                                            <?php
                                            if ($access == 1 || $access == 2) {
                                                ?>
                                                <a href="<?php echo base_url('properties/property/sale'); ?>" class="btn btn-default">
                                                    <i class="glyphicon glyphicon-plus"></i><span class="hidden-xs">Add New Listing</span>
                                                </a>
    <?php
}
?>
                                            <a href="<?php echo base_url('properties/property/showmap/1'); ?>" class="btn btn-default">
                                                <i class="fa fa-map-o"></i> <span class="hidden-xs">Show Map</span>
                                            </a>
                                            <a class="btn btn-default" href="javascript:;" onclick="printpage();" id="printpdf"><i class="fa fa-print"></i><span class="hidden-xs">Print</span></a>
                                            <a class="btn btn-default" href="javascript:;" onclick="downloadpdf()"><i class="glyphicon glyphicon-download-alt"></i><span class="hidden-xs">Save as PDF</span></a>
                                            <a class="btn btn-default" href="javascript:;" onclick="exportUser()"><i class="fa fa-external-link"></i><span class="hidden-xs">Export to Excel</span></a>
                                        </div>
                                    </div>

                                    <div class="btn-group col-xs-2 hidden-sm hidden-md hidden-lg">
                                        <button type="button" class="btn dropdown-toggle pull-right" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fa fa-ellipsis-v fa-lg"></i>
                                            <span class="sr-only">Toggle Dropdown</span>
                                        </button>
                                        <ul class="dropdown-menu">
                                            <li>
                                                <?php
                                                if ($access == 1 || $access == 2) {
                                                    ?>
                                                    <a href="<?php echo base_url('properties/property/sale'); ?>" class="btn btn-default btn-sm">
                                                        <i class="glyphicon glyphicon-plus"></i> <span>Add New Listing</span>
                                                    </a>
    <?php
}
?>
                                            </li>
                                            <li><a href="<?php echo base_url('properties/property/showmap/1'); ?>"><i class="fa fa-map"></i> <span>Show Map</span></a></li>
                                            <li><a href="javascript:;" onclick="printpage()" id="printpdf"><i class="fa fa-print"></i> <span>Print</span></a></li>
                                            <li><a href="javascript:;" onclick="downloadpdf()"><i class="glyphicon glyphicon-download-alt"></i> <span>Save as PDF</span></a></li>
                                            <li><a href="javascript:;" onclick="exportUser()"><i class="fa fa-external-link"></i> <span>Export to Excel</span></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </form>

                        </div>
                        <div class="">
                            <div class="table-responsive">
                                <table class="table table-striped table-hover" id="editable-sample">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Image</th>
                                            <th>Reference</th>
                                            <th>Price(£)</th>
                                            <th>Address</th>
                                            <th>Type</th>                            
                                            <th>Listed</th>
                                            <th class="reduce">Listing Status</th>
                                            <th class="reduce">Last Updated</th>
                                            <th class="reduce">Photos/Floor Plans</th>
                                            <th class="reduce">Site Status</th>
<!--                                            <th>Edit</th>-->
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <?php
                                        if (!empty($saleproperty)) {
                                            if (!empty($links)) {
                                                $i = 1 + ($this->pagination->cur_page - 1) * $this->pagination->per_page;
                                            } else {
                                                $i = 1;
                                            }
                                            //$i=$this->uri->segment(4);

                                            foreach ($saleproperty as $key => $sale) {
                                                if($sale->property_pstatus==1){
                                            $repl=array(' ',',','_');
                                            $proptyurl='http://demo.easamove.co.uk/property-for-sale/'.strtolower(str_replace($repl,'-',$sale->property_name)).'/'.$sale->property_id.'/'.strtolower(str_replace($repl,'-',$sale->property_sname));
                                            }else{
                                                $proptyurl='javscript:;';
                                            }
                                                ?>
                                                <tr class="">
                                                    <td><?php echo $i; ?></td>
                                                    <td><a target="_blank" href="<?php echo $proptyurl;?>"><img src="http://pro.easamove.co.uk/images/mainimg/<?php echo $sale->propimg_s; ?>"/></a></td>
                                                    <td><a href="<?php echo base_url('properties/property/propEdit') . '/' . $sale->property_id ?>"><?php echo $sale->property_ownrefer; ?></a></td>
                                                    <td><a href="<?php echo base_url('properties/property/propEdit') . '/' . $sale->property_id ?>">£<?php echo $sale->property_price; ?></a></td>

                                                    <td><a href="<?php echo base_url('properties/property/propEdit') . '/' . $sale->property_id ?>"><?php echo $sale->property_sname; ?></a></td>
                                                    <td><a href="<?php echo base_url('properties/property/propEdit') . '/' . $sale->property_id ?>"><?php echo $sale->propertytype_name; ?></a></td>

                                                    <td><a href="<?php echo base_url('properties/property/propEdit') . '/' . $sale->property_id ?>"><?php echo date('d-m-Y', strtotime($sale->property_created_on)); ?></a></td>
                                                    <td><a href="<?php echo base_url('properties/property/propEdit') . '/' . $sale->property_id ?>"><?php echo $sale->listingstatus_name; ?></a></td>
                                                    <td><a href="<?php echo base_url('properties/property/propEdit') . '/' . $sale->property_id ?>"><?php echo date('d-m-Y', strtotime($sale->property_modified_on)); ?></a></td>
                                                    <td><a href="<?php echo base_url('properties/property/propEdit') . '/' . $sale->property_id ?>"><?php echo $sale->imgcnt . ' / ' . $sale->floorcnt . ' / ' . $sale->epc ?></a></td>
                                                    <td><a href="<?php echo base_url('properties/property/propEdit') . '/' . $sale->property_id ?>"><?php echo $sale->sitestatus_name; ?></a></td>

                                                    <!--<td><a href="<?php // echo base_url('properties/property/propEdit') . '/' . $sale->property_id ?>" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></a></td>-->
                                                </tr>
        <?php
        $i++;
    }
} else {
    ?>
                                            <tr><td colspan="7">No Record Found</td></tr>
                                                <?php }
                                            ?>
                                    </tbody>
                                    <tfoot>
                                        <tr>

<?php
//echo $linkCount;
if ($linkCount <= 1) {
    ?>


                                                <td colspan="3"> 
                                                    <div class="btn-group">
    <?php echo $countprop; ?> Results
                                                    </div>
                                                </td>
                                            <?php
                                            } else {
                                                $ifor = 1 + ($this->pagination->cur_page - 1) * $this->pagination->per_page;
                                                ?>
                                                <td colspan="3"> 
                                                    <div class="btn-group">
    <?php echo $ifor . " - " . ($i - 1) . " of " . $countprop; ?> Results
                                                    </div>
                                                </td>
    <?php }
?>
                                            <td colspan="11">
<?php echo $links; ?>
                                            </td>
                                        </tr>
                                    </tfoot>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>    
            </div>
        </div>

    </section>        
</section>



<!--<embed id="exPDF" src ="" width="550" height="550" name="blabla" onload="window.print()">-->
<!--<object id="exPDF" type="application/pdf" data="" width="100%" height="500"/>-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>
<script>
                      function SubmitFormElement() {
                          $('#submitForm').submit();
                      }
                      var base_url = '<?php echo base_url(); ?>';
                      // var row = text = offset = sel = '';

                      // $(document).ready(function () {
                      //     sel = $('#status').val();
                      //     getUser(base_url, row = 10, text = '', offset = 1, sel);
                      // });
                      // function getUser(base_url, row, text, offset, sel) {
                      //     $('table .data-append').remove();
                      //     $('table .foot').remove();
                      //     $.ajax({
                      //         type: "POST",
                      //         url: base_url + "properties/property/getprop",
                      //         data: 'row=' + row + '&text=' + text + '&offset=' + offset + '&sel=' + sel,
                      //         success: function (data) {
                      //             $('table .data-append').remove();
                      //             $('table .foot').remove();
                      //             if (data != '') {

                      //                 $('thead').after(data);
                      //                 $('.foot').css('display', '');
                      //             } else {
                      //                 $('thead').after('No Data');
                      //                 $('.foot').css('display', 'none');
                      //             }
                      //         }
                      //     });
                      // }
                      // $('#status').on('change', function () {
                      //     sel = $('#status').val();
                      //     getUser(base_url, row = 10, text, offset = 1, sel);
                      // });


                      // $('table').on('click', '.rowscnt', function () {
                      //     $('table .rowscnt').removeClass('active');
                      //     row = $(this).text();
                      //     $(this).addClass('active');
                      //     text = $('#search').val();
                      //     sel = $('#status').val();
                      //     getUser(base_url, row, text, offset = 1, sel);
                      //     console.log($(this).text());
                      // });
                      // $('.search').on('click', function () {
                      //     text = $('#search').val();
                      //     if (text.length > 3) {
                      //         sel = $('#status').val();
                      //         getUser(base_url, row = 10, text, offset = 1, sel);
                      //     }
                      // });
                      // $('#search').on('keypress', function () {
                      //     text = $('#search').val();
                      //     if (text.length > 3 ) {
                      //         sel = $('#status').val();
                      //         getUser(base_url, row = 10, text, offset = 1, sel);
                      //     }
                      // });
                      // $('.table').on('click', '.offset', function () {
                      //     $('table .offset').closest('li').removeClass('active');

                      //     offset = $(this).text();
                      //     sel = $('#status').val();
                      //     getUser(base_url, row = 10, text = '', offset, sel);

                      //     $(this).closest('li').addClass('active');


                      // });
                      // $('.table').on('click', '.next', function () {


                      //     offset = $(this).closest('ul').find('li.active').find('a').text();
                      //     console.log(offset);
                      //     var next = offset++;
                      //     console.log(next);
                      //     sel = $('#status').val();
                      //     getUser(base_url, row = 10, text = '', next, sel);

                      // });
                      // $('.table').on('click', '.prev', function () {


                      //     offset = $(this).closest('ul').find('li.active').find('a').text();
                      //     console.log(offset);
                      //     var prev = offset--;
                      //     sel = $('#status').val();
                      //     getUser(base_url, row = 10, text = '', prev, sel);

                      // });
                      function exportUser() {
                          var sel = $('#status').val();
                          var url = base_url + "properties/property/export/1/" + sel;
                          window.open(url, '_blank');

                      }

                      function downloadpdf() {
                          var sel = $('#status').val();
                          var url = base_url + "properties/property/export";
                          var userid = "<?php echo $this->session->userdata['agentuser']['id']; ?>";
                          var today = new Date();
                          var dd = today.getDate();
                          var mm = today.getMonth() + 1; //January is 0!

                          var yyyy = today.getFullYear();
                          if (dd < 10) {
                              dd = '0' + dd
                          }
                          if (mm < 10) {
                              mm = '0' + mm
                          }
                          var today = yyyy + '-' + mm + '-' + dd;
                          var openurl = base_url + '/images/property_' + userid + '_' + today + '.pdf';
//                          var openurl = base_url + '/images/property.pdf';
                          $.ajax({
                              url: base_url + "properties/property/downpdf",
                              type: "POST",
                              data: 'type=1&sel=' + sel,
                              success: function (data) {
                                  window.open(openurl, '_blank');
                              }
                          });

                      }
                      //printpage();
                      function printpage() {
                          var sel = $('#status').val();
                          var url = base_url + "properties/property/export";
                          var userid = "<?php echo $this->session->userdata['agentuser']['id']; ?>";
                          var today = new Date();
                          var dd = today.getDate();
                          var mm = today.getMonth() + 1; //January is 0!

                          var yyyy = today.getFullYear();
                          if (dd < 10) {
                              dd = '0' + dd
                          }
                          if (mm < 10) {
                              mm = '0' + mm
                          }
                          var today = yyyy + '-' + mm + '-' + dd;
                          var openurl = base_url + '/images/property_' + userid + '_' + today + '.pdf';
                          //var openurl = base_url + '/images/property_'+userid+'.pdf';
                          $.ajax({
                              url: base_url + "properties/property/downpdf",
                              type: "POST",
                              data: 'type=1&sel=' + sel,
                              success: function (data) {

                                  window.open(openurl, '_blank');
                              }
                          });
//                                        window.print();
                      }
                      $(window).load(function () {
                          console.log("Load");
                          setTimeout(function () {
                              $('.successUpdate').fadeOut('2000')
                          }, 3000);
                      })
</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
