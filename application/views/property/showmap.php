<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?>
<!-- main content -->
<section id="main-content" class="main-content" style="">
    <section class="surround">

        <div class="panel sale-rent">
            <div class="panel-heading">Properties</div>

            <div class="panel-body">
                <div class="adv-table editable-table ">
                    <div class="row">
<!--                        <div class="col-md-2 col-sm-6">
                            <div class="form-group">
                                <select class="form-control input-sm" id="status">
                                    <option value="0">All</option>
                                    <option value="Public">Public</option>
                                    <option value="Private">Private</option>
                                    <option value="Deleted">Deleted</option>
                                </select>
                            </div>

                        </div>-->
<!--                        <div class="col-md-4 col-sm-6">


                            <div class="form-group">
                                <label class="sr-only" for="search">Search</label>
                                <div class="input-group">

                                    <input type="text" class="form-control input-sm" id="search" placeholder="Search">
                                    <div class="input-group-addon"><a class="s-color search" href="javascript:;"><i class="fa fa-search"></i></a></div>
                                </div>
                            </div>                                          


                        </div>-->
                        <div class="col-md-3 col-sm-6">
                            <div class="btn-group" style="margin-bottom:10px">
                                
                                <a href="javascript:void(0);" onclick="history.go(-1)" class="btn btn-default btn-sm">
                                    <i class="fa fa-map-o"></i>List View
                                </a>
                            </div>
                        </div>
                        
                    </div>

                    <div class="space15"></div>
                    <div class="table-responsive" id="map" style="height: 500px;">

                    </div>

                </div>
            </div>

    </section>        
</section>
 <script language="javascript" src="https://maps.googleapis.com/maps/api/js?sensor=true&v=3"></script>
     <script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
     <script src="http://code.jquery.com/jquery-migrate-1.1.1.min.js"></script>
<script>
//                                        initialize();
var baseurl = '<?php echo base_url(); ?>';
                                        function initialize() {
                                            var latitude = 12.87,
                                                    longitude = 24.20,
                                                    radius = 8000, //how is this set up
                                                    center = new google.maps.LatLng(latitude, longitude),
                                                    bounds = new google.maps.Circle({center: center, radius: radius}).getBounds(),
                                                    mapOptions = {
                                                        center: center,
                                                        zoom: 13,
                                                        mapTypeId: google.maps.MapTypeId.ROADMAP,
                                                        scrollwheel: true
                                                    };

                                            var map = new google.maps.Map(document.getElementById("map"), mapOptions);

                                            setMarkers(center, radius, map);
                                        }

                                        function setMarkers(center, radius, map) {
                                            var json = (function () {
                                                var json = null;
                                                $.ajax({
                                                    'async': false,
                                                    'global': false,
                                                    'url': "<?php echo base_url();?>properties/property/getmarker",
                                                    'data':"proptype=<?php echo $prop_type;?>",
                                                    'dataType': "json",
                                                    success: function (data) {
                                                        console.log(data.length);
                                                        json = data;
                                                        console.log(json);
                                                        if(json!=''){
                                                        map.setCenter(new google.maps.LatLng(json[0].lat,json[0].long));
                                                    }else{
                                                        $('#map').hide();
                                                        $('.panel-body').html('<h3>No Property Available</h3>')
                                                    }
                                                    }
                                                });
                                                return json;
                                            })();

                                            var circle = new google.maps.Circle({
                                                strokeColor: '#000000',
                                                strokeOpacity: 0.25,
                                                strokeWeight: 1.0,
                                                fillColor: '#ffffff',
                                                fillOpacity: 0.1,
                                                clickable: false,
                                                map: map,
                                                center: center,
                                                radius: radius
                                            });
                                            var bounds = circle.getBounds();

                                            //loop between each of the json elements
                                            console.log(json.length);
                                            for (var i = 0, length = json.length; i < length; i++) {
                                                var data = json[i],
                                                        latLng = new google.maps.LatLng(data.lat, data.long);



//                                                if (bounds.contains(latLng)) {
                                                    // Creating a marker and putting it on the map
                                                    console.log(latLng);
                                                    var marker = new google.maps.Marker({
                                                        position: latLng,
                                                        map: map,
                                                        title: data.address,
                                                        icon: baseurl + 'images/map-icon.png',
                                                    });
                                                    infoBox(map, marker, data);
//                                                }else{
//                                                      console.log(latLng);
//                                                }
                                            }
                                            var latLng = marker.getPosition(); // returns LatLng object
                                            map.setCenter(latLng);
                                            circle.bindTo('center', marker, 'position');

                                        }

                                        function infoBox(map, marker, data) {
                                            var infoWindow = new google.maps.InfoWindow();
                                            // Attaching a click event to the current marker
                                            google.maps.event.addListener(marker, "click", function (e) {
                                                infoWindow.setContent(data.address);
                                                infoWindow.open(map, marker);
                                            });

                                            // Creating a closure to retain the correct data 
                                            // Note how I pass the current data in the loop into the closure (marker, data)
                                            (function (marker, data) {
                                                // Attaching a click event to the current marker
                                                google.maps.event.addListener(marker, "click", function (e) {
                                                   var locstret='<div id="content" style="width:200px;"><div class="wrap" style="padding: 10px;">' +
                                                           '<h3>'+data.pname+'</h3>' +
                                                        '<p>'+data.address+'</p></div>' +
                                                        '<img src="<?php echo base_url();?>images/mainimg/'+data.image+'" class="img-responsive"/>'+
                                                        '</div>';
                                                    infoWindow.setContent(locstret);
                                                    infoWindow.open(map, marker);
                                                    style_infowindow();
                                                });
                                            })(marker, data);
                                        }
                                        function style_infowindow() {
                                            var iwOuter = $('.gm-style-iw');

                                            /* Since this div is in a position prior to .gm-div style-iw.
                                             * We use jQuery and create a iwBackground variable,
                                             * and took advantage of the existing reference .gm-style-iw for the previous div with .prev().
                                             */
                                            var iwBackground = iwOuter.prev();

                                            // Removes background shadow DIV
                                            iwBackground.children(':nth-child(2)').css({'display': 'none'});

                                            // Removes white background DIV
                                            iwBackground.children(':nth-child(4)').css({'display': 'none'});

                                            // Moves the infowindow 115px to the right.
                                            iwOuter.parent().parent().css({left: '18px'});
                                            iwOuter.find('div:nth-child(1)').css({'width': '100%', 'max-width': 'unset', 'max-height': 'unset'});

                                            // Moves the shadow of the arrow 76px to the left margin.
                                            iwBackground.children(':nth-child(1)').css('display', 'none');

                                            // Moves the arrow 76px to the left margin.
                                            iwBackground.children(':nth-child(3)').attr('style', function (i, s) {
                                                return s + 'left: 95px !important;'
                                            });

                                            iwBackground.children(':nth-child(3)').find('div:nth-child(1)').children(':nth-child(1)').css('display', 'none');

                                            iwBackground.children(':nth-child(3)').find('div:nth-child(2)').css({'z-index': '1', 'top': '18px', 'left': '-18px', 'height': '10px', 'width': '16px'});

                                            // Changes the desired tail shadow color.
                                            iwBackground.children(':nth-child(3)').find('div:nth-child(2)').children(':nth-child(1)').css({'z-index': '-1', 'transform': 'none', 'height': '0px', 'width': '0px', 'border': '8px solid rgb(122, 122, 122)', 'top': '-1px', 'border-bottom': '0', 'border-right-color': 'transparent', 'border-left-color': 'transparent', 'background': 'transparent', 'box-shadow': 'none'});

                                            // Reference to the div that groups the close button elements.
                                            var iwCloseBtn = iwOuter.next();

                                            // Apply the desired effect to the close button
                                            iwCloseBtn.css({opacity: '1', width:'15px', height:'15px', right: '40px', top: '26px', border: '1px solid rgb(255, 255, 255)', 'border-radius': '13px', 'box-shadow': '-1px 2px 15px 1px rgb(52, 52, 52)'});

                                            // The API automatically applies 0.7 opacity to the button after the mouseout event. This function reverses this event to the desired value.
                                            iwCloseBtn.mouseout(function () {
                                                $(this).css({opacity: '1'});
                                            });
                                        }
  google.maps.event.addDomListener(window, 'load', initialize);
</script>


