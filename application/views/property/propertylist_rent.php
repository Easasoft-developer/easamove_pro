<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$access = $this->session->userdata['agentuser']['access'];
if (!empty($forata)) {
    $_SESSION["typestatus"] = (isset($forata['typestatus'])) ? $forata['typestatus'] : $_SESSION["typestatus"];
    $_SESSION["searchtype"] = (isset($forata['searchtype'])) ? $forata['searchtype'] : $_SESSION["searchtype"];
} else {
    $_SESSION["typestatus"] = "";
    $_SESSION["searchtype"] = "";
}
?>
<style type="text/css">
    .panel-heading {
        padding: 25px 15px;
        border-bottom: 1px solid #ebebeb; }
    .successUpdate{
        position: absolute;
        right: 30px;
        width: 50%;
        top: 13px;
    }
</style>
<section id="main-content" class="main-content">
    <section class="surround1">



        <div class="container-fluid property_listing">
            <div class="row">
                <div class="col-xs-12">
                    <div class="panel">
                        <div class="panel-heading">


                            <div class="row">
                                <div class="col-sm-12">
                                    <h3>Properties For Rent</h3>
                                </div>
                            </div>
                            <?php
                            if ($this->session->flashdata('propertyAdd')) {
                                ?>
                                <div class="successUpdate alert alert-success" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                    <i class="fa fa-check"></i> <?php echo $this->session->flashdata('propertyAdd'); ?>
                                </div>
                                <?php
                            }
                            ?>
                            <form id="submitForm" method="post" action="<?php echo base_url(); ?>properties/Property/rentlist">
                                <div class="row sel-sear-btn">
                                    <div class="col-lg-onehalf col-md-2 col-sm-2 col-xs-12">
                                        <div class="form-group">
                                            <select class="form-control" onchange="SubmitFormElement();" name="status" id="status">
                                                <option value="All" <?php if ($_SESSION["typestatus"] == 'All') {
                                echo "selected='selected'";
                            } ?>>All</option>
                                                <option value="Public" <?php if ($_SESSION["typestatus"] == 'Public') {
                                echo "selected='selected'";
                            } ?>>Public</option>
                                                <option value="Private" <?php if ($_SESSION["typestatus"] == 'Private') {
                                echo "selected='selected'";
                            } ?>>Private</option>
                                                <option value="Deleted" <?php if ($_SESSION["typestatus"] == 'Deleted') {
                                echo "selected='selected'";
                            } ?>>Deleted</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-5 col-sm-10 col-xs-10 pr0 pl0">
                                        <div class="form-group">
                                            <input type="text" class="form-control search-prop" name="search" placeholder="Search here" value="<?php echo $_SESSION["searchtype"]; ?>">
                                        </div>
                                    </div>
                                    <div class="p0 col-md-1 col-sm-2">
                                        <a class="s-color search btn btn-default search_show" href="javascript:;" onclick="SubmitFormElement();" style="font-size: 14px;border-left: 0;">Search</a>
                                    </div>
                                    <div class="col-lg-fivehalf col-md-12 hidden-xs">
                                        <div class="btn-group">
                                            <?php
                                            if ($access == 1 || $access == 2) {
                                                ?>
                                                <a href="<?php echo base_url('properties/property/rent'); ?>" class="btn btn-default">
                                                    <i class="glyphicon glyphicon-plus"></i><span class="hidden-xs">Add New Listing</span>
                                                </a>
    <?php
}
?>
                                            <a href="<?php echo base_url('properties/property/showmap/2'); ?>" class="btn btn-default">
                                                <i class="fa fa-map-o"></i> <span class="hidden-xs">Show Map</span>
                                            </a>
                                            <a class="btn btn-default" href="javascript:;" onclick="printpage();" id="printpdf"><i class="fa fa-print"></i><span class="hidden-xs">Print</span></a>
                                            <a class="btn btn-default" href="javascript:;" onclick="downloadpdf()"><i class="glyphicon glyphicon-download-alt"></i><span class="hidden-xs">Save as PDF</span></a>
                                            <a class="btn btn-default" href="javascript:;" onclick="exportUser()"><i class="fa fa-external-link"></i><span class="hidden-xs">Export to Excel</span></a>
                                        </div>
                                    </div>

                                    <div class="btn-group col-xs-2 hidden-sm hidden-md hidden-lg">
                                        <button type="button" class="btn dropdown-toggle pull-right" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fa fa-ellipsis-v fa-lg"></i>
                                            <span class="sr-only">Toggle Dropdown</span>
                                        </button>
                                        <ul class="dropdown-menu">
                                            <li>
                                                <?php
                                                if ($access == 1 || $access == 2) {
                                                    ?>
                                                    <a href="<?php echo base_url('properties/property/rent'); ?>" class="btn btn-default btn-sm">
                                                        <i class="glyphicon glyphicon-plus"></i> <span>Add New Listing</span>
                                                    </a>
    <?php
}
?>
                                            </li>
                                            <li><a href="<?php echo base_url('properties/property/showmap/2'); ?>"><i class="fa fa-map"></i> <span>Show Map</span></a></li>
                                            <li><a href="javascript:;" onclick="printpage();" id="printpdf"><i class="fa fa-print"></i> <span>Print</span></a></li>
                                            <li><a href="javascript:;" onclick="downloadpdf()"><i class="glyphicon glyphicon-download-alt"></i> <span>Save as PDF</span></a></li>
                                            <li><a href="javascript:;" onclick="exportUser()"><i class="fa fa-external-link"></i> <span>Export to Excel</span></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </form>

                        </div>
                        <div class="">
                            <div class="table-responsive">
                                <table class="table table-striped table-hover" id="editable-sample">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Image</th>
                                            <th>Reference</th>
                                            <th>Price(£)</th>
                                            <th>Address</th>
                                            <th>Type</th>                            
                                            <th>Listed</th>
                                            <th class="reduce">Listing Status</th>
                                            <th class="reduce">Last Updated</th>
                                            <th class="reduce">Photos/Floor Plans</th>
                                            <th class="reduce">Site Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        if (!empty($rentproperty)) {
                                            if (!empty($links)) {
                                                $i = 1 + ($this->pagination->cur_page - 1) * $this->pagination->per_page;
                                            } else {
                                                $i = 1;
                                            }

                                            foreach ($rentproperty as $key => $rent) {
                                                 if($rent->property_pstatus==1){
                                                $repl=array(' ',',','_');
                                                $proptyurl='http://demo.easamove.co.uk/property-to-rent/'.strtolower(str_replace($repl,'-',$rent->property_name)).'/'.$rent->property_id.'/'.strtolower(str_replace($repl,'-',$rent->property_sname));
                                                 }else{
                                                      $proptyurl='javscript:;';
                                                 }
                                                ?>
                                                <tr class="">
                                                    <td><?php echo $i; ?></td>
                                                    <td><a target="_blank" href="<?php echo $proptyurl;?>"><img src="http://pro.easamove.co.uk/images/mainimg/<?php echo $rent->propimg_s; ?>"/></a></td>
                                                    <td><a href="<?php echo base_url('properties/property/proprentEdit') . '/' . $rent->property_id ?>"><?php echo $rent->property_ownrefer; ?></a></td>
                                                    <td><a href="<?php echo base_url('properties/property/proprentEdit') . '/' . $rent->property_id ?>">£<?php echo $rent->property_price; ?></a></td>

                                                    <td><a href="<?php echo base_url('properties/property/proprentEdit') . '/' . $rent->property_id ?>"><?php echo $rent->property_sname; ?></a></td>
                                                    <td><a href="<?php echo base_url('properties/property/proprentEdit') . '/' . $rent->property_id ?>"><?php echo $rent->propertytype_name; ?></a></td>

                                                    <td><a href="<?php echo base_url('properties/property/proprentEdit') . '/' . $rent->property_id ?>"><?php echo date('d-m-Y', strtotime($rent->property_created_on)); ?></a></td>
                                                    <td><a href="<?php echo base_url('properties/property/proprentEdit') . '/' . $rent->property_id ?>"><?php echo $rent->listingstatus_name; ?></a></td>
                                                    <td><a href="<?php echo base_url('properties/property/proprentEdit') . '/' . $rent->property_id ?>"><?php echo date('d-m-Y', strtotime($rent->property_modified_on)); ?></a></td>
                                                    <td><a href="<?php echo base_url('properties/property/proprentEdit') . '/' . $rent->property_id ?>"><?php echo $rent->imgcnt . ' / ' . $rent->floorcnt . ' / ' . $rent->epc ?></a></td>
                                                    <td><a href="<?php echo base_url('properties/property/proprentEdit') . '/' . $rent->property_id ?>"><?php echo $rent->sitestatus_name; ?></a></td>

                                                    
                                                </tr>
        <?php
        $i++;
    }
} else {
    ?>
                                            <tr><td colspan="7">No Record Found</td></tr>
                                                <?php }
                                            ?>
                                    </tbody>
                                    <tfoot>
                                        <tr>

                                                    <?php
                                                    if ($linkCount <= 1) {
                                                        ?>


                                                <td colspan="3"> 
                                                    <div class="btn-group">
                                                        <?php echo $countprop; ?> Results
                                                    </div>
                                                </td>
                                            <?php
                                            } else {
                                                $ifor = 1 + ($this->pagination->cur_page - 1) * $this->pagination->per_page;
                                                ?>
                                                <td colspan="3"> 
                                                    <div class="btn-group">
    <?php echo $ifor . " - " . ($i - 1) . " of " . $countprop; ?> Results
                                                    </div>
                                                </td>
    <?php }
?>
                                            <td colspan="11">
<?php echo $links; ?>
                                            </td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>    
            </div>
        </div>

    </section>        
</section>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>
<script>
                      function SubmitFormElement() {
                          $('#submitForm').submit();
                      }
                      var base_url = '<?php echo base_url(); ?>';
                     
                      function exportUser() {
                          var sel = $('#status').val();
                          var url = base_url + "properties/property/export/2/" + sel;
                          window.open(url, '_blank');

                      }

                      function downloadpdf() {
                          var sel = $('#status').val();
                          var url = base_url + "properties/property/export";
                          var userid = "<?php echo $this->session->userdata['agentuser']['id']; ?>";
                          var today = new Date();
                          var dd = today.getDate();
                          var mm = today.getMonth() + 1; //January is 0!

                          var yyyy = today.getFullYear();
                          if (dd < 10) {
                              dd = '0' + dd
                          }
                          if (mm < 10) {
                              mm = '0' + mm
                          }
                          var today = yyyy + '-' + mm + '-' + dd;
                          var openurl = base_url + '/images/property_' + userid + '_' + today + '.pdf';
                          $.ajax({
                              url: base_url + "properties/property/downpdf",
                              type: "POST",
                              data: 'type=2&sel=' + sel,
                              success: function (data) {
                                  window.open(openurl, '_blank');
                              }
                          });

                      }
                      //printpage();
                      function printpage() {
                          var sel = $('#status').val();
                          var url = base_url + "properties/property/export";
                          var userid = "<?php echo $this->session->userdata['agentuser']['id']; ?>";
                          var today = new Date();
                          var dd = today.getDate();
                          var mm = today.getMonth() + 1; //January is 0!

                          var yyyy = today.getFullYear();
                          if (dd < 10) {
                              dd = '0' + dd
                          }
                          if (mm < 10) {
                              mm = '0' + mm
                          }
                          var today = yyyy + '-' + mm + '-' + dd;
                          var openurl = base_url + '/images/property_' + userid + '_' + today + '.pdf';

                          $.ajax({
                              url: base_url + "properties/property/downpdf",
                              type: "POST",
                              data: 'type=2&sel=' + sel,
                              success: function (data) {

                                  window.open(openurl, '_blank');
                              }
                          });
//                                        window.print();
                      }
                      $(window).load(function () {
                          console.log("Load");
                          setTimeout(function () {
                              $('.successUpdate').fadeOut('2000')
                          }, 3000);
                      })
</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
