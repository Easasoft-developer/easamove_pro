<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<style type="text/css">
    .panel-heading {
  padding: 25px 15px;
  border-bottom: 1px solid #ebebeb; }
  .panel-body {
    padding: 15px;
}
</style>
<!-- main content -->
<section id="main-content" class="main-content">
<section class="surround1">



<div class="container-fluid user-settings">
<!-- activity reports -->
  <div class="row">
      <div class="col-xs-12">

        <div class="panel">
          <div class="panel-heading">
            

            <div class="row">
              <div class="col-sm-12">
                <h3>Activity Reports</h3>
              </div>
            </div>


            <div class="row sel-sear-btn">
              
              <div class="col-md-3 col-sm-4 col-xs-10">
                <div class="form-group">
                <input  type="text" class="form-control" name="seldate" id="reportrange" placeholder="Select the date">
              </div>
              </div>
              <div class="col-md-9 col-sm-8 hidden-xs">
                  <div class="btn-group">                      
                 
                  <button type="button" onclick="downloadpdf()" class="btn btn-default"><i class="fa fa-print"></i> <span class="hidden-xs">Print</span></button>

                  </div>
              </div>
              
              <div class="btn-group col-xs-2 hidden-sm hidden-md hidden-lg">
                <button type="button" class="btn dropdown-toggle pull-right" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="fa fa-ellipsis-v fa-lg"></i>
                  <span class="sr-only">Toggle Dropdown</span>
                </button>
                <ul class="dropdown-menu">                          
                  <li><a href="javascript:;" onclick="downloadpdf()"><i class="fa fa-print"></i> <span>Print</span></a></li>
                </ul>
              </div>
            </div>


          </div>
          <div class="">
            <div class="table-responsive">
            <table class="table table-striped table-hover" id="no-more-tables">
                <thead class="properties">
                <tr>
                    <th>Type</th>
                    <th>HA1</th>
                    <th>HA2</th>
                    <th>HA3</th>
                    <th>HA4</th>                            
                    <th>Other Areas</th>
                    <th>Total</th>
                </tr>
                </thead>                
              </table>
            </div>
          </div>
        </div>


      </div>    
  </div>
  <div class="row emails-calls">
  <!-- emails -->
    <div class="col-sm-6">
      <div class="panel">
        <div class="panel-heading">
          <h3>Emails<small> ( Email types and lead numbers )</small></h3>
        </div>
       
          <div class="table-responsive">
            <table class="table table-striped table-hover" id="no-more-tables">
                <thead>
                <tr>
                    <th>Type</th>
                    <th>Total Leads</th>
                </tr>
                </thead>
                <tbody>
                <tr class="">
                    <td>Valuations</td>
                    <td>0</td>                            
                </tr>
                <tr class="">
                    <td>Property for sale emails</td>
                    <td><?php echo $saleemail;?></td>                            
                </tr>
                <tr class="">
                    <td>Property to rent emails</td>
                    <td><?php echo $rentemail;?></td>                            
                </tr>
                <tr class="">
                    <td><strong>Total</strong></td>
                    <td><?php echo $rentemail+$saleemail;?></td>                            
                </tr>  
                
                </tbody>
                
              </table>
            </div>   
        
      </div>
    </div>
<!-- telephone -->
    <div class="col-sm-6">
      <div class="panel">
        <div class="panel-heading">
          <h3>Telephone calls<small> ( Answered Vs unanswered calls )</small></h3>
        </div>
       
          <div class="table-responsive">
            <table class="table table-striped table-hover" id="no-more-tables">
                <thead>
                <tr>
                    <th>Type</th>
                    <th>Total</th>
                </tr>
                </thead>
                <tbody>
                <tr class="">
                    <td>Answered</td>
                    <td>0</td>                            
                </tr>
                <tr class="">
                    <td>Unanswered</td>
                    <td>0</td>                            
                </tr>
                <tr class="">
                    <td><strong>Total</strong></td>
                    <td>0</td>                            
                </tr>  
                
                </tbody>
                
              </table>
            </div>   
        
      </div>
    </div>
  </div>
</div>
  
</section>        
</section>


<script type="text/javascript" src="//cdn.jsdelivr.net/jquery/1/jquery.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>

<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
<script type="text/javascript">
    $(function () {

        function cb(start, end) {
            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }
        cb(moment().subtract(29, 'days'), moment());

        $('#reportrange').daterangepicker({
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }, cb);

    });
    var base_url = '<?php echo base_url(); ?>';
    var date = '';

    $(document).ready(function () {
        console.log('here1');
//        getCount(base_url, date);
    });
    function getCount(base_url, date) {
        $('table .data-append').remove();
        $('table .foot').remove();
        $.ajax({
            type: "POST",
            url: base_url + "reports/reports/getActivity",
            data: 'date=' + date,
            success: function (data) {
                if (data != '') {

                    $('thead.properties').after(data);
                } else {
                    $('thead.properties').after('No Data');
                }
            }
        });
    }
    $('#reportrange').on('change', function (e) {
        date = $(this).val();
        if (date != '') {
            console.log('here2');
            getCount(base_url, date);
        }
    });
</script>


