<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<style type="text/css">
    .panel-heading {
  padding: 25px 15px;
  border-bottom: 1px solid #ebebeb; }
  .panel-body {
    padding: 15px;
}
</style>
<!-- main content -->
      <section id="main-content" class="main-content">
        <section class="surround" style="margin-top: 90px;">



        <div class="container-fluid">
        <!-- competitor analysis -->
          <div class="row">
              <div class="col-xs-12">

                <div class="panel">
                  <div class="panel-heading">
                    

                    <div class="row">
                      <div class="col-sm-12">
                        <h3>Competitor analysis<small> ( Compare your listing levels to those of nearby competitors with ZPG )</small></h3>
                      </div>
                    </div>


                    <div class="row sel-sear-btn">                      
                      
                      <div class="col-sm-12 col-xs-12">
                          <div class="btn-group">                      
                         
                          <button type="button" class="btn btn-default"><i class="fa fa-print"></i> Print</button>

                          </div>
                      </div>
                      
                      <!-- <div class="btn-group col-xs-2 hidden-sm hidden-md hidden-lg">
                        <button type="button" class="btn dropdown-toggle pull-right" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          <i class="fa fa-ellipsis-v fa-lg"></i>
                          <span class="sr-only">Toggle Dropdown</span>
                        </button>
                        <ul class="dropdown-menu">                          
                          <li><a href="#"><i class="fa fa-print"></i> <span>Print</span></a></li>
                        </ul>
                      </div> -->

                    </div>


                  </div>
                  <div class="">
                    <div class="table-responsive competitor">
                    <table class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>Listing Type</th>
                            <th>Rank</th>
                            <th>Competitor</th>
                            <th>Average Clickthrough rate(last month)</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr class="">
                            <td rowspan="5">
                            <select class="form-control">
                              <option>Sale</option>
                              <option>Rent</option>
                            </select>
                            </td>
                            <td>0</td>
                            <td>5</td>                            
                            <td>7</td>                            
                        </tr>
                        <tr class="">
                            
                            <td>1</td>
                            <td>5</td>
                            <td>7</td>                            
                        </tr>
                        <tr class="">
                           
                            <td>2</td>
                            <td>5</td>
                            <td>7</td>                            
                        </tr>
                        <tr class="">
                            
                            <td>3</td>
                            <td>5</td>
                            <td>7</td>                            
                        </tr>
                        <tr class="">
                            
                            <td>4</td>
                            <td>5</td>
                            <td>7</td>                            
                        </tr>
                        
                        
                        </tbody>
                        
                      </table>
                    </div>
                  </div>
                </div>


              </div>    
          </div>
        </div>
          

          <div class="row">
          <!-- emails -->
            <div class="col-sm-12">
              <div class="panel">
                  <div class="panel-heading">
                    

                    <div class="row">
                      <div class="col-sm-12">
                        <h3>Competitor Chart<small> ( Compare your listing levels to those of nearby competitors with ZPG )</small></h3>
                      </div>
                    </div>

                  </div>



                  
                  <div class="panel-body">
                    <div class="row">
                    <div class="col-sm-4 col-md-3 col-lg-2">
                      <div class="form-group">
                        <label><strong>Listing Type</strong></label>
                        <select class="form-control">
                          <option>Sale</option>
                          <option>Rent</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-sm-12">
                      <div id="chart_div"></div>
                    </div>
                  </div>
                  </div>

                </div>
            </div>

          </div>
          
          
        </section>        
      </section>

<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<!--<div id="chart_div"></div>-->
<script type="text/javascript">

    // var base_url = '<?php echo base_url(); ?>';
    // var date = '';
    // google.load("visualization", "1", {packages: ["corechart"]});
    // google.setOnLoadCallback(drawChart);
    // function drawChart() {
    //     var data = google.visualization.arrayToDataTable([
    //         ["Competitors", "Percent", {role: "style"}],
    //         ["competitor1", 8.94, "color: #f69b25"],
    //         ["competitor2", 10.49, "color: #f69b25"],
    //         ["competitor3", 19.30, "color: #f69b25"],
    //         ["competitor4", 21.45, "color: #f69b25"],
    //         ["competitor5", 11.45, "color: #f69b25"],
    //         ["competitor6", 31.45, "color: #f69b25"],
    //         ["competitor7", 1.45, "color: #f69b25"],
    //         ["competitor8", 2.45, "color: #f69b25"],
    //         ["competitor9", 17.45, "color: #f69b25"],
    //         ["competitor10", 12.45, "color: #f69b25"]
    //     ]);

    //     var view = new google.visualization.DataView(data);
    //     view.setColumns([0, 1,
    //         {calc: "stringify",
    //             sourceColumn: 1,
    //             type: "string",
    //             role: "annotation"},
    //         2]);

    //     var options = {
    //         title: "Competitor Analysis",
    //         height: 500,
    //         bar: {groupWidth: "95%"},
    //         legend: {position: "none"},
    //         hAxis: {title: "competitors" , direction:-1, slantedText:true, slantedTextAngle:60 },
    //          yAxis: {title: "Percent", slantedText:true,},
    //     };
    //     var chart = new google.visualization.ColumnChart(document.getElementById("chart_div"));
    //     chart.draw(view, options);
    // }

    google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Year', 'Sale', 'Rent'],
          ['2013',  1000,      400],
          ['2014',  1170,      460],
          ['2015',  660,       1120],
          ['2016',  1030,      540]
        ]);

        var options = {
          title: 'Competitor Analysis',
          hAxis: {title: 'Year',  titleTextStyle: {color: '#333'}},
          vAxis: {minValue: 0}
        };

        var chart = new google.visualization.AreaChart(document.getElementById('chart_div'));
        chart.draw(data, options);
      }
    </script>



    <script type="text/javascript">
    $("li.active.open").click(function(){
      $(this).removeClass("dropdown-menu");
    });

    $('input[name="daterange"]').daterangepicker({
    "linkedCalendars": false,
    "autoUpdateInput": false,
    "startDate": "11/22/2015",
    "endDate": "11/28/2015",
    "applyClass": "btn-success btn-sm",
    "cancelClass": "btn-default btn-sm"
    }, function(start, end, label) {
      console.log("New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')");
    });
                                        
</script>


