<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//$actualurl="http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
//include("common/settingmenu.php");
//print_r($user);
?>
<style type="text/css">
    .panel-heading {
  padding: 25px 15px;
  border-bottom: 1px solid #ebebeb; }
  .panel-body {
    padding: 15px;
}
</style>
<form class="" id="usermanage" method="post" action="<?php echo base_url(); ?>settings/insert">
<section class="surround1"> 

    <div class="container-fixed add-listing">

<!-- User Information -->
    <div class="panel">  

      <div class="panel-heading">
        
            <h3>User Management</h3>
          
      </div>        

     <div class="panel-title panel-title1">
        User Information 
      </div>
      
      <input type="hidden" id="echeck" value="0"/>
      <input type="hidden" name="id" value="<?php echo isset($user[0]->agentuser_id) ? $user[0]->agentuser_id : '0'; ?>"/>
      <div class="panel-body">

        <div class="row">
          <div class="form-group col-sm-6">
              <label for="p-name">User Name</label>                                                  
              <input id="build-age" type="text" name="username" maxlength="50" id="username" class="form-control" value="<?php echo isset($user[0]->agentuser_name) ? $user[0]->agentuser_name : ''; ?>">
            </div>

            <div class="form-group col-sm-6">
              <label for="p-name">User Email</label>                                                  
              <input id="build-age" type="text"  name="usermail" maxlength="100" id="usermail" onchange="checkemail(this.value);" class="form-control" value="<?php echo isset($user[0]->agentuser_email) ? $user[0]->agentuser_email : ''; ?>" <?php echo isset($user[0]->agentuser_email) ? 'readonly' : ''; ?>>
            </div>

            <div class="form-group col-sm-6">
              <label for="p-name">User Mobile Number</label>                                                
              <input id="build-age" type="text" name="usermobile" maxlength="15" id="usermobile"  class="form-control" value="<?php echo isset($user[0]->agentuser_mobile) ? $user[0]->agentuser_mobile : ''; ?>">
            </div>

            <div class="form-group col-sm-6">
              <label for="p-name">User Role</label>                                               
              <input type="text" id="userrole" name="userrole" class="form-control" maxlength="20" value="<?php echo isset($user[0]->agentuser_userrole) ? $user[0]->agentuser_userrole : ''; ?>" placeholder="(max 20 Characters)">
            </div>

            <div class="form-group col-sm-6">
              <p>User Access</p>
              <?php
                $read = $write = '';
                if (isset($user[0]->agentuser_access)) {
                    if ($user[0]->agentuser_access == 2) {
                        $write = 'checked';
                    } else if ($user[0]->agentuser_access == 3) {
                        $read = 'checked';
                    }
                }
                ?>
              <label class="checkbox-inline">
                <input type="checkbox"  <?php echo $write; ?> style="width: 20px;height: 11px;margin-top: 2px;" id="CanAdd" name="permission" value="2" class="form-control"> Write Only
              </label>
              <label class="checkbox-inline">
                <input type="checkbox" <?php echo $read; ?> style="width: 20px;height: 11px;margin-top: 2px;" id="CanRead" name="permission" value="3" class="form-control"> Read Only
              </label>
            </div>

                         
        </div>            
        
      </div>

      <div class="panel-title">
        <div class="save-cancel">
          <button type="submit" class="btn btn-default submit">Save</button>
          <a href="" class="btn btn-default" onclick="history.go(-1)">Cancel</a>
        </div>
      </div>
      
      
    </div> 
     


    </div> 

    
  </section> 
</form>









<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>
<script>
    var base_url = '<?php echo base_url(); ?>';
    $("#usermanage").keyup(function(event){
        if(event.keyCode == 13){
            $(".submit").click();
        }
    });

    $(document).ready(function () {

    });
    $('.submit').on('click', function () {
        $('.error').remove();
        var emailPattern = /(?![0-9])^[a-z0-9%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i;
        var mobpattern = /^(?:(?:\+?[0-9]{2}|0|0[0-9]{2}|\+|\+[0-9]{2}\s*(?:[.-]\s*)?)?(?:\(\s*([2-9][02-9]|[2-9][02-8]1|[2-9][02-8][02-9])\s*\)|([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9]))\s*(?:[.-]\s*)?)?([2-9]1[02-9]|[2-9][02-9]1|[2-9][02-9]{2})\s*(?:[.-]\s*)?([0-9]{4})(?:\s*(?:#|x\.?|ext\.?|extension)\s*(\d+))?$/;

//        alert(echeck);

        if ($("input[name='username']").val() == '') {
            $(".panel-title1").append('<span class="error" style="color:red">Please enter user name</span>');
            $("input[name='username']").focus();
        } else if ($("input[name='usermail']").val() == '') {
            $(".panel-title1").append('<span class="error" style="color:red">Please enter user email</span>');
            $("input[name='usermail']").focus();
        } else if (!emailPattern.test($("input[name='usermail']").val())) {
            $(".panel-title1").append('<span class="error" style="color:red">Please enter valid email address</span>');
             $("input[name='usermail']").focus();
        } else if ($("input[name='usermobile']").val() == '') {
            $(".panel-title1").append('<span class="error" style="color:red">Please enter mobile number</span>');
             $("input[name='usermobile']").focus();
        } else if (!mobpattern.test($("input[name='usermobile']").val())) {
            
    $(".panel-title1").append('<span class="error" style="color:red">Please enter valid mobile number</span>');
    $("input[name='usermobile']").focus();
        } else if ($("input[name='userrole']").val() == '') {
            $(".panel-title1").append('<span class="error" style="color:red">Please enter user role</span>');
            $("input[name='userrole']").focus();
        } else if ($("input[name='permission']:checked").length == 0) {
            $("input[name='userrole']").after('<span class="error" style="color:red">Please select one permission</span>');
            $("input[name='permission']").focus();
        } else {
//              alert($("#echeck").val());
            //checkemail($("input[name='usermail']").val());
            setTimeout(function(){ 
              if ($("#echeck").val() == '1') {
                  console.log("Hello");
                  $(".panel-title1").append('<span class="error" style="color:red">Email address already registered, please try some other email address</span>');
                  $("input[name='usermail']").focus();
              } else {
                  $('#usermanage').submit();
                  console.log("Ready to submit");
    //                 
              }
            }, 500);
            

            
        }
        return false;
    });
    $("input[name='permission']").on('change', function () {
        $("input[name='permission']").not(this).prop('checked', false);
    });
    // function ajaxadmin(base_url, datas) {
    //     $.ajax({
    //         type: "POST",
    //         url: base_url + "/settings/insert",
    //         data: datas,
    //         success: function (data) {
    //             alert('Saved Successfully');
    //             window.location.reload();
    //         }
    //     });
    // }



    function checkemail(email) {
      console.log("Hello");
        if ($("input[name='id']").val() == 0) {
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>/settings/checkemail",
                data: "usermail=" + email,
                dataType: "html",
                success: function (data) {
                    console.log(data);
                    $('#echeck').val(data);

                }
            });
        }

    }
    


</script>
