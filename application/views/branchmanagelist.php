<?php
include("common/header.php");
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//$actualurl="http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
include("common/settingmenu.php");
$access = $this->session->userdata['agentuser']['access'];
$access = $this->session->userdata['agentuser']['access'];
if (!empty($forata)) {
    $_SESSION["searchtype"] = (isset($forata['searchtype'])) ? $forata['searchtype'] : $_SESSION["searchtype"];
} else {
    $_SESSION["searchtype"] = "";
}
?>
<style type="text/css">
    .panel-heading {
        padding: 25px 15px;
        border-bottom: 1px solid #ebebeb; }
    .panel-body {
        padding: 15px;
    }
    .successUpdate{
        position: absolute;
        right: 30px;
        width: 50%;
        top: 13px;
    }
</style>
<section id="main-content" class="main-content">
    <section class="surround1">



        <div class="container-fluid user-settings">
            <div class="row">
                <div class="col-xs-12">
                    <div class="panel">
                        <div class="panel-heading">


                            <div class="row">
                                <div class="col-sm-12">
                                    <h3>Branch Management</h3>
                                </div>
                            </div>
                            <div class="successUpdate alert alert-success" role="alert" style="display:none">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                                <i class="fa fa-check"></i> Agent branch deleted successfully
                            </div>
                            <?php
                            if ($this->session->flashdata('agentAdded')) {
                                ?>
                                <div class="successUpdate alert alert-success" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                    <i class="fa fa-check"></i> <?php echo $this->session->flashdata('agentAdded'); ?>
                                </div>
                                <?php
                            }
                            ?>
                            <form method="post" id="submitForm">
                                <div class="row sel-sear-btn">

                                    <div class="col-md-6 col-sm-12 col-xs-10 pr0">
                                        <div class="form-group">
                                            <input type="text" class="form-control search-prop" id="search" name="search" placeholder="Search here" value="<?php echo $_SESSION["searchtype"]; ?>">
                                        </div>
                                    </div>
                                    <div class="p0 col-md-1 col-sm-2">
                                        <a class="s-color search btn btn-default search_show" href="javascript:;" onclick="SubmitFormElement();" style="font-size: 14px;border-left: 0;">Search</a>
                                    </div>
                                    <div class="col-md-5 col-sm-12 hidden-xs">
                                        <div class="btn-group">
                                            <?php
                                            if ($access == 1 || $access == 2) {
                                                ?>
                                                <button type="button" onclick="location.href = '<?php echo base_url('settings/addbranch'); ?>';" class="btn btn-default"><i class="glyphicon glyphicon-plus"></i> <span class="hidden-xs">Add New Branch</span></button>                          
                                            <?php } ?>
                                            <button type="button" class="btn btn-default" onclick="printpage();"><i class="fa fa-print"></i> <span class="hidden-xs">Print</span></button>
                                            <button type="button" class="btn btn-default" onclick="downloadpdf()"><i class="glyphicon glyphicon-download-alt"></i> <span class="hidden-xs">Save as PDF</span></button>
                                            <button type="button" class="btn btn-default" onclick="exportbranch()"><i class="fa fa-external-link"></i> <span class="hidden-xs">Export to Excel</span></button>

                                        </div>
                                    </div>

                                    <div class="btn-group col-xs-2 hidden-sm hidden-md hidden-lg">
                                        <button type="button" class="btn dropdown-toggle pull-right" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fa fa-ellipsis-v fa-lg"></i>
                                            <span class="sr-only">Toggle Dropdown</span>
                                        </button>
                                        <ul class="dropdown-menu">
                                            <?php
                                            if ($access == 1 || $access == 2) {
                                                ?>
                                                <li><a href="<?php echo base_url('settings/addbranch'); ?>"><i class="glyphicon glyphicon-plus"></i> <span>Add New Branch</span></a></li>
                                            <?php } ?>
                                            <li><a href="javascript:;" onclick="printpage();"><i class="fa fa-print"></i> <span>Print</span></a></li>
                                            <li><a href="javascript:;" onclick="downloadpdf()"><i class="glyphicon glyphicon-download-alt"></i> <span>Save as PDF</span></a></li>
                                            <li><a href="javascript:;" onclick="exportbranch()"><i class="fa fa-external-link"></i> <span>Export to Excel</span></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </form>

                        </div>
                        <div class="">
                            <div class="table-responsive">
                                <table class="table table-striped table-hover" id="no-more-tables">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Agent Name</th>
                                            <th>Email Address</th>
                                            <th>Agent Phone</th>
                                            <th>Agent Post Code</th>                            
                                            <th>Edit</th>
                                            <th>Delete</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        if (!empty($agentBranch)) {
                                            if (!empty($links)) {
                                                $i = 1 + ($this->pagination->cur_page - 1) * $this->pagination->per_page;
                                            } else {
                                                $i = 1;
                                            }
                                            //$i=$this->uri->segment(4);
                                            foreach ($agentBranch as $key => $branch) {
                                                ?>
                                                <tr class="">
                                                    <td><?php echo $i; ?></td>
                                                    <td><?php echo $branch->agent_name; ?></td>
                                                    <td><?php echo $branch->email_id; ?></td>

                                                    <td><?php echo $branch->phone_no; ?></td>

                                                    <td><?php echo $branch->post_code; ?></td>                                
                                                    <td><a href="<?php echo base_url('settings/branchEdit') . '/' . $branch->agent_id ?>" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></a></td>
                                                    <td><button class="btn btn-primary btn-xs" onclick="deleteBranch(<?php echo $branch->agent_id; ?>)"><i class="fa fa-times"></i></a></td>
                                                </tr>
                                                <?php
                                                $i++;
                                            }
                                        } else {
                                            ?>
                                            <tr><td colspan="7">No Record Found</td></tr>
    <?php }
?>
                                    </tbody>
                                    <tfoot>
                                        <tr>

                                            <?php
                                            //echo $linkCount;
                                            if ($linkCount <= 1) {
                                                ?>


                                                <td colspan="3"> 
                                                    <div class="btn-group">
    <?php echo $countprop; ?> Results
                                                    </div>
                                                </td>
                                            <?php
                                            } else {
                                                $ifor = 1 + ($this->pagination->cur_page - 1) * $this->pagination->per_page;
                                                ?>
                                                <td colspan="3"> 
                                                    <div class="btn-group">
                                                <?php echo $ifor . " - " . ($i - 1) . " of " . $countprop; ?> Results
                                                    </div>
                                                </td>
                                                    <?php }
                                                ?>
                                            <td colspan="11">
<?php echo $links; ?>
                                            </td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>    
            </div>
        </div>

    </section>        
</section>







<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>
<script>
                            function SubmitFormElement() {
                                $('#submitForm').submit();
                            }
                            var base_url = '<?php echo base_url(); ?>';
                            // var row = text = offset = '';

                            // $(document).ready(function () {
                            //     getBranch(base_url, row = 10, text = '', offset = 1);
                            // });
                            // function getBranch(base_url, row, text) {
                            //     $('table .data-append').remove();
                            //     $('table .foot').remove();
                            //     $.ajax({
                            //         type: "POST",
                            //         url: base_url + "settings/getBranch",
                            //         data: 'row=' + row + '&text=' + text + '&offset=' + offset,
                            //         success: function (data) {
                            //             if (data != '') {

                            //                 $('thead').after(data);
                            //                 $('.foot').css('display', '');
                            //             } else {
                            //                 $('thead').after('No Data');
                            //                 $('.foot').css('display', 'none');
                            //             }
                            //         }
                            //     });
                            // }
                            function deleteBranch(id) {
                                if (confirm('If you delete this branch, you will lose all property, users related in easaportal. Are you sure you want to Delete?')) {
                                    $.ajax({
                                        type: "POST",
                                        url: "<?php echo base_url(); ?>/settings/deleteBranch",
                                        data: "id=" + id,
                                        success: function (data) {
                                            if (data) {
                                                if (data == 1) {
                                                    $('#' + id).remove();
                                                    $('.successUpdate').show();

                                                    setTimeout(function () {
                                                        $('.successUpdate').fadeOut();
                                                        window.location.href = base_url + "settings/branchManage";
                                                    }, 3000);
                                                } else {
                                                    alert('Permission denied');
                                                }
                                            }
                                        }
                                    });
                                }
                            }

//     $('table').on('click', '.rowscnt', function () {
//         $('table .rowscnt').removeClass('active');
//         row = $(this).text();
//         $(this).addClass('active');
//         text = $('#search').val();
//         getBranch(base_url, row, text, offset = 1)
//         console.log($(this).text());
//     });
//     $('.search').on('click', function () {
//         text = $('#search').val();
//         getBranch(base_url, row = 10, text, offset = 1)
//     });
//     $('.table').on('click', '.offset', function () {
//         $('table .offset').closest('li').removeClass('active');

//         offset = $(this).text();
//         getBranch(base_url, row = 10, text = '', offset);

//         $(this).closest('li').addClass('active');


//     });
//     $('.table').on('click', '.next', function () {


//         offset = $(this).closest('ul').find('li.active').find('a').text();
// //          $('table .offset').closest('li').removeClass('active');
//         console.log(offset);
//         var next = offset++;
//         console.log(next);
//         getBranch(base_url, row = 10, text = '', next);

//     });
                            function exportbranch() {
                                var url = base_url + "settings/exportbranch";
                                window.open(url, '_blank');

                            }

                            function downloadpdf() {
                                var url = base_url + "settings/exportbranch";
                                var userid = "<?php echo $this->session->userdata['agentuser']['id']; ?>";
                                var today = new Date();
                                var dd = today.getDate();
                                var mm = today.getMonth() + 1; //January is 0!

                                var yyyy = today.getFullYear();
                                if (dd < 10) {
                                    dd = '0' + dd
                                }
                                if (mm < 10) {
                                    mm = '0' + mm
                                }
                                var today = yyyy + '-' + mm + '-' + dd;
                                var openurl = base_url + '/images/branchdata_' + userid + '_' + today + '.pdf';
                                $.ajax({
                                    url: base_url + "settings/downbranchpdf",
                                    type: "POST",
                                    success: function (data) {
                                        window.open(openurl, '_blank');
                                    }
                                });

                            }
                            function printpage() {
                                var sel = $('#status').val();
                                //var url = base_url + "properties/property/export";

                                var userid = "<?php echo $this->session->userdata['agentuser']['id']; ?>";
                                var today = new Date();
                                var dd = today.getDate();
                                var mm = today.getMonth() + 1; //January is 0!

                                var yyyy = today.getFullYear();
                                if (dd < 10) {
                                    dd = '0' + dd
                                }
                                if (mm < 10) {
                                    mm = '0' + mm
                                }
                                var today = yyyy + '-' + mm + '-' + dd;
                                var openurl = base_url + '/images/branchdata_' + userid + '_' + today + '.pdf';
                                $.ajax({
                                    url: base_url + "settings/downbranchpdf",
                                    type: "POST",
                                    data: 'type=1&sel=' + sel,
                                    success: function (data) {

                                        window.open(openurl, '_blank');
                                    }
                                });
//                                        window.print();
                            }
</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
