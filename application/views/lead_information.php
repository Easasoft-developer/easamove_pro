<?php
include("common/header.php");

//print_r($getAgent);
?>
<!-- main content -->
<style>
    .error{
        color: red;
    }
    .panel-heading {
  padding: 25px 15px;
  border-bottom: 1px solid #ebebeb; }
  .panel-body {
    padding: 15px;
}
</style>
<section id="main-content" class="main-content" style="">
    <section class="surround1">
        <div class="container-fixed add-listing">
  
<!-- User Information -->
        <form id="myForm" role="form" action="<?php echo base_url(); ?>Lead_controller/updateletting" method="post" onsubmit="return validationLetting()">
        <?php if ($this->session->flashdata('lettingInformation')) { ?>
            <div class="alert alert-success"> <?= $this->session->flashdata('lettingInformation') ?> </div>
        <?php } ?>
        
        <div class="panel">  

          <div class="panel-heading">
            
                <h3>Letting information</h3>
              
          </div>   
          <div class="panel-body">
          
              <div class="row">

                 <div class="form-group col-md-12">
                      <textarea rows="10" name="Letting_information" id="" class="form-control ckeditor" placeholder="About..."><?php echo isset($leadinfo[0]->letting_information) ? $leadinfo[0]->letting_information : ''; ?></textarea>                        
                  </div>
              </div>
          </div>
          
          <div class="panel-title">
            <div class="save-cancel">
                <button type="submit" class="btn btn-default">
                      Update
                </button>
              <!-- <a href="" class="btn btn-default">Save</a> -->
            </div>
          </div>
          
        </div> 
         </form>


        </div> 
    </section>        
</section>
<?php
include("common/footer.php");
?>

