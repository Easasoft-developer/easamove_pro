<?php
include("common/header.php");

//print_r($getAgent);
?>
<!-- main content -->
<style>
    .error{
        color: red;
    }
    .panel-heading {
  padding: 25px 15px;
  border-bottom: 1px solid #ebebeb; }
  .panel-body {
    padding: 15px;
}
</style>
<section id="main-content" class="main-content" style="">
    <section class="surround1">
        <div class="container-fixed add-listing">
  
<!-- User Information -->
        <form id="myForm" role="form" action="<?php echo base_url(); ?>myaccount/myaccount/updateAgent" name="saveAgentDetails" method="post" enctype="multipart/form-data" onsubmit="return validationAgent()">
        <?php if ($this->session->flashdata('agentupdate')) { ?>
            <div class="alert alert-success"> <?= $this->session->flashdata('agentupdate') ?> </div>
        <?php } ?>
                    <input type="hidden" name="getAgentId" value="<?php echo isset($getAgent[0]->agent_id) ? $getAgent[0]->agent_id : 0; ?>">
        <div class="panel">  

          <div class="panel-heading">
            
                <h3>My Account</h3>
              
          </div>        

         <div class="panel-title">
            Basic Information
          </div>

          <div class="panel-body">

            <div class="row">
                <div class="col-md-12 p0">
                  <div class="form-group col-sm-6">
                  <label for="p-name">Forename <span style="color: red;">*</span></label>                                                  
                  <input id="forename" name="forename" placeholder="Enter name"  type="text" class="form-control input-sm" value="<?php echo isset($getAgent[0]->forename) ? $getAgent[0]->forename : ''; ?>">
                </div>

                <div class="form-group col-sm-6">
                  <label for="p-name">Surname  <span style="color: red;">*</span></label>                                                  
                  <input id="surname" name="surname" placeholder="Enter surname"  type="text" class="form-control input-sm" value="<?php echo isset($getAgent[0]->surname) ? $getAgent[0]->surname : ''; ?>">
                </div>
                </div>
                <div class="col-md-12 p0">
                <div class="form-group col-sm-6">
                  <label for="p-name">Email Address  <span style="color: red;">*</span></label>                                                
                  <input id="emailaddress" name="emailaddress" type="text" class="form-control input-sm" readonly="readonly" value="<?php echo isset($getAgent[0]->email_id) ? $getAgent[0]->email_id : ''; ?>">
                </div>

                <div class="form-group col-sm-6">
                  <label for="p-name">Phone Number<span style="color: red;">*</span></label>                                               
                  <input id="p-number" name="p-number" type="text" class="form-control input-sm" value="<?php echo isset($getAgent[0]->phone_no) ? $getAgent[0]->phone_no : ''; ?>">
                </div>
                </div>
                <div class="col-md-12 p0">
                <div class="form-group col-sm-6">
                  <label for="p-name">Fax Number <span style="color: red;">*</span></label>                                                
                  <input id="f-number" name="f-number" type="text" class="form-control input-sm" value="<?php echo isset($getAgent[0]->fax) ? $getAgent[0]->fax : ''; ?>">
                </div>

                <div class="form-group col-sm-6">
                  <label for="p-name">Website</label>                                               
                  <input id="website" name="website" type="text" class="form-control input-sm" value="<?php echo isset($getAgent[0]->website) ? $getAgent[0]->website : ''; ?>" placeholder="eg: http://www.example.com">
                </div>
                </div>

                             
            </div>            
            
          </div>

          <div class="panel-title">
            Logo Information
          </div>
          <div class="panel-body">
              <div class="row">
                  <div class="form-group col-md-6">
                      <label class="btn btn-default pull-left">
                            <i class="fa fa-plus"></i>
                            Upload Photos
                            <input type="file" id="logotype" name="agent_logo" class="pimage" onchange="readURL(this)" style="display:none;">
                        </label>
                        <div class="contentview pull-left"  style="margin: 6px 0 0 10px;"><p></p></div>
                  </div>
                  <div class="form-group col-md-6 mb10 contentDisplay">
                        <input type="hidden" id="logotype" name="agent_img" class="pimage" value="<?php echo isset($getAgent[0]->agent_logo) ? $getAgent[0]->agent_logo : ''; ?>">
                        <img id="imglogo" src="http://admin.easamove.co.uk/images/<?php echo isset($getAgent[0]->agent_logo) ? $getAgent[0]->agent_logo : ''; ?>"  height="50px"/>
                    </div>
              </div>
          </div>

          <div class="panel-title">
            Agent Gallery
          </div>
          <div class="panel-body">
              <div class="row">
                 <div class="form-group col-md-6">
                      <label class="btn btn-default pull-left">
                            <i class="fa fa-plus"></i>
                            Upload Photos
                            <input type="file" id="fileupload" name="fileupload[]" class="pimage" multiple="multiple" style="display:none;">
                        </label>
                        
                  </div> 
                  <div class="form-group col-md-12">
                      <div id="dvPreview"></div>
                      <div class="row m15">
                          <?php
                            if (!isset($getAgent[0]->agent_id)) {
                                ?>

                                <?php
                            } else {
                                foreach ($agentImage as $img) {
                                    ?>  
                          <div class="m0 col-sm-4 col-md-3 alert media">
                            <figure>
                            <!-- <input type="hidden" name="propimg[]" value="<?php echo $img->propimg_l; ?>"/> -->
                              <a href=""><img src="<?php echo base_url(); ?>images/agentimage/<?php echo $img->agent_image; ?>" alt="<?php echo $img->agent_image; ?>" class="img-responsive" style="height:170px;">
                                   
                              </a>
                              <input class="form-control" type="text" name="caption[]" value="<?php echo $img->agent_caption; ?>">
                              <span class="savename hidden">Photos 1</span>
                              <a href="" class="close-icon close" data-dismiss="alert" onclick="imgRemove(<?php echo $img->agentimg_id; ?>,'<?php echo $img->agent_image; ?>');"><img src="<?php echo base_url(); ?>images/close-icon.png"></a>
                            </figure>
                          </div>
                           <?php }
                           } ?> 
                      </div>
                  </div>
                  
              </div>
          </div>
          <div class="panel-title">
            Agent Members
          </div>
          <div class="panel-body">
          <div class="row mb10">
              <?php 
              foreach($agentMemeber as $age){
              ?>
              <div class="col-md-2">
                <div class="form-group">
                  <label class="checkbox-inline">
                    <input type="checkbox" id="inlineCheckbox1" <?php echo isChecked($age->member_id, $getAgent[0]->member_id); ?> name="agent-member[]" value="<?php echo $age->member_id; ?>"> <img src="http://admin.easamove.co.uk/images/agent_member/<?php echo $age->member_logo; ?>" style="height: 50px;">
                  </label>
                </div>
              </div>
              <?php } ?>
              <div class="col-md-12">
                <p>Note: please choose your members.</p>
              </div>
          </div>
              <div class="row">

                 <div class="form-group col-md-12">
                      <textarea rows="8" name="agent_desc" id="agent_desc" class="form-control ckeditor" placeholder="About..."><?php echo isset($getAgent[0]->agent_aboutus) ? $getAgent[0]->agent_aboutus : ''; ?></textarea>                        
                  </div>
              </div>
          </div>
          <div class="panel-title">
            <div class="save-cancel">
                <button type="submit" class="btn btn-default">
                      Update
                </button>
              <!-- <a href="" class="btn btn-default">Save</a> -->
            </div>
          </div>
          
        </div> 
         </form>


        </div> 
    </section>        
</section>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript">
var baseurl = "<?php echo base_url(); ?>";
function readURL(input) {
    if (input.files && input.files[0]) {
        var extension = input.files[0].name.substring(input.files[0].name.lastIndexOf('.'));
        console.log('here image ' + extension);
        if (extension == '.jpeg' || extension == '.png' || extension == '.jpg') {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#imglogo').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        } else {
            $("#imglogo").after('<span class="error">Please choose valid file format</span>');
            $("#imglogo").focus();
        }
    }
}

//validation for agent 

function validationAgent() {
//        return false;
    var letter = /^[a-zA-Z\s]*$/;
    var mobilePattern = /^(?:(?:\+?[0-9]{2}|0|0[0-9]{2}|\+|\+[0-9]{2}\s*(?:[.-]\s*)?)?(?:\(\s*([2-9][02-9]|[2-9][02-8]1|[2-9][02-8][02-9])\s*\)|([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9]))\s*(?:[.-]\s*)?)?([2-9]1[02-9]|[2-9][02-9]1|[2-9][02-9]{2})\s*(?:[.-]\s*)?([0-9]{4})(?:\s*(?:#|x\.?|ext\.?|extension)\s*(\d+))?$/;
    var faxpattern = /^\+?[0-9]{6,}$/;
    var website = /^(http:\/\/|https:\/\/|ftp:\/\/|)[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/i;
    $('.error').remove();
    //Checking condition for fields
    if ($("input[name='forename']").val() == '') {
        $("input[name='forename']").after('<div class="col-md-12 p0 error">Please enter your forename</div>');
        $("input[name='forename']").focus();
    } else if (!letter.test($("input[name='forename']").val())) {
        $("input[name='forename']").after('<div class="col-md-12 p0 error">Please enter valid forename</div>');
        $("input[name='forename']").focus();
    } else if ($("input[name='surname']").val() == '') {
        $("input[name='surname']").after('<div class="col-md-12 p0 error">Please enter your surname</div>');
        $("input[name='surname']").focus();
    } else if (!letter.test($("input[name='surname']").val())) {
        $("input[name='surname']").after('<div class="col-md-12 p0 error">Please enter valid surname</div>');
        $("input[name='surname']").focus();
    } else if ($("input[name='p-number']").val() == '') {
        $("input[name='p-number']").after('<div class="col-md-12 p0 error">Please enter your phone number</div>');
        $("input[name='p-number']").focus();
    } else if (!mobilePattern.test($("input[name='p-number']").val())) {
        $("input[name='p-number']").after('<div class="col-md-12 p0 error">Enter valid phone no</div>');
        $("input[name='p-number']").focus();
    } else if ($("input[name='f-number']").val() == '') {
        $("input[name='f-number']").after('<div class="col-md-12 p0 error">Please enter your fax nuumber</div>');
        $("input[name='f-number']").focus();
    } else if (!faxpattern.test($("input[name='f-number']").val())) {
        $("input[name='f-number']").after('<div class="col-md-12 p0 error">Enter valid fax no</div>');
        $("input[name='f-number']").focus();
    } else if (!website.test($("input[name='website']").val()) && $("input[name='website']").val() != "") {
        $("input[name='website']").after('<div class="col-md-12 p0 error">Please enter valid url</div>');
        $("input[name='website']").focus();
    } else if ($("input[name='agent_img']").val() == ''&&$("input[name]=getAgentId").val()==0) {
        $("input[name='agent_img']").parent('.contentDisplay').after('<div class="col-md-3 error mt10">Required</div>');
        $("input[name='agent_img']").focus();
    } else if ($("#agent_desc").val() == '') {
        $("#agent_desc").parent('.contentDisplay').after('<div class="col-md-3 error mt10">Required</div>');
        $("#agent_desc").focus();
    }else {
        return true;
    }
    return false;
}

var filesArray = [];
window.onload = function () {
    if (window.File && window.FileList && window.FileReader) {

        var preview = $('#dvPreview');
        var fileUpload = document.getElementById("fileupload");
        fileUpload.onchange = function () {
            if (typeof (FileReader) != "undefined") {
                var dvPreview = document.getElementById("dvPreview");
//                                        dvPreview.innerHTML = "";
                var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg|.gif|.png|.bmp)$/;
                preview.empty();
                for (var i = 0; i < fileUpload.files.length; i++) {
                    var file = fileUpload.files[i];
                    if (regex.test(file.name.toLowerCase())) {

                        var reader = new FileReader();
                        reader.onload = (function (theFile) {
                            console.log(file.name);
                            var flname = file.name;
                            return function (e) {
                                preview.append('<div class="m0 col-sm-4 col-md-3 alert media"><figure><a href=""><input type="hidden" name="propimg[]" value="'+flname+'"/><img src="' + e.target.result + '" alt="' + e.target.result + '" class="img-responsive" style="height:170px;"></a><input class="form-control" type="text" name="caption[]"><a href="" class="close-icon close" data-dismiss="alert"><img src="../images/close-icon.png"></a></figure></div>');
                                filesArray.push(theFile);
                            };
                        })(i, preview);
                        reader.readAsDataURL(file);
                    } else {
                        alert(file.name + " is not a valid image file.");
                        dvPreview.innerHTML = "";
                        return false;
                    }
                    // UploadFile(file, i)
                }
            } else {
                alert("This browser does not support HTML5 FileReader.");
            }
        }
    }

};

function imgRemove(id,imgUrl){
    console.log(imgUrl);
    $.ajax({
        type: "POST",
        url: '<?php echo base_url(); ?>myaccount/Myaccount/deleteImages',
        data: "id=" + id +'&url=' + imgUrl,
        dataType: "html",
        success: function (data) {
            console.log(data);
                     

        }
    });
}
</script>
<?php
include("common/footer.php");
?>

