<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//if(isset($cnt))

//echo $ctn;
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>EASAPRO</title>

        <!-- Bootstrap -->
        <link href="<?php echo base_url(); ?>css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>css/style.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>css/responsive.css" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

        <link rel="stylesheet" href="<?php echo base_url(); ?>css/datatables.css" />
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
	    
    <body class="log">
        <div class="login-bg">
            <div class="panel login text-center">
                <div class="panel-heading "> 
                    <h1>EASAPRO</h1>
                    <h4 class="panel-title"></h4>    
                </div>
                <div class="panel-body">
                    <?php
                    if(isset($message)){

                    echo "<div class='control-group errorcls alertForError' style='padding-bottom:10px;'>".$message."</div>";   

                    }
                    if(!isset($messageId)){
                    ?>

                    <form id="resetPassword" method="POST" action="<?php echo base_url('Account/reset'); ?>" name="resetPassword" class="col-md-12 col-sm-12 col-xs-12 col-lg-12 pb10">
                        <input type="hidden" name="fkpassword" value="<?php if(isset($getId)) { echo $getId; } ?>">
                        <div class="form-group forgoterror">
                            <label>New Password</label>
                            <input type="password" name="newpassword" id="newpassword" placeholder="Enter New Password" class="form-control txtEmailPass" value="">
                            
                        </div>
                        <div class="form-group forgoterror">
                            <label>Confirm Password</label>
                            <input type="password" name="confirmpassword" id="confirmpassword" placeholder="Re-enter your Password" class="form-control txtEmailPass" value="<?php $this->input->post('confirmpassword') ?>">
                            
                        </div>
                        <div class="form-group row col-md-12 col-sm-12 col-xs-12 col-lg-12 mt20">
                            <div class="pull-left"><button type="submit" class="btn btn-primary btnLogin" name="reset" id="reset">SUBMIT</button></div>
                            <!-- <div class="pull-left frgt"><a href="<?php echo base_url(); ?>" class="aForgetPassword">Sign In</a></div> -->
                        </div>
                    </form>
<?php }else{
    ?>
                    <div class="pb mb pt alert alertForSuccess   fadeoutthis" role="alert">
                         <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> 
                        <span><?=$messageId?>
                          
                        </span>
                    </div>
<?php
} ?>
               
            </div>
                </form>
            </div>
        </div>


        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="<?php echo base_url(); ?>js/bootstrap.min.js"></script>

    </body>
</html>