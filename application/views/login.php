<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
﻿<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Srock</title>

        <!-- Bootstrap -->
        <link href="<?php echo base_url(); ?>css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>css/main.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>css/custom_class.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>css/responsive.css" rel="stylesheet">
        <link rel="icon" href="<?php echo base_url(); ?>images/favicon.png" type="image/png">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

        <link rel="stylesheet" href="<?php echo base_url(); ?>css/datatables.css" />
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="log">
        <div class="login-bg">
            <div class="panel login text-center">
                <div class="panel-heading "> 
                    <h1><img src="<?php echo base_url(); ?>images/logo_img.png" style="width: 200px;
"></h1>
                    <h4 class="panel-title"><p>Welcome! Please Login</p></h4>    
                </div>
                <div class="panel-body">
                    <form role="form" method="post" action="<?php echo base_url('Account/login'); ?>" onsubmit="return validate();">
                        <?php if ($this->session->flashdata('logincheck')) { ?>
                            <div class="alert alert-danger"> <?= $this->session->flashdata('logincheck') ?> </div>
                        <?php } ?>
                        <div class="form-group">
                            <label class="sr-only" for="user">username</label>
                            <input type="text" class="form-control" name="UserName" id="UserName" placeholder="Your Username">
                        </div> 

                        <div class="form-group">
                            <label class="sr-only" for="password">password</label>
                            <input type="password" class="form-control" name="Password" id="Password"  placeholder="Your Password">
                        </div>

                        <div class="text-left mb15">
                            <input type="checkbox" name="RememberMe" id="RememberMe" value="Remember me" />
                            <label for="RememberMe" class="remember"><span>Remember me</span></label>
                            <!-- <label>
                                <input type="checkbox" name="RememberMe" id="RememberMe" value="Remember me"> Remember me
                            </label> -->

                            <a href="<?php echo base_url('Account/forgotpassword'); ?>">Forgot Password ?</a>
                        </div>
                        <input class="btn btn-warning btn-sm" type="submit" value="Login">

                    </form>
                </div>
            </div>
        </div>


        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="<?php echo base_url(); ?>js/bootstrap.min.js"></script>
        <script>
           function validate(){
               
                $('.error').remove();
                var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
               if ($("input[name='UserName']").val() == '') {
                    $("input[name='UserName']").after('<span class="error" style="color:red">Please fill this field</span>');
                     return false;
                } else if (!emailPattern.test($("input[name='UserName']").val())) {
                    $("input[name='UserName']").after('<span class="error" style="color:red">Please enter valid email address</span>');
                     return false;
                }  if ($("input[name='Password']").val() == '') {
                    $("input[name='Password']").after('<span class="error" style="color:red">Please fill this field</span>');
                     return false;
                } else {
                    return true;
                }
                 return false;
            }
        </script>
    </body>
</html>

