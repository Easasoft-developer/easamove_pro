<?php
include("common/header.php");
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//$actualurl="http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
include("common/settingmenu.php");
?>

<section class="surround">
    <div class="panel sale-rent">
        <div class="panel-heading">Settings</div>
        <!-- <div class="panel-heading tab-dark-navy-blue ">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a aria-expanded="false" data-toggle="tab" href="#sale">For Sale<span class="badge">5</span></a>
                </li>
                <li class="">
                    <a aria-expanded="false" data-toggle="tab" href="#rent">For Rent<span class="badge">50</span></a>
                </li>                        
            </ul>
        </div> -->
        <div class="panel-body">
            <div class="adv-table editable-table ">
                <div class="row">

                    <div class="col-md-4 col-sm-6">


                        <div class="form-group">
                            <label class="sr-only" for="search">Search</label>
                            <div class="input-group">

                                <input type="text" class="form-control input-sm" id="search" placeholder="Search">
                                <div class="input-group-addon"><a class="s-color search" href="javascript:;"><i class="fa fa-search"></i></a></div>
                            </div>
                        </div>                                          


                    </div>

                    <div class="col-md-3 col-sm-6">
                        <div class="btn-group pull-right">                                       
                            <ul class="list-inline">
                                <li><a href="#"><i class="fa fa-print"></i> Print</a></li>
                                <li><a href="#"><i class="fa fa-download"></i> Save as PDF</a></li>
                                <li><a href="#"><i class="fa fa-external-link-square"></i> Export to Excel</a></li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="space15"></div>
                <div class="table-responsive">
                    <table class="table table-striped table-hover" id="editable-sample">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>User Name</th>
                                <th>User Email Address</th>
                                <th>User Role</th>
                                <th>User Access</th>
                                <th>Edit</th>
                                <th>Delete</th>
                            </tr>
                        </thead>
                        
                        
                    </table>
                </div>

            </div>
        </div>


</section>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>
<script>
    var base_url = '<?php echo base_url(); ?>';
     var row =text=offset='';

    $(document).ready(function () {
        getUser(base_url, row = 10, text = '',offset=1);
    });
    function getUser(base_url, row, text) {
        $('table .data-append').remove();
        $('table .foot').remove();
        $.ajax({
            type: "POST",
            url: base_url + "settings/getUser",
            data: 'row=' + row + '&text=' + text+'&offset='+offset,
            success: function (data) {
                if (data != '') {
                    
                    $('thead').after(data);
                    $('.foot').css('display', '');
                } else {
                    $('thead').after('No Data');
                    $('.foot').css('display', 'none');
                }
            }
        });
    }
    function deleteuser(id) {
        if (confirm('Are you sure you want to Delete?')) {
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>/settings/deleteUser",
                data: "id=" + id,
                success: function (data) {
                    if (data) {
                        if (data == 1) {
                            $('#' + id).remove();

                            window.location.reload();
                        } else {
                            alert('Permission denied');
                        }
                    }
                }
            });
        }
    }
    
    $('table').on('click','.rowscnt', function () {
    $('table .rowscnt').removeClass('active');
         row = $(this).text();
        $(this).addClass('active');
         text = $('#search').val();
        getUser(base_url, row, text,offset=1)
        console.log($(this).text());
    });
    $('.search').on('click', function () {
         text = $('#search').val();
        getUser(base_url, row = 10, text,offset=1)
    });
    $('.table').on('click','.offset',function(){
        $('table .offset').closest('li').removeClass('active');
       
         offset = $(this).text();
        getUser(base_url, row=10, text='',offset);

     $(this).closest('li').addClass('active');
    
        
    });
     $('.table').on('click','.next',function(){
       
       
         offset =  $(this).closest('ul').find('li.active').find('a').text();
//          $('table .offset').closest('li').removeClass('active');
         console.log(offset);
         var next=offset++;
          console.log(next);
        getUser(base_url, row=10, text='',next);
        
    });
        
</script>

<?php
include("common/footer.php");
?>