<?php
include("common/header.php");
//print_r($getAgent);
?>
<!-- main content -->
<style>
    .error{
        color: red;
    }
    .panel-heading {
  padding: 25px 15px;
  border-bottom: 1px solid #ebebeb; }
  .panel-body {
    padding: 15px;
}
</style>
<section id="main-content" class="main-content" style="">
    <section class="surround1">
        <div class="container-fixed add-listing">
  
<!-- User Information -->
        <form id="changepasswordForm" role="form" action="<?php echo base_url(); ?>myaccount/myaccount/updatepassword" method="post">
        <input type="hidden" id="echeck">
        <?php if ($this->session->flashdata('changepassword')) { ?>
            <div class="successUpdate alert alert-success" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                  </button>
                  <i class="fa fa-check"></i> <?php echo $this->session->flashdata('changepassword'); ?>
                </div>
        <?php } ?>
        <div class="panel">  

          <div class="panel-heading">
            
                <h3>Change password</h3>
              
          </div>  

          <div class="panel-body">

            <div class="row"> 
                <div class="col-md-12 p0">
                  <div class="form-group col-sm-6">
                  <label for="p-name">Old password <span style="color: red;">*</span></label>                                                  
                  <input name="old-password" placeholder="Enter confirm password"  type="password" class="form-control input-sm" onchange="checkOldPassword(this.value);" autocomplete="off">
                </div>
                </div>              
                <div class="col-md-12 p0">
                  <div class="form-group col-sm-6">
                  <label for="p-name">New password <span style="color: red;">*</span></label>                                                  
                  <input name="new-password" placeholder="Enter new password"  type="password" class="form-control input-sm" autocomplete="off">
                </div>
                </div>
                
                <div class="col-md-12 p0">
                  <div class="form-group col-sm-6">
                  <label for="p-name">Confirm password <span style="color: red;">*</span></label>                                                  
                  <input name="confirm-password" placeholder="Enter confirm password"  type="password" class="form-control input-sm" autocomplete="off">
                </div>
                </div>       
            </div>            
            
          </div>

          <div class="panel-title">
            <div class="save-cancel">
                <button type="button" class="btn btn-default submit">
                      Change password
                </button>
              <!-- <a href="" class="btn btn-default">Save</a> -->
            </div>
          </div>
          
        </div> 
         </form>


        </div> 
    </section>        
</section>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript">
var baseurl = "<?php echo base_url(); ?>";
$("#changepasswordForm").keyup(function(event){
        if(event.keyCode == 13){
            $(".submit").click();
        }
    });
//validation for agent 
$('.submit').on('click', function () {

  $('.error').remove();
    //Checking condition for fields
    if ($("input[name='old-password']").val() == '') {
        $("input[name='old-password']").after('<div class="col-md-12 p0 error">Please enter your old password</div>');
        $("input[name='old-password']").focus();
    } else if ($("input[name='new-password']").val() == '') {
        $("input[name='new-password']").after('<div class="col-md-12 p0 error">Please enter your new password</div>');
        $("input[name='new-password']").focus();
    } else if ($("input[name='new-password']").val().length < 8) {
        $("input[name='new-password']").after('<div class="col-md-12 p0 error">New password should be greater than 8 character</div>');
        $("input[name='new-password']").focus();
    } else if ($("input[name='confirm-password']").val() == '') {
        $("input[name='confirm-password']").after('<div class="col-md-12 p0 error">Please enter your confirm password</div>');
        $("input[name='confirm-password']").focus();
    } else if ($("input[name='new-password']").val() != $("input[name='confirm-password']").val()) {
        $("input[name='confirm-password']").after('<div class="col-md-12 p0 error">New password and confirm password should be same</div>');
        $("input[name='confirm-password']").focus();
    } else {
        setTimeout(function(){ 
          if ($("#echeck").val() == '1') {
            console.log("Hello 1");
            $("input[name='old-password']").after('<span class="error" style="color:red">Invalid old password</span>');
            $("input[name='old-password']").focus();
          } else {
            console.log("Hello 0");
             $('form#changepasswordForm').submit(); 
  //                 
          }
        }, 500);
    }

});
// function validationPassword() {
//     $('.error').remove();
//     //Checking condition for fields
//     if ($("input[name='old-password']").val() == '') {
//         $("input[name='old-password']").after('<div class="col-md-12 p0 error">Please enter your old password</div>');
//         $("input[name='old-password']").focus();
//     } else if ($("input[name='new-password']").val() == '') {
//         $("input[name='new-password']").after('<div class="col-md-12 p0 error">Please enter your new password</div>');
//         $("input[name='new-password']").focus();
//     } else if ($("input[name='new-password']").val().length < 8) {
//         $("input[name='new-password']").after('<div class="col-md-12 p0 error">New password should be greater than 8 character</div>');
//         $("input[name='new-password']").focus();
//     } else if ($("input[name='confirm-password']").val() == '') {
//         $("input[name='confirm-password']").after('<div class="col-md-12 p0 error">Please enter your confirm password</div>');
//         $("input[name='confirm-password']").focus();
//     } else if ($("input[name='new-password']").val() != $("input[name='confirm-password']").val()) {
//         $("input[name='confirm-password']").after('<div class="col-md-12 p0 error">New password and confirm password should be same</div>');
//         $("input[name='confirm-password']").focus();
//     } else {
//         setTimeout(function(){ 
//           if ($("#echeck").val() == '1') {
//             $("input[name='old-password']").after('<span class="error" style="color:red">Invalid old password</span>');
//             $("input[name='old-password']").focus();
//           } else {

//              //$('form#changepasswordForm').submit(); 
//   //                 
//           }
//         }, 500);
//     }
//     if($("#echeck").val() == '1' || $("#echeck").val() == ''){
//       return false;
//     }else{
//       return true;
//     }
    
// }

function checkOldPassword(id){
  $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>myaccount/myaccount/checkOldPassword",
      data: "oldpassword=" + id,
      dataType: "html",
      success: function (data) {

          $('#echeck').val(data);

      }
  });
}


</script>
<?php
include("common/footer.php");
?>

