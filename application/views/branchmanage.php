<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//$actualurl="http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
//include("common/settingmenu.php");
//print_r($agent);
?>
<style>
    .error{
        color: red;
    }
    .panel-heading {
  padding: 25px 15px;
  border-bottom: 1px solid #ebebeb; }
  .panel-body {
    padding: 15px;
}
</style>
<form id="add-to-listing-partial-form" name="add-to-listing-partial-form" class="" method="post"  enctype="multipart/form-data" action="<?php echo base_url(); ?>settings/agentinsert">
<input type="hidden" id="echeck" value="0"/>
<input type="hidden" name="id" value="<?php echo isset($agent[0]->agent_id) ? $agent[0]->agent_id : '0'; ?>"/>
<section class="surround1"> 

        <div class="container-fixed add-listing">
  
<!-- Basic info -->
        <div class="panel">  

          <div class="panel-heading">
            <div class="row">
              <div class="col-sm-6">
                <h3>Add Agent Branch</h3>
              </div>
              <div class="col-sm-6">
                <div class="save-cancel">
                  <a href="javascript:;" class="btn btn-default submit">Save</a>
                  <a href="javascript:;" onclick="window.history.go(-1);" class="btn btn-default">Cancel</a>
                </div>
              </div>
            </div>
          </div>        

          <div class="panel-title">
            <div class="row">
              <div class="col-sm-6 basic_info">
                Basic Infomation
              </div>
              <div class="col-sm-6 hidden-xs map_info cont_info">
                Contact Information
              </div>
            </div>
          </div>
          

          <div class="panel-body">
            <div class="row">
              <div class="col-sm-6">

              
              <div class="row">
                <div class="form-group col-sm-6">
                  <label for="agentrefno">Agent Reference <span style="color: #F00"> *</span></label>                                                  
                  <input type="text" id="agentrefno" name="agentrefno" class="form-control"  maxlength="10"  value="<?php echo isset($agent[0]->agent_refno) ? $agent[0]->agent_refno : ''; ?>">
                </div>

                <div class="form-group col-sm-6">
                  <label for="cName">Company Name <span style="color: #F00"> *</span></label>                                                  
                  <input type="text" id="cName" name="cName" class="form-control"  maxlength="50"  value="<?php echo isset($agent[0]->agent_name) ? $agent[0]->agent_name : ''; ?>" placeholder="Goren Gates">
                </div>

                <div class="form-group col-sm-6">
                  <label for="vatregno">VAT Registration Number</label>                                                
                  <input type="text" id="vatregno" name="vatregno" class="form-control"  maxlength="10"  value="<?php echo isset($agent[0]->vat_reg_no) ? $agent[0]->vat_reg_no : ''; ?>">
                </div>

                <div class="form-group col-sm-6">
                  <label for="vatregcty">VAT Registration Country</label>                                               
                  <input type="text" id="vatregcty" name="vatregcty" class="form-control" maxlength="30" value="<?php echo isset($agent[0]->vat_reg_country) ? $agent[0]->vat_reg_country : ''; ?>" />                                
                </div>

                <div class="form-group col-sm-6">
                  <label for="defcur">Default Currency </label>                                                  
                  <input type="text" id="defcur" name="defcur" class="form-control" maxlength="3" value="<?php echo isset($agent[0]->default_currency) ? $agent[0]->default_currency : 'INR'; ?>" />                                
                </div>

                <div class="form-group col-sm-6">
                  <label for="invoicedue">Invoice Due Days <span style="color: #F00"> *</span></label>                                                  
                  <input type="text" id="invoicedue" name="invoicedue" class="form-control" maxlength="3" value="<?php echo isset($agent[0]->invoice_due_day) ? $agent[0]->invoice_due_day : ''; ?>" />                                
                </div>

                <div class="form-group col-sm-6">
                  <label for="workstart">Start Time </label>                                                  
                  <input type="text" id="workstart" name="workstart" class="form-control" maxlength="3" value="<?php echo isset($agent[0]->work_start_time) ? $agent[0]->work_start_time : ''; ?>" readonly/>                                
                </div>

                <div class="form-group col-sm-6">
                  <label for="workend">End Time </label>                                                  
                  <input type="text" id="workend" name="workend" class="form-control" maxlength="3" value="<?php echo isset($agent[0]->work_end_time) ? $agent[0]->work_end_time : ''; ?>" readonly/>                                
                </div>

                <div class="form-group col-sm-12">
                  <label for="AgentTypeId">Agent Type <span style="color: #F00"> *</span></label>                                                  
                  <select id="AgentTypeId" name="AgentTypeId" class="form-control">
                    <option value="0">- Select -</option>
                    <?php
                    $check = '';
                    foreach ($agenttype as $key => $type) {
                        if ($agent[0]->agent_type == $type->agent_id) {
                            $check = 'selected="selected"';
                        }
                        echo "<option $check value=" . $type->agent_id . ">" . $type->agent_name . "</option>";
                    }
                    ?>
                </select> 
                </div>

                

              </div>
              
         
                 
                
              </div>

              <div class="col-md-6">

              
              <div class="row">
                <div class="form-group col-sm-3">
                    <label for="title">Title <span style="color: #F00"> *</span></label>                                                  
                    <select id="title" name="title" class="form-control">
                        <option value="MR" selected="selected">MR</option>
                        <option value="MRS">MRS</option>
                    </select>
                </div>

                <div class="form-group col-sm-4">
                  <label for="Forename">Forename <span style="color: #F00"> *</span></label>                                                  
                  <input type="text" name="Forename" id="Forename" maxlength="30" value="<?php echo isset($agent[0]->forename) ? $agent[0]->forename : ''; ?>" class="form-control" />                                
                </div>

                <div class="form-group col-sm-5">
                  <label for="Surname">Surname <span style="color: #F00"> *</span></label>                                                
                  <input type="text" name="Surname" id="Surname" maxlength="30" value="<?php echo isset($agent[0]->surname) ? $agent[0]->surname : ''; ?>" class="form-control" />                                
                </div>

                <div class="form-group col-sm-6">
                  <label for="Email">Email address <span style="color: #F00"> *</span></label>                                               
                  <input type="text" id="Email" name="Email" maxlength="30" onchange="checkemail(this.value);" value="<?php echo isset($agent[0]->email_id) ? $agent[0]->email_id : ''; ?>" <?php echo isset($agent[0]->email_id) ? "readonly" : ''; ?> class="form-control" />
                </div>

                <div class="form-group col-sm-6">
                  <label for="Phone">Phone Number <span style="color: #F00"> *</span></label>                                                  
                  <input type="text" id="Phone" name="Phone" maxlength="15" value="<?php echo isset($agent[0]->phone_no) ? $agent[0]->phone_no : ''; ?>" class="form-control"/>
                </div>

                <div class="form-group col-sm-6">
                  <label for="Fax">Fax <span style="color: #F00"> *</span></label>                                                  
                  <input type="text" id="Fax" name="Fax" maxlength="10" value="<?php echo isset($agent[0]->fax) ? $agent[0]->fax : ''; ?>" class="form-control" />
                </div>

                <div class="form-group col-sm-6">
                  <label for="Website">Website</label>                                                  
                  <input type="text" id="Website" name="Website" maxlength="100" value="<?php echo isset($agent[0]->website) ? $agent[0]->website : ''; ?>" class="form-control"  placeholder="eg: http://www.example.com"/>
                </div>
                <div class="form-group col-sm-6">
                  <label for="Address_number">Number <span style="color: #F00"> *</span></label>                                                  
                  <input type="text" name="Address_number" id="Address_number" maxlength="200" value="<?php echo isset($agent[0]->number) ? $agent[0]->number : ''; ?>" class="form-control" />                                
                </div>
                <div class="form-group col-sm-6">
                  <label for="Address_Street">Street <span style="color: #F00"> *</span></label>                                                  
                  <input type="text" name="AddressStreet" id="Address_Street" maxlength="200" value="<?php echo isset($agent[0]->street) ? $agent[0]->street : ''; ?>" class="form-control" />                                
                </div>
                <div class="form-group col-sm-6">
                  <label for="Address_CityName">City <span style="color: #F00"> *</span></label>                                                  
                  <input type="text" name="AddressCityName" id="Address_CityName" maxlength="50" value="<?php echo isset($agent[0]->town) ? $agent[0]->town : ''; ?>" class="form-control" />                                
                <input type="hidden" name="Latitude" id="Latitude" value="<?php echo isset($agent[0]->agent_lat) ? $agent[0]->agent_lat : '0'; ?>"/>
                <input type="hidden" name="Longitude" id="Longitude" value="<?php echo isset($agent[0]->agent_long) ? $agent[0]->agent_long : '0'; ?>"/>
                </div>
                <div class="form-group col-sm-6">
                  <label for="Address_County">County <span style="color: #F00"> *</span></label>                                                  
                  <input type="text" name="AddressCounty" id="Address_County" maxlength="50" value="<?php echo isset($agent[0]->county) ? $agent[0]->county : ''; ?>" class="form-control" />                               
                </div>
                <div class="form-group col-sm-6">
                  <label for="Address_Zip">Post Code <span style="color: #F00"> *</span></label>                                                  
                  <input type="text" name="AddressZip" id="Address_Zip" maxlength="10" value="<?php echo isset($agent[0]->post_code) ? $agent[0]->post_code : ''; ?>" placeholder="e.g. W4 2LT rather than W42LT" class="form-control" >
                </div>

              </div>
                
              </div>
          </div>
          
        </div> 

        <!-- contact info -->
        <div class="panel">      

          <div class="panel-title">
            <div class="row">
              <div class="col-md-12 hidden-sm hidden-xs age_logo">
                Agent Logo
              </div>
            </div>
          </div>
          

          <div class="panel-body">
            <div class="row">
              

              <div class="col-md-6 col-xs-12">

              <h5 class="visible-xs visible-sm"><strong>Agent Logo</strong></h5>
                
                <figure>
                  <img alt="100%x180" src="http://admin.easamove.co.uk/images/<?php echo isset($agent[0]->agent_logo) ? $agent[0]->agent_logo : 'noimage.jpeg'; ?>" id="preview" style="height: 75px; display: block;">
                  <p class="help-block">We support png, jpeg and jpg files</p class="help-block">
                </figure>

                <label class="btn btn-primary">
                    <i class="fa fa-plus"></i>
                    Click to add photos
                    <input type="file" name="agentImage" style="display: none" class="DN" id="Photo_Upload_Control"/>
                    <input type="hidden" name="imageedit" id="imageedit" value="<?php echo isset($agent[0]->agent_logo) ? $agent[0]->agent_logo : ''; ?>"/>
                </label>
                                
              </div>

            </div>
          </div>
          
        </div> 



<!-- About -->
        <div class="panel media document">
          <div class="panel-title">
            Agent Members 
          </div>
          <div class="panel-body">
          <div class="row mb10">
              <?php 
              foreach($agentMemeber as $age){
              ?>
              <div class="col-md-2">
                <div class="form-group">
                  <label class="checkbox-inline">
                    <input type="checkbox" id="inlineCheckbox1" <?php echo isChecked($age->member_id, $agent[0]->member_id); ?> name="agent-member[]" value="<?php echo $age->member_id; ?>"> <img src="http://admin.easamove.co.uk/images/agent_member/<?php echo $age->member_logo; ?>" style="height: 50px;">
                  </label>
                </div>
              </div>
              <?php } ?>
              <div class="col-md-12">
                <p>Note: please choose your members.</p>
              </div>
          </div>
          </div>
          <div class="panel-title">
            About 
          </div>

          <div class="panel-body">            
            <div class="row">
              <div class="col-sm-12">
                <!-- <label for="full-describe">Summary</label> -->
                    <textarea rows="3" id="About" name="About" max-length="4000" class="form-control ckeditor"><?php echo isset($agent[0]->agent_aboutus) ? $agent[0]->agent_aboutus : ''; ?></textarea>
                    <p class="help-block">Maximum character: 4000</p>                    
              </div>              
            </div>            
            
          </div>

          <div class="panel-title">
            <div class="save-cancel">
              <button type="button" class="btn btn-default submit">Save</button>
              <a href="javascript:;" onclick="window.history.go(-1);" class="btn btn-default">Cancel</a>
            </div>
          </div>


        </div>          

        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                      <!--<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->

                    </div>
                    <div class="modal-body" style="text-align: center">
                        <img  src="<?php echo base_url(); ?>images/ajax-loader.gif"/>
                        <h1>Please Wait ....</h1>
                    </div>

                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div>
          


        
      </section>
</form>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>


<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.ui.timepicker.js?v=0.3.3"></script>
<!-- jQuery -->
<script type="text/javascript">

var base_url = '<?php echo base_url(); ?>';
// var cont = 'Agent';
function checkemail(email) {
  console.log("Hello");
    if ($("input[name='id']").val() == 0) {
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>/settings/checkemail",
            data: "usermail=" + email,
            dataType: "html",
            success: function (data) {

                $('#echeck').val(data);

            }
        });
    }

}
//initialize();
$(document).ready(function () {
//        initAutocomplete();
});
// $('.submit').on('click', function () {
//     $('form#add-to-listing-partial-form').submit();
// });
$("form#add-to-listing-partial-form").keyup(function(event){
    if(event.keyCode == 13){
        $(".submit").click();
    }
});
$('.submit').on('click', function (e) {
  //console.log("Hello");
    var letter = /^[a-zA-Z\s]*$/;
    var emailPattern = /(?![0-9])^[a-z0-9%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i;
    var mobilePattern = /^(?:(?:\+?[0-9]{2}|0|0[0-9]{2}|\+|\+[0-9]{2}\s*(?:[.-]\s*)?)?(?:\(\s*([2-9][02-9]|[2-9][02-8]1|[2-9][02-8][02-9])\s*\)|([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9]))\s*(?:[.-]\s*)?)?([2-9]1[02-9]|[2-9][02-9]1|[2-9][02-9]{2})\s*(?:[.-]\s*)?([0-9]{4})(?:\s*(?:#|x\.?|ext\.?|extension)\s*(\d+))?$/;
    var faxpattern = /^\+?[0-9]{6,}$/;
    var website = /^(http:\/\/|https:\/\/|ftp:\/\/|)[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/i;
    $('.error').remove();
    if ($("input[name='agentrefno']").val() == '') {
        $(".basic_info").append('<span class="error">Please enter agent refernce no</span>');
        $("input[name='agentrefno']").focus();
    } else if ($("input[name='cName']").val() == '') {
        $(".basic_info").append('<span class="error">Please enter company name</span>');
        $("input[name='cName']").focus();
    } else if ($("input[name='defcur']").val() == '') {
        $(".basic_info").append('<span class="error">Please enter default currency</span>');
        $("input[name='defcur']").focus();
    } else if ($("input[name='invoicedue']").val() == '') {
        $(".basic_info").append('<span class="error">Please enter invoice due</span>');
        $("input[name='invoicedue']").focus();
    } else if ($("select[name='AgentTypeId']").val() == '0') {
        $(".basic_info").append('<span class="error">Please enter agent type</span>');
        $("select[name='AgentTypeId']").focus();
    }else if ($("select[name='title']").val() == '0') {
        $(".cont_info").append('<span class="error">Please enter title.</span>');
        $("select[name='title']").focus();
    } else if ($("input[name='Forename']").val() == '') {
        $(".cont_info").append('<span class="error">Please enter forename</span>');
        $("input[name='Forename']").focus();
    } else if (!letter.test($("input[name='Forename']").val())) {
        $(".cont_info").append('<span class="error">Please use letter in your forename</span>');
        $("input[name='Forename']").focus();
    } else if ($("input[name='Surname']").val() == '') {
        $(".cont_info").append('<span class="error">Please enter surname</span>');
        $("input[name='Surname']").focus();
    } else if (!letter.test($("input[name='Surname']").val())) {
        $(".cont_info").append('<span class="error">Please use letter in your surname</span>');
        $("input[name='Surname']").focus();
    } else if ($("input[name='Email']").val() == '') {
        $(".cont_info").append('<span class="error">Please enter email address</span>');
        $("input[name='Email']").focus();
    } else if (!emailPattern.test($("input[name='Email']").val())) {
        $(".cont_info").append('<span class="error">Please enter valid email address</span>');
        $("input[name='Email']").focus();
    } else if ($("input[name='Phone']").val() == '') {
        $(".cont_info").append('<span class="error">Please enter phone number</span>');
        $("input[name='Phone']").focus();
    } else if (!mobilePattern.test($("input[name='Phone']").val())) {
        $(".cont_info").append('<span class="error">Please enter valid phone number</span>');
        $("input[name='Phone']").focus();
    } else if ($("input[name='Fax']").val() == '') {
        $(".cont_info").append('<span class="error">Please enter fax number</span>');
        $("input[name='Fax']").focus();
    } else if (!faxpattern.test($("input[name='Fax']").val())) {
        $(".cont_info").append('<span class="error">Please enter valid fax number</span>');
        $("input[name='Fax']").focus();
    } else if (!website.test($("input[name='Website']").val()) && $("input[name='Website']").val() != "") {
        $(".cont_info").append('<span class="error">Please enter valid Website address</span>');
        $("input[name='Website']").focus();
    } else if ($("input[name='Address_number']").val() == '') {
        $(".cont_info").append('<span class="error">Please enter number</span>');
        $("input[name='Address_number']").focus();
    } else if ($("input[name='AddressStreet']").val() == '') {
        $(".cont_info").append('<span class="error">Please enter street name</span>');
        $("input[name='AddressStreet']").focus();
    } else if ($("input[name='AddressCounty']").val() == '') {
        $(".cont_info").append('<span class="error">Please county name</span>');
        $("input[name='AddressCounty']").focus();
    } else if ($("input[name='AddressCityName']").val() == '') {
        $(".cont_info").append('<span class="error">Please enter city name</span>');
        $("input[name='AddressCityName']").focus();
    } else if ($("input[name='AddressZip']").val() == '') {
        $(".cont_info").append('<span class="error">Please enter postcode</span>');
        $("input[name='AddressZip']").focus();
    } else if ($("input[name='agentImage']").val() == '' && $('#imageedit').val() == '') {
        $(".age_logo").append('<span class="error">Please fill this field</span>');
        $("#preview").focus();
    } else {
       // checkemail($("input[name='Email']").val());
        $('#myModal').modal('show');
        setTimeout(function(){ 
          if ($("#echeck").val() == '1') {
            $('#myModal').modal('hide');
            $(".cont_info").append('<span class="error" style="color:red">email address already registered</span>');
            $("input[name='Email']").focus();
        } else {
            // var formData = new FormData(this);
            // ajaxAgents(base_url,formData);
            //console.log("Coming");
            $('#myModal').modal('show');
            $('form#add-to-listing-partial-form').submit();
//                 
        }
        }, 500);
        


    }
});

function readURL(input) {

    if (input.files && input.files[0]) {
        var extension = input.files[0].name.substring(input.files[0].name.lastIndexOf('.'));
        console.log('here image ' + extension);
        if (extension == '.jpeg' || extension == '.png' || extension == '.jpg') {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#preview').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        } else {
            $("#preview").after('<span class="error">Please choose valid file format</span>');
            $("#preview").focus();
        }
    }
}

$("#Photo_Upload_Control").change(function () {
    $('.error').remove();
    readURL(this);

});
//                $('#loadmap').on('click', function () {
//                    $('#myModal').modal();
//                    initialize();
//                });
function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

// var geocoder = new google.maps.Geocoder();

// function geocodePosition(pos) {
//     geocoder.geocode({
//         latLng: pos
//     }, function (responses) {
//         if (responses && responses.length > 0) {

//             updateMarkerAddress(responses[0].formatted_address);
//             var add = responses[0].formatted_address;
//             var str = add.split(',');
//             var india = str[str.length - 1];
//             var city = str[str.length - 2];
//             var street = str;
//             $('#Address_Zip').val();
//             $('#Address_Street').val(street.toString());
//             $('#Address_CityName').val(city);
//             $('#Address_County').val(india);
//         } else {
//             updateMarkerAddress('Cannot determine address at this location.');
//         }
//     });
// }
// function getcode() {
//     var address = $('#pac-input').val();
//     console.log(address);
//     geocoder.geocode({address: address}, function (results, status) {
//         console.log(results);
//         if (status == google.maps.GeocoderStatus.OK)
//         {

//             map.setCenter(results[0].geometry.location);
//             marker = new google.maps.Marker({
//                 position: results[0].geometry.location,
//                 title: 'Point A',
//                 map: map,
//                 draggable: true
//             });
//             geocodePosition(results[0].geometry.location)
//             updateMarkerPosition(marker.getPosition());
//             geocodePosition(marker.getPosition());

//             google.maps.event.addListener(marker, 'dragend', function () {
//                 updateMarkerStatus('Drag ended');
//                 geocodePosition(marker.getPosition());
//                 updateMarkerPosition(marker.getPosition());
//             });


//         }
//         else
//         {
//             alert("Geocode was not successful for the following reason: " + status);
//         }
//     });

// }
// function updateMarkerStatus(str) {
// }

// function updateMarkerPosition(latLng) {

//     document.getElementById("Latitude").value = latLng.lat();
//     document.getElementById("Longitude").value = latLng.lng();
// }

// function updateMarkerAddress(str) {
// //  document.getElementById('address').innerHTML = str;
// }
// var image = 'http://www.google.com/intl/en_us/mapfiles/ms/micons/blue-dot.png';
// function initialize() {
//     var latLng = new google.maps.LatLng(-34.397, 150.644);

//     map = new google.maps.Map(document.getElementById('map'), {
//         zoom: 8,
//         center: latLng,
//         mapTypeId: google.maps.MapTypeId.ROADMAP
//     });
//     marker = new google.maps.Marker({
//         position: latLng,
//         title: 'Point A',
//         map: map,
//         draggable: true
//     });

//     // Update current position info.
//     updateMarkerPosition(latLng);
//     geocodePosition(latLng);
//     var input = document.getElementById('pac-input');
//     var autocomplete = new google.maps.places.Autocomplete(input, {
//         types: ["geocode"]
//     });

//     autocomplete.bindTo('bounds', map);
//     var infowindow = new google.maps.InfoWindow();
//     // Add dragging event listeners.
//     google.maps.event.addListener(marker, 'dragstart', function () {
//         updateMarkerAddress('Dragging...');
//         updateMarkerPosition(marker.getPosition());
//     });

//     google.maps.event.addListener(marker, 'drag', function () {
//         updateMarkerStatus('Dragging...');
//         updateMarkerPosition(marker.getPosition());
//     });

//     google.maps.event.addListener(marker, 'dragend', function () {
//         updateMarkerStatus('Drag ended');
//         geocodePosition(marker.getPosition());
//         updateMarkerPosition(marker.getPosition());
//     });
//     google.maps.event.addListener(autocomplete, 'place_changed', function () {

//         var place = autocomplete.getPlace();
//         if (place.geometry.viewport) {
//             map.fitBounds(place.geometry.viewport);
//         } else {
//             map.setCenter(place.geometry.location);
//             map.setZoom(17);
//         }


//         moveMarker(place.name, place.geometry.location);

//     });
// }
// function moveMarker(placeName, latlng) {
//     marker.setIcon(image);
//     marker.setPosition(latlng);
//     infowindow.setContent(placeName);
//     infowindow.open(map, marker);
// }
// function geocodePosition(pos) {
//     var geocoder = new google.maps.Geocoder();
//     geocoder.geocode({
//         latLng: pos
//     }, function (responses) {
//         if (responses && responses.length > 0) {
//             console.log(responses[0].formatted_address);
//             var add = responses[0].formatted_address;
//             var str = add.split(',');
//             var india = str[str.length - 1];
//             var city = str[str.length - 2];
//             var street = str;
//             $('#Address_Zip').val();
//             $('#Address_Street').val(street.toString());
//             $('#Address_CityName').val(city);
//             $('#Address_County').val(india);

//         } else {

//         }
//     });
// }
// $("#pac-input").focusin(function () {
//     $(document).keypress(function (e) {
//         if (e.which == 13) {
// //                    infowindow.close();
//             var firstResult = $(".pac-container .pac-item:first").text();

//             var geocoder = new google.maps.Geocoder();
//             geocoder.geocode({"address": firstResult}, function (results, status) {
//                 if (status == google.maps.GeocoderStatus.OK) {
//                     var lat = results[0].geometry.location.lat(),
//                             lng = results[0].geometry.location.lng(),
//                             placeName = results[0].address_components[0].long_name,
//                             latlng = new google.maps.LatLng(lat, lng);
//                     document.getElementById("Latitude").value = lat;
//                     document.getElementById("Longitude").value = lng;

//                     moveMarker(placeName, latlng);
//                     $("#pac-input").val(firstResult);
//                 }
//             });
//         }
//     });
// });
$("input[name='agentrefno']").on('keyup', function () {
    $('.error').remove();
    if ($("input[name='id']").val() == 0) {
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>settings/checkref",
            data: "ref=" + $("input[name='agentrefno']").val(),
            dataType: 'html',
            success: function (data) {
                if (data == 1) {
                    $("input[name='agentrefno']").after('<span class="error">Refernce number already exist</span>');
                    $('.submit').prop('disabled', true);
                } else {
                    $("input[name='agentrefno']").after('<span class="error"></span>');
                    $('.submit').prop('disabled', false);
                }
            }
        });
    }
});
$(document).ready(function () {
    $('#workstart').timepicker({
        showPeriod: true,
        showLeadingZero: true,
    });

    $('#workend').timepicker({
        showPeriod: true,
        showLeadingZero: true,
    });
});


function ajaxAgents(base_url, datas) {
    $('.submit').text('Processing');
    $.ajax({
        type: "POST",
        url: base_url + "settings/agentinsert",
        data: datas,
        mimeType: "multipart/form-data",
        contentType: false,
        cache: false,
        processData: false,
        success: function (data) {

            alert('Saved Successfully');
            $("input[name='id']").val('0');
            $('#add-to-listing-partial-form')[0].reset();
            window.location.reload();
        }
    });
}     
</script>
