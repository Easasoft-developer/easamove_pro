<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Easamove</title>
    <meta http-equiv='content-Control' content='no-cache' />
    <meta http-equiv='Cache-Control' content='no-cache' />
    <meta http-equiv='Expires' content='0' />
    <meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
    <meta http-equiv="content-language" content="en" />
    <meta name="robots" content="noindex, follow" />
  </head>


  <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" border="1" style="font-family: Arial,Helvetica Neue,Helvetica,sans-serif;font-size:14px;color:#333;">

    <table width="650" border="0" cellspacing="0" cellpadding="0" background="http://pro.easamove.co.uk/images/mailer-header.jpg" height="181" style="margin:auto;padding:20px;text-align:center;color:white;">
      <tr>
        <td style="padding-top:20px;">
          <img width="253" src="http://pro.easamove.co.uk/images/logo.png">
        </td>
      </tr>
      <tr>
        <td><h3 style="font-size:24px;margin: 15px 0px 10px;"><b>Change Password</b></h3></td>
      </tr>

      <tr>
        <td><img src="http://pro.easamove.co.uk/images/waves.png"></td>
      </tr>
    </table>

    <table width="650" border="0" cellspacing="0" cellpadding="0" style=" border-right: 1px solid #ccc;  border-left: 1px solid #ccc; margin:0 auto; height:auto;font-size:13px; font-family: Arial,Helvetica Neue,Helvetica,sans-serif;padding: 20px;">

      <tr>
        <td><h5 style="font-size:14px;margin: 10px 0px;"><b>Hello <?php echo $name; ?>,</b></h5></td>
      </tr>

    <tr>
        <td style="line-height:1.5;padding-bottom:10px;">We received a request to change the password for your Easapro account for email address <a href="mailto:menonjats@gmail.com" style="color:#ff9000;">menonjats@gmail.com</a>.</td>
      </tr>

      <tr>
        <td style="line-height:1.5;padding-bottom:10px;">If you want to change your password, please click on the link below (or copy and paste the URL into
        your browser) <a href="<?php echo $link;?>" style="color:#ff9000;"><?php echo $link;?></a>.</td>
      </tr>

      <tr>
        <td style="line-height:1.5;padding-bottom:10px;">This link takes you to a secure page where you can change your password which needs to have a minimum of 8 characters.</td>
      </tr>

      <tr>
        <td style="line-height:1.5;padding-bottom:10px;">If you don't want to change your password, you can safely ignore this email and continue using Easapro with your existing password. We're happy to help, so please feel free to contact us with any questions or feedback. </td>
      </tr>

    <tr>
        <td style="line-height:1.5;padding-bottom:5px;padding-top:10px;">Best Regards,</td>
      </tr>

      <tr>
        <td style="line-height:1.5;padding-bottom:10px;">Easamove team</td>
      </tr>
      
    </table>


    <table width="650" border="0" cellspacing="0" cellpadding="0" style=" font-family: Arial,Helvetica Neue,Helvetica,sans-serif; font-size:13px; margin:0 auto; height:auto; background:#efefef;border:1px solid #ccc;border-top:0;padding: 10px 20px;">
      <tr>
        <td style="padding:10px 0px;">
          <img width="135" src="http://pro.easamove.co.uk/images/logo-footer.png">
        </td>
        <td style=" font-size:12px; padding:10px 0px;" align="right">
          © 2016 Easamove Property.  All rights reserved.
        </td>
        <td style="padding:10px 0px;" align="right">
          <ul style="list-style:none;margin: 0px;">
            <li style="float:left;margin-left: 0px;"><a href="" style="padding: 0px 5px;display: block;"><img width="16" src="http://pro.easamove.co.uk/images/facebook.png"></a></li>
            <li style="float:left;margin-left: 0px;"><a href="" style="padding: 0px 5px;display: block;"><img width="16" src="http://pro.easamove.co.uk/images/twitter.png"></a></li>
            <li style="float:left;margin-left: 0px;"><a href="" style="padding: 0px 5px;display: block;"><img width="18" src="http://pro.easamove.co.uk/images/google-plus.png"></a></li>
            <li style="float:left;margin-left: 0px;"><a href="" style="display: block;"><img width="24" src="http://pro.easamove.co.uk/images/linkedin.png"></a></li>
            <li style="float:left;margin-left: 0px;"><a href="" style="padding: 0px 5px;display: block;"><img width="20" src="http://pro.easamove.co.uk/images/youtube.png"></a></li>
          </ul>
        </td>
      </tr>
    </table>

    </table>
  </body>
</html>




<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Easamove Admin</title>
</head>
<body>
<table width ="550px" border="0" cellspacing="0" cellpadding="0" style=" border-top: 1px solid #f2f2f2;  border-right: 1px solid #f2f2f2;  border-left: 1px solid #f2f2f2;font-family:'Open Sans'; font-size:14px; margin:0 auto; height:auto; background-color:#fffff6;">
	<tr valign="top" style="background-color:#f2f2f2;text-align: center;">
    	<td width="10%" style=" padding-bottom: 5px; padding-top: 5px;"><img src="" alt="EASAMOVE"></td>

        <!-- <td style="padding:20px 0 20px 15px; font-size: 24px;"></td> -->
    </tr>
    <!-- <tr valign="top" style="background-color:#f2f2f2;text-align: center;">
        <td width="10%" style=" padding-bottom: 10px;"><span style="padding:20px 0 5px 15px; font-size: 15px; font-family:'MyriadSetPro-Text';letter-spacing: 5px; color:#777;">ONLINE WORKSHOP ON FILMMAKING</span></td>
    </tr> -->
    <table width ="550px" border="0" cellspacing="0" cellpadding="0" style=" border-right: 1px solid #f2f2f2;  border-left: 1px solid #f2f2f2; margin:0 auto; height:auto; background-color:#ffffff; font-family:'Open Sans'; padding:0 15px 0 15px;">
   		
        <tr>
        	<td style=" font-size:14px; font-weight:400; color:#555353; padding:15px 0 0 0;">Hello <?php echo $name; ?><br /><br />
            Thank you for signing up with us. To verify your registration, please click on the link below.</td>
        </tr>
        
        <tr>
        	<td style=" font-size:14px; font-weight:400; color:#555353; padding:15px 0 0 0;">
             <a style="color:#2a98d7" href="http://www.easasoft.com/easamove/pro.easamove/Account/resetpassword?user=<?php echo $link;?>">Click here to reset you password</a></td>
        </tr>
        
         <tr>
             <td>
                 <h1 style="font-size:15px; color:#333; font-weight:500; margin:26px 0 0 0px;">Thanks,</h1>
                 <h2 style="font-size:14px; color:#444; font-weight:500; margin:0 0 0 0px; padding:3px 0 24px 0">Easamove</h2>
             </td>
          </tr>
   </table>
   
   <table width ="550px" border="0" cellspacing="0" cellpadding="0" style="  font-family:'Open Sans'; font-size:14px; margin:0 auto; height:auto; background-color:#f7fbfe;border-left:1px solid #cadce6;border-right:1px solid #cadce6;border-bottom:1px solid #cadce6;">
        <tr valign="top" style="background-color:#fff; ">
            <td style="text-align:center;border-top:1px solid #333;">
              <a href="" style="padding:2px 3px;"><img src="http://easasoft.com/onlinestudies/images/facebook1.png"></a>
              <a href="" style="padding:2px 3px;"><img src="http://easasoft.com/onlinestudies/images/youtube1.png"></a>
              <a href="" style="padding:2px 3px;"><img src="http://easasoft.com/onlinestudies/images/twitter1.png"></a>
            </td>
        </tr>
        <tr valign="top" style="background-color:#fff; ">
            <td><h5 style="text-align:center;font-size:12px; color:#718591; font-weight:400; margin: 0 10px; padding: 5px 0;">Copyright © <?php echo date('Y'); ?> -easamove.co.in. All rights reserved
            
            </h5></td>
        </tr>
        
    </table>
</table>
</body>
</html>
