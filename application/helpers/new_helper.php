<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

//Get saved properties
if (!function_exists('isChecked')) {

    function isChecked($value, $member) {

        $array = explode(',', $member);

      if(in_array($value, $array)){
        return "checked";
      }else{
        return "";
      }
    }

}

//Get Agent letting information for fees apply
if (!function_exists('getAgentLettingInformation')) {

    function getAgentLettingInformation($id) {

        $CI = &get_instance();
        $query = $CI->db->query("SELECT letting_information FROM easamoveadmin_tbl_agent
 WHERE agent_id = '$id'");

        if ($query->num_rows() > 0) {
            $row = $query->row();
            return $row->letting_information;
        }
    }

}
