<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Reports extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('Agent_model');
        $this->load->library('session');
        $this->load->library('form_validation');
        $this->load->model('Report_model');
        $this->load->model('Prop_model');

        // Your own constructor code
    }

    public function index() {
        if (isset($this->session->userdata['agentuser']['logged_in']) && $this->session->userdata['agentuser']['logged_in'] == TRUE) {
//            $data['salecount'] = $this->Agent_model->getSalepropcount();
//            $data['rentcount'] = $this->Agent_model->getRentpropcount();
            $data['saleemail'] = $this->Report_model->getsaleemailcount();
            $data['rentemail'] = $this->Report_model->getrentemailcount();
            $this->load->view('common/header');
            $this->load->view('common/reportmenu');
            $this->load->view('reports/activity',$data);
            $this->load->view('common/footer');
        } else {

            $this->load->view('login');
        }
    }
    public function getActivity(){
         $result = $this->Report_model->activitySale();
        $result2 = $this->Report_model->activityRent();
        $html = '';
        $html = '<tbody class="data-append">';
       if(!empty($result)){
                $html.='<tr>
                   <td>Sale</td>
                   <td>' . $result[0]->HA1 . '</td>
                   <td>' . $result[0]->HA2 . '</td>
                   <td>' . $result[0]->HA3 . '</td>
                   <td>' . $result[0]->HA4 . '</td>
                   <td>' . $result[0]->HA5 . '</td>
                   <td>' . $result[0]->other . '</td>
                   <td>' . ($result[0]->HA5+$result[0]->HA2+$result[0]->HA3+$result[0]->HA4+$result[0]->HA5+$result[0]->other). '</td>';
             
                $html.='</tr>';
                $html.='<tr>
                   <td>To Rent</td>
                   <td>' . $result2[0]->HA1 . '</td>
                   <td>' . $result2[0]->HA2 . '</td>
                   <td>' . $result2[0]->HA3 . '</td>
                   <td>' . $result2[0]->HA4 . '</td>
                   <td>' . $result2[0]->HA5 . '</td>
                   <td>' . $result2[0]->other. '</td>
                   <td>' . ($result2[0]->HA5+$result2[0]->HA2+$result2[0]->HA3+$result2[0]->HA4+$result2[0]->HA5+$result2[0]->other)  . '</td>';
             
                $html.='</tr>';
                 $html.='<tr>
                   <td>Total</td>
                   <td>' . ($result2[0]->HA1+$result[0]->HA1) . '</td>
                   <td>' . ($result2[0]->HA2+$result[0]->HA2) . '</td>
                   <td>' . ($result2[0]->HA3+$result[0]->HA3) . '</td>
                   <td>' . ($result2[0]->HA4+$result[0]->HA4) . '</td>
                   <td>' . ($result2[0]->HA5+$result[0]->HA5) . '</td>
                   <td>' . ($result2[0]->other+$result[0]->other). '</td>
                   <td>' . ($result2[0]->HA5+$result2[0]->HA2+$result2[0]->HA3+$result2[0]->HA4+$result2[0]->HA5+$result2[0]->other+$result[0]->HA5+$result[0]->HA2+$result[0]->HA3+$result[0]->HA4+$result[0]->HA5+$result[0]->other)  . '</td>';
             
                $html.='</tr>';
            
        } else {
            $html.='<tr><td colspan="7">No Record Found</td></tr>';
        }
          $html.='</tbody>';
        echo $html;
    }
    public function getCompetitor(){
        if (isset($this->session->userdata['agentuser']['logged_in']) && $this->session->userdata['agentuser']['logged_in'] == TRUE) {
//            $data['salecount'] = $this->Agent_model->getSalepropcount();
//            $data['rentcount'] = $this->Agent_model->getRentpropcount();
            $this->load->view('common/header');
            $this->load->view('common/reportmenu');
            $this->load->view('reports/competitor');
            $this->load->view('common/footer');
        } else {

            $this->load->view('login');
        }
    }
}
