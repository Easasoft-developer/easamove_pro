<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Myaccount extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('Myaccount_model');
        $this->load->library('session');
        $this->load->library('form_validation');

        // Your own constructor code
    }

    //myaccount index method starts here
    public function index() {
        $data['getAgent'] = $this->Myaccount_model->getAgentDetail();
        $data['agentImage'] = $this->Myaccount_model->agentImage();
        $data['agentMemeber'] = $this->Myaccount_model->agentMemeber();
        $this->load->view('myaccount_view', $data);
    }

    public function updateAgent() {
//        print_r($_POST);
//        exit();
        $ageng_id = $this->input->post('getAgentId');
        $svar = $this->session->userdata['agentuser'];
        $name = $svar['name'];
        if ($ageng_id > 0) {
            $config = array(
                'upload_path' => $_SERVER['DOCUMENT_ROOT'].'/../admin.easamove.co.uk/images/',
                'allowed_types' => 'jpg|gif|png|jpeg',
                'overwrite' => 1,
//            'file_name' =>$newFileName,
            );
//           print_r($_FILES);exit();
            if ($_FILES['agent_logo']['name'] != '') {
                $this->load->library('upload', $config);
          // print_r($_FILES);exit();
                $this->upload->initialize($config);
                if (!$this->upload->do_upload('agent_logo')) {
                    $error = array('error' => $this->upload->display_errors());
                } else {
                    $data1 = $this->upload->data();

                    $file_path = $data1['file_path'];
                    $file = $data1['full_path'];
                    $file_ext = $data1['file_ext'];
//                    print_r($data1);exit();
                     $final_file_name = $name . '_' . time() . '' . $file_ext;
                    //exit();
                    $rename = rename($file, $file_path . $final_file_name);
//                    exit();
                }
            } else {
                $final_file_name = $this->input->post('agent_img');
            }
            $agentmember = $this->input->post('agent-member');
            $extractMember = implode(",", $agentmember);
            $data = array(
                'forename' => $this->input->post('forename'),
                'surname' => $this->input->post('surname'),
                'phone_no' => $this->input->post('p-number'),
                'fax' => $this->input->post('f-number'),
                'website' => $this->input->post('website'),
                'agent_aboutus' => $this->input->post('agent_desc'),
                'agent_logo' => $final_file_name,
                'agent_modified_on' => date('Y-m-d H:i:s'),
                'agent_modified_by' => 1,
                'member_id' => $extractMember
            );
            $result = $this->Myaccount_model->updateAgent($data, $ageng_id);
            if ($result) {
                $this->session->set_flashdata('agentupdate', 'Agent updated successfully');
            } else {
                $this->session->set_flashdata('agentupdate', '');
            }
            redirect('myaccount/Myaccount');
        }
    }

    public function deleteImages(){
        echo $this->Myaccount_model->deleteImages();
    }

    //change password 
    public function changepassword(){
        $this->load->view('change_password');
    }

    public function checkOldPassword(){
        $eid = $this->input->post('oldpassword');
        $userid = $this->session->userdata['agentuser']['useragentid'];
       
        
        $query = $this->db->query("SELECT agentuser_password FROM pro_tbl_agentuser WHERE agentuser_id='$userid'");
        $row = $query->row();
        if($eid == $row->agentuser_password){
            echo 0;
        }else{
            echo 1;
        }
    }

    public function updatepassword(){
        $newpass = $this->input->post('new-password');
        $result = $this->Myaccount_model->updatepassword($newpass);

        if($result > 0){
            $this->session->set_flashdata('changepassword', 'Password changed successfully');
            redirect('myaccount/Myaccount/changepassword');
        }else if($result==0){
        $this->session->set_flashdata('changepassword', 'New Password same as old password');
            redirect('myaccount/Myaccount/changepassword');
        }
    }

}
