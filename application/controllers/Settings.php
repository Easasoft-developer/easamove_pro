<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Settings extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('Agent_model');
        $this->load->library('session');
        $this->load->library('form_validation');
        $this->load->library("pagination");
        $this->load->model('Myaccount_model');

        // Your own constructor code
    }

    public function index() {
//        print_r($this->session->all_userdata());exit();
        if (isset($this->session->userdata['agentuser']['logged_in']) && $this->session->userdata['agentuser']['logged_in'] == TRUE) {
            $data['users'] = $this->Agent_model->getUsercnt();
             $data['branch'] = $this->Agent_model->getbranchcnt();
              $this->load->view('common/header');
            $this->load->view('common/settingmenu', $data);
            $this->load->view('usermanagelist');
             $this->load->view('common/footer');
        } else {

            $this->load->view('login');
        }
    }

    public function useradd() {
//        print_r($this->session->all_userdata());exit();
        if (isset($this->session->userdata['agentuser']['logged_in']) && $this->session->userdata['agentuser']['logged_in'] == TRUE) {
            $data['users'] = $this->Agent_model->getUsercnt();
             $data['branch'] = $this->Agent_model->getbranchcnt();
             
            $this->load->view('common/header');
            $this->load->view('common/settingmenu', $data);
            $this->load->view('usermanage');
            $this->load->view('common/footer');
        } else {

            $this->load->view('login');
        }
    }
    public function userManage() {
//        print_r($this->session->all_userdata());exit();
        if (isset($this->session->userdata['agentuser']['logged_in']) && $this->session->userdata['agentuser']['logged_in'] == TRUE) {
            $config = array();
            $config['full_tag_open'] = '<ul class="pagination pagination-sm">';
            $config['full_tag_close'] = '</ul>';
            $config['prev_link'] = 'Prev';
            $config['prev_tag_open'] = '<li>';
            $config['prev_tag_close'] = '</li>';
            $config['next_link'] = 'Next';
            $config['next_tag_open'] = '<li>';
            $config['next_tag_close'] = '</li>';
            $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
            $config['cur_tag_close'] = '</a></li>';
            $config['num_tag_open'] = '<li>';
            $config['num_tag_close'] = '</li>';
             
            $config['first_tag_open'] = '<li>';
            $config['first_tag_close'] = '</li>';
            $config['last_tag_open'] = '<li>';
            $config['last_tag_close'] = '</li>';
             
            $config['first_link'] = '&lt;&lt;';
            $config['last_link'] = '&gt;&gt;';
            $config["base_url"] = base_url() . "settings/usermanage";
            $config["total_rows"] = $this->Agent_model->getUserCount();
            $config["per_page"] = 10;
            $config["uri_segment"] = 3;
            $choice = $config["total_rows"] / $config["per_page"];
            $config["num_links"] = round($choice);
            $this->pagination->initialize($config);
            $page = ($this->uri->segment(3))? $this->uri->segment(3) : 0;


            $data['users'] = $this->Agent_model->getUsercnt();
            $data['branch'] = $this->Agent_model->getbranchcnt();
            $data['userManagement'] = $this->Agent_model->getUserDetails($config["per_page"], $page);
            $data["links"] = $this->pagination->create_links();
            $data['countprop'] = $config["total_rows"];
            $data['perPage'] = $config["per_page"];
            $data['linkCount'] = $config["num_links"];
            if($this->input->post('search')){
                $search = array(
                    'searchtype' => $this->input->post('search')
                );
                $data['forata'] = $search;
            } 
             
            $this->load->view('common/header');
            $this->load->view('common/settingmenu', $data);
            $this->load->view('usermanagelist',$data);
            $this->load->view('common/footer');
        } else {

            $this->load->view('login');
        }
    }
public function branchManage() {
//        print_r($this->session->all_userdata());exit();
        if (isset($this->session->userdata['agentuser']['logged_in']) && $this->session->userdata['agentuser']['logged_in'] == TRUE) {
            $config = array();
            $config['full_tag_open'] = '<ul class="pagination pagination-sm">';
            $config['full_tag_close'] = '</ul>';
            $config['prev_link'] = 'Prev';
            $config['prev_tag_open'] = '<li>';
            $config['prev_tag_close'] = '</li>';
            $config['next_link'] = 'Next';
            $config['next_tag_open'] = '<li>';
            $config['next_tag_close'] = '</li>';
            $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
            $config['cur_tag_close'] = '</a></li>';
            $config['num_tag_open'] = '<li>';
            $config['num_tag_close'] = '</li>';
             
            $config['first_tag_open'] = '<li>';
            $config['first_tag_close'] = '</li>';
            $config['last_tag_open'] = '<li>';
            $config['last_tag_close'] = '</li>';
             
            $config['first_link'] = '&lt;&lt;';
            $config['last_link'] = '&gt;&gt;';
            $config["base_url"] = base_url() . "settings/branchManage";
            $config["total_rows"] = $this->Agent_model->agentBranchCount();
            $config["per_page"] = 10;
            $config["uri_segment"] = 3;
            $choice = $config["total_rows"] / $config["per_page"];
            $config["num_links"] = round($choice);
            $this->pagination->initialize($config);
            $page = ($this->uri->segment(3))? $this->uri->segment(3) : 0;
            $data['agentBranch'] = $this->Agent_model->agentBranch($config["per_page"], $page);
            $data["links"] = $this->pagination->create_links();
            $data['countprop'] = $config["total_rows"];
            $data['perPage'] = $config["per_page"];
            $data['linkCount'] = $config["num_links"];
            if($this->input->post('search')){
                $search = array(
                    'searchtype' => $this->input->post('search')
                );
                $data['forata'] = $search;
            } 

            $this->load->view('common/header');
            $this->load->view('common/settingmenu');
            $this->load->view('branchmanagelist',$data);
            $this->load->view('common/footer');
        } else {

            $this->load->view('login');
        }
    }
    public function addbranch() {
//        print_r($this->session->all_userdata());exit();
        if (isset($this->session->userdata['agentuser']['logged_in']) && $this->session->userdata['agentuser']['logged_in'] == TRUE) {
            $data['branch'] = $this->Agent_model->getbranchcnt();
            $data['users'] = $this->Agent_model->getUsercnt();
            $data['agenttype'] = $this->Agent_model->getAgenttype();
            $data['agentMemeber'] = $this->Myaccount_model->agentMemeber();
            $this->load->view('common/header');
            $this->load->view('common/settingmenu', $data);
            $this->load->view('branchmanage',$data);
            $this->load->view('common/footer');
        } else {

            $this->load->view('login');
        }
    }
    public function insert() {
        $id = $this->session->userdata['agentuser']['id'];
        if ($this->input->post('id') == 0) {

            $key = $this->generateRandomString(16);
            $pass = $this->generateRandomString(8);
            $data = array(
                'agentuser_name' => $this->input->post('username'),
                'agentuser_email' => $this->input->post('usermail'),
                'agentuser_password' => $pass,
                'agentuser_mobile' => $this->input->post('usermobile'),
                'agentuser_userrole' => $this->input->post('userrole'),
                'agentuser_access' => $this->input->post('permission'),
                'agent_id' => $id,
                'agentuser_created_on' => date('yy-mm-dd H:i:s'),
                'agentuser_created_by' => 1,
                'agentuser_verifykey' => $key
            );
            $result = $this->Agent_model->addAgentuser($data);

            $this->load->library('email');        
            $config['protocol'] = "smtp";
            $config['smtp_host'] = "ssl://mail.easamove.co.uk";
            $config['smtp_port'] = "465";
            $config['smtp_user'] = "info@demo.easamove.co.uk"; 
            $config['smtp_pass'] = "wow123";
            $config['charset'] = "utf-8";
            $config['mailtype'] = "html";
            $config['newline'] = "\r\n";
            $this->email->initialize($config);
            $mydata['name'] = $this->input->post('username');

            $mydata['link'] = base_url() . 'settings/verify/' . $key;

            $message = $this->load->view('mail_template/welcome_message', $mydata, true);
            $this->email->from('info@demo.easamove.co.uk', 'Easasoft');

            $this->email->to($this->input->post('usermail'));

            $this->email->subject('Welcome to Easamove');

            $this->email->message($message);

            $this->email->send();
            if($result){
                $this->session->set_flashdata('userAdd', 'User created successfully');
                redirect('settings/usermanage');
            }
        } else {
            $id = $this->input->post('id');
             $aid = $this->session->userdata['agentuser']['id'];
            $data = array(
                'agentuser_name' => $this->input->post('username'),
                'agentuser_mobile' => $this->input->post('usermobile'),
                'agentuser_userrole' => $this->input->post('userrole'),
                'agentuser_access' => $this->input->post('permission'),
                'agent_id' => $aid,
                'agentuser_modified_on' => date('yy-mm-dd H:i:s'),
                'agentuser_modified_by' => $id
            );
            $result = $this->Agent_model->updateAgentuser($data, $id);
            if($result){
                $this->session->set_flashdata('userAdd', 'User updated successfully');
                redirect('settings/usermanage');
            }
        }
        return $result;
    }
    public function verify($key) {

//        $key = $this->input->get('key');
        if ($key != '') {
            $res = $this->Agent_model->verify($key);
            if ($res != FALSE) {
                $this->load->library('email');        
                $config['protocol'] = "smtp";
                $config['smtp_host'] = "ssl://mail.easamove.co.uk";
                $config['smtp_port'] = "465";
                $config['smtp_user'] = "info@demo.easamove.co.uk"; 
                $config['smtp_pass'] = "wow123";
                $config['charset'] = "utf-8";
                $config['mailtype'] = "html";
                $config['newline'] = "\r\n";
                $this->email->initialize($config);
                $mydata['name'] = $res[0]->agentuser_name;
                $mydata['email'] = $res[0]->agentuser_email;
                $mydata['password'] = $res[0]->agentuser_password;

                $message = $this->load->view('mail_template/agent_verify', $mydata, true);
                $this->email->from('info@demo.easamove.co.uk', 'Members details');

                $this->email->to($res[0]->agentuser_email);

                $this->email->subject('Welcome to Easamove');

                $this->email->message($message);

                $this->email->send();
                $this->session->set_flashdata('logincheck', 'Agent verified successfully, your account detail has been sent to your registered email id');
            }else{
                $this->session->set_flashdata('logincheck', 'You already verified');
            }
            
        } else {
            $this->session->set_flashdata('logincheck', '');
        }
        $this->load->view('login');
    }
public function verifyagent($key) {

//        $key = $this->input->get('key');
        if ($key != '') {
            $res = $this->Agent_model->verifyagent($key);
            if ($res) {
                $this->load->library('email');
                $config['charset'] = 'iso-8859-1';
                $config['wordwrap'] = TRUE;
                $config['mailtype'] = 'html';
                $this->email->initialize($config);
                $mydata['name'] = $res[0]->agent_name;
                $mydata['email'] = $res[0]->email_id;
                $mydata['password'] = $res[0]->agent_password;

                $message = $this->load->view('mail_template/agent_verify', $mydata, true);
                $this->email->from('info@easasoft.com', 'Easasoft');

                $this->email->to($res[0]->email_id);

                $this->email->subject('Welcome to Easamove');

                $this->email->message($message);

                $this->email->send();
            }
            $this->session->set_flashdata('logincheck', 'Agent verified successfully, your account detail has been sent to your registered email id');
        } else {
            $this->session->set_flashdata('logincheck', '');
        }
        $this->load->view('login');
    }
    function generateRandomString($length) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function checkemail() {

        $eid = $this->input->post('usermail');
        $query = $this->db->query("SELECT * from pro_tbl_agentuser where agentuser_is_deleted=0 and agentuser_email='$eid'");
        // echo $this->db->last_query();
        // exit();
        if ($query->num_rows() > 0) {
            echo 1;
        } else {
            echo 0;
        }
    }

    public function getUser() {

        $result = $this->Agent_model->getAgentuser();
        $cnt = $this->Agent_model->getUsercnt();
        $offset=$this->input->post('offset');
        $rows=0;
        if ($cnt > 10) {
            $rows = $cnt / 10;
        }
        $diable='';
        if($rows==0){
            $diable='disabled';
        }
        $html = '';
        $html = '<tbody class="data-append">';
        if(!empty($result)){
        foreach ($result as $key => $res) {
            $html.='<tr id="' . $res->agentuser_id . '">
                            <td>' . ($key + 1) . '</td>
                            <td>' . $res->agentuser_name . '</td>
                            <td>' . $res->agentuser_email . '</td>
                            <td>' . $res->agentuser_userrole . '</td>';

            $acc = '';
            if ($res->agentuser_access == 2) {
                $acc = 'Write';
            } else if ($res->agentuser_access == 3) {
                $acc = 'Read';
            }
            $html.='<td>' . $acc . '</td>';
            $html.='<td><a class="btn btn-primary btn-xs" href="' . base_url('settings/userEdit') . '/' . $res->agentuser_id . '"><i class="fa fa-pencil"></i></button></td>';

            $html.='<td><button class="btn btn-primary btn-xs" onclick="deleteuser(' . $res->agentuser_id . ')"><i class="fa fa-times"></i></a></td>';

            $html.='</tr>';
        }
        }else{
             $html.='<tr><td colspan="7">No Record Found</td></tr>';
        }
        $html.='</tbody>';
         
        $html.='<tfoot class="foot">
                            <tr>
                              <!-- <td colspan="2">Showing </td> -->
                                <td colspan="3"> 
                                    <div class="btn-group">
                                        <button class="btn btn-default btn-sm rowscnt" type="button">'.$cnt.'</button>
                                        
                                    </div> Items
                                </td>';
         if($rows>0){
             
                            $html.='<td colspan="11">
                                    <ul class="pagination pagination-sm">
                                        <li class="'.$diable.'"><a href="javascript:;" class="prev"><i class="fa fa-arrow-left"></i> Prev</a></li>';
        for($i=1;$i<=$rows+1;$i++) {
            if($i==$offset){
                $cls='active';
            }else{
                 $cls='';
            }
            $html.='<li class="'.$cls.'"><a href="javascript:;" class="offset">'.$i.'</a></li>';
        }
        $html.='<li class="'.$diable.'"><a href="javascript:;" class="next"><i class="fa fa-arrow-right"></i> Next</a></li>
                                    </ul>
                                </td>';
         }
                            $html.='</tr>
                        </tfoot>';
          
        echo $html;
    }

    public function deleteUser() {

        $id = $this->input->post('id');
        $data = array(
            'agentuser_is_deleted' => 1,
            'agentuser_modified_on' => date('yy-mm-dd H:i:s'),
            'agentuser_modified_by' => 1
        );
        $result = $this->Agent_model->deleteUser($data, $id);
        echo $result;
    }

    public function userEdit() {
//        print_r($this->session->all_userdata());exit();
        if (isset($this->session->userdata['agentuser']['logged_in']) && $this->session->userdata['agentuser']['logged_in'] == TRUE) {
            $id = $this->uri->segment(3);
            $data1['branch'] = $this->Agent_model->getbranchcnt();
            $data1['users'] = $this->Agent_model->getUsercnt();
            $data['user'] = $this->Agent_model->getUserdata($id);
            $this->load->view('common/header');
            $this->load->view('common/settingmenu',$data1);
            $this->load->view('usermanage', $data);
            $this->load->view('common/footer');
        } else {

            $this->load->view('login');
        }
    }

    
    public function agentinsert() {
         $aid = $this->session->userdata['agentuser']['id'];
         
        $config = array(
            'upload_path' => 'D:\hshome\easam\admin.easamove.co.uk/images/',
            'allowed_types' => 'jpg|gif|png|jpeg',
            'overwrite' => 1,
//            'file_name' =>$newFileName,
        );
        if($_FILES['agentImage']['name']!=''){
        $this->load->library('upload', $config);
//           print_r($_FILES);
        $this->upload->initialize($config);
        if (!$this->upload->do_upload('agentImage')) {
            $error = array('error' => $this->upload->display_errors());
        } else {
            $data1 = $this->upload->data();

            $file_path = $data1['file_path'];
            $file = $data1['full_path'];
            $file_ext = $data1['file_ext'];

            $final_file_name = $this->input->post('cName') . '_' . time() . '' . $file_ext;
            $rename = rename($file, $file_path . $final_file_name);
        }
        }else{
            $final_file_name=$this->input->post('imageedit');
        }

// here is the renaming functon
        $address = $this->input->post('AddressStreet').','.$this->input->post('AddressCityName').','.$this->input->post('AddressCounty').','.$this->input->post('AddressZip'); // Google HQ
        $prepAddr = str_replace(' ','+',$address);
    
        $geocode=file_get_contents('http://maps.google.com/maps/api/geocode/json?address='.$prepAddr.'&sensor=false');
         
        $output= json_decode($geocode);
         
        $lat = $output->results[0]->geometry->location->lat;
        $long = $output->results[0]->geometry->location->lng;
        $agentmember = $this->input->post('agent-member');
        $extractMember = implode(",", $agentmember);
        if ($this->input->post('id') == 0) {
            $key = $this->generateRandomString(16);
            
            $data = array(
                'agent_refno' => $this->input->post('agentrefno'),
                'agent_name' => $this->input->post('cName'),
                'vat_reg_no' => $this->input->post('vatregno'),
                'vat_reg_country' => $this->input->post('vatregcty'),
                'agent_key' => $this->generateRandomString(8),
                'default_currency' => $this->input->post('defcur'),
                'invoice_due_day' => $this->input->post('invoicedue'),
                'work_start_time' => $this->input->post('workstart'),
                'work_end_time' => $this->input->post('workend'),
                'agent_logo' => $final_file_name,
                'number' => $this->input->post('Address_number'),
                'street' => $this->input->post('AddressStreet'),
                'locality' => $this->input->post('AddressStreet'),
                'county' => $this->input->post('AddressCounty'),
                'town' => $this->input->post('AddressCityName'),
                'post_code' => $this->input->post('AddressZip'),
                'title' => $this->input->post('title'),
                'forename' => $this->input->post('Forename'),
                'surname'=> $this->input->post('Surname'),
                'email_id'=> $this->input->post('Email'),
                'phone_no'=> $this->input->post('Phone'),
                'fax'=> $this->input->post('Fax'),
                'website'=> $this->input->post('Website'),
                'agent_type'=> $this->input->post('AgentTypeId'),
                'agent_head_office'=>$aid,
                'agent_aboutus'=> $this->input->post('About'),
                'agent_created_on' => date('yy-mm-dd H:i:s'),
                'agent_created_by' => 1,
                'agent_key' => $key,
                'agent_lat' => $lat,
                'agent_long' => $long,
                'member_id' => $extractMember
            );
            $result = $this->Agent_model->addAgent($data);

            //agent mail

            $this->load->library('email');

            $config['charset'] = 'iso-8859-1';
            $config['wordwrap'] = TRUE;
            $config['mailtype'] = 'html';
            $this->email->initialize($config);
            $mydata['name'] = $this->input->post('Forename');

            $mydata['link'] = base_url() . 'settings/verifyagent/' . $key;

            $message = $this->load->view('mail_template/agent_welcome', $mydata, true);
            $this->email->from('info@easasoft.com', 'Easasoft');

            $this->email->to($this->input->post('Email'));

            $this->email->subject('Welcome to Easamove');

            $this->email->message($message);

            $this->email->send();
            $this->session->set_flashdata('agentAdded', 'Branch successfully added');
            redirect('settings/branchManage');
        } else {
            $id = $this->input->post('id');
            $data = array(
                'agent_refno' => $this->input->post('agentrefno'),
                'agent_name' => $this->input->post('cName'),
                'vat_reg_no' => $this->input->post('vatregno'),
                'vat_reg_country' => $this->input->post('vatregcty'),
                'agent_key' => $this->generateRandomString(8),
                'default_currency' => $this->input->post('defcur'),
                'invoice_due_day' => $this->input->post('invoicedue'),
                'work_start_time' => $this->input->post('workstart'),
                'work_end_time' => $this->input->post('workend'),
                'agent_logo' => $final_file_name,
                'number' => $this->input->post('Address_number'),
                'street' => $this->input->post('AddressStreet'),
                'locality' => $this->input->post('AddressStreet'),
                'county' => $this->input->post('AddressCounty'),
                'town' => $this->input->post('AddressCityName'),
                'post_code' => $this->input->post('AddressZip'),
                'title' => $this->input->post('title'),
                'forename' => $this->input->post('Forename'),
                'surname'=> $this->input->post('Surname'),
                'phone_no'=> $this->input->post('Phone'),
                'fax'=> $this->input->post('Fax'),
                'website'=> $this->input->post('Website'),
                'agent_type'=> $this->input->post('AgentTypeId'),
                'agent_head_office'=>$aid,
                'agent_aboutus'=> $this->input->post('About'),
                'agent_created_on' => date('yy-mm-dd H:i:s'),
                'agent_created_by' => 1,
                'agent_key' => $key,
                'agent_lat' => $lat,
                'agent_long' => $long,
                'member_id' => $extractMember
            );
            $result = $this->Agent_model->updateAgent($data, $id);
            $this->session->set_flashdata('agentAdded', 'Branch successfully updated');
            redirect('settings/branchManage');
        }
        
    }
    public function checkref(){
          $ref=$this->input->post('ref');
         $query=$this->db->query("select * from  easamoveadmin_tbl_agent where  REPLACE(lower(agent_refno),' ','')=REPLACE(lower('$ref'),' ','') and agent_is_deleted=0");
        if ($query->num_rows() > 0) {
            echo 1;
        }else{
            echo 0;
        }
    }
    
    
    public function getBranch() {

        $result = $this->Agent_model->getAgentbranch();
        $cnt = $this->Agent_model->getbranchcnt();
        $offset=$this->input->post('offset');
        $rows=0;
        if ($cnt > 10) {
            $rows = $cnt / 10;
        }
        $disble='';
        if($rows==0){
            $disble='disabled';
        }
        $html = '';
        $html = '<tbody class="data-append">';
        if(!empty($result)){
        foreach ($result as $key => $res) {
            $html.='<tr id="' . $res->agent_id . '">
                            <td>' . ($key + 1) . '</td>
                            <td>' . $res->agent_name . '</td>
                            <td>' . $res->email_id . '</td>
                            <td>' . $res->phone_no . '</td>
                            <td>' . $res->post_code . '</td>';

            $html.='<td><a class="btn btn-primary btn-xs" href="' . base_url('settings/branchEdit') . '/' . $res->agent_id . '"><i class="fa fa-pencil"></i></button></td>';

            $html.='<td><button class="btn btn-primary btn-xs" onclick="deleteBranch(' . $res->agent_id . ')"><i class="fa fa-times"></i></a></td>';

            $html.='</tr>';
        }
        }else{
             $html.='<tr><td colspan="7">No Record Found</td></tr>';
        }
        $html.='</tbody>';
         
        $html.='<tfoot class="foot">
                            <tr>
                              <!-- <td colspan="2">Showing </td> -->
                                <td colspan="3"> 
                                    <div class="btn-group">
                                        <button class="btn btn-default btn-sm rowscnt" type="button">'.$cnt.'</button>
                                        
                                    </div> Items
                                </td>';
        
                                 if($rows>0){
                                     $html.='<td colspan="11">
                                    <ul class="pagination pagination-sm">
                                        <li class="'.$disble.'"><a href="javascript:;"><i class="fa fa-arrow-left"></i> Prev</a></li>';
        for($i=1;$i<=$rows+1;$i++) {
            if($i==$offset){
                $cls='active';
            }else{
                 $cls='';
            }
            
            $html.='<li class="'.$cls.'"><a href="javascript:;" class="offset">'.$i.'</a></li>';
        }
        $html.='<li class="'.$disble.'"><a href="javascript:;" class="next"><i class="fa fa-arrow-right"></i> Next</a></li>
                                    </ul>
                                </td>';
                                }
                            $html.='</tr>
                        </tfoot>';
          
        echo $html;
    }

    public function deleteBranch() {
        $aid = $this->session->userdata['agentuser']['id'];
        $id = $this->input->post('id');
        $data = array(
            'agent_is_deleted' => 1,
            'agent_modified_on' => date('yy-mm-dd H:i:s'),
            'agent_modified_by' => $aid
        );
        $result = $this->Agent_model->deleteAgent($data, $id);
        echo $result;
    }
    public function branchEdit(){
         $site = $this->uri->segment(3);
        if ($site != '') {
            $id = $site;
        } else {
            $id = 0;
        }
       $data['branch'] = $this->Agent_model->getbranchcnt();
            $data['users'] = $this->Agent_model->getUsercnt();
        $data['agenttype'] = $this->Agent_model->getAgenttype();
        $data['agentMemeber'] = $this->Myaccount_model->agentMemeber();
        $data['agent'] = $this->Agent_model->getAgentdata($id);

        $this->load->view('common/header');
        $this->load->view('common/settingmenu', $data);
        $this->load->view('branchmanage', $data);
        $this->load->view('common/footer');
    }
     public function export() {
         $aid = $this->session->userdata['agentuser']['id'];
         $type = $this->uri->segment(3);
        $this->load->library('PHPExcel');
//        $this->load->library('PHPExcel/IOFactory');

        $objPHPExcel = new PHPExcel();
//        $reader = PHPExcel_IOFactory::createReader('Excel5');
        $objPHPExcel->getProperties()->setTitle("export")->setDescription("none");

        $objPHPExcel->setActiveSheetIndex(0);
        $query = $this->db->query("SELECT * FROM pro_tbl_agentuser where agentuser_is_deleted=0 and agent_id=$aid and agentuser_access!=1");
        // Field names in the first row
        $fields = $query->list_fields();
        $col = 0;
        foreach ($fields as $field) {
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, 1, $field);
            $col++;
        }

        // Fetching the table data
        $row = 2;
        foreach ($query->result() as $data) {
            $col = 0;
            foreach ($fields as $field) {
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $data->$field);
                $col++;
            }

            $row++;
        }

        $objPHPExcel->setActiveSheetIndex(0);

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

        // Sending headers to force the user to download the file
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="users_' . date('dMy') . '.csv"');
        header('Cache-Control: max-age=0');

//     $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "PDF");
        $objWriter->save('php://output');
    }
    
    
    
    public function create() {
        $aid = $this->session->userdata['agentuser']['id'];
        $this->load->library('Pdf');
//        
        $pdf = new Pdf(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $pdf->SetAuthor('Nicola Asuni');
        $pdf->SetTitle('');
        $pdf->SetSubject('TCPDF Tutorial');
        $pdf->SetKeywords('Customer');

        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', '8'));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
        if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
            require_once(dirname(__FILE__) . '/lang/eng.php');
            $pdf->setLanguageArray($l);
        }

// ---------------------------------------------------------
// set font
        $pdf->SetFont('helvetica', '', 10);

// add a page
        $pdf->AddPage();

        $query = $this->db->query("SELECT * FROM pro_tbl_agentuser WHERE agentuser_is_deleted = 0 and agent_id=$aid and agentuser_access!=1");
        $html = '';
        
        $html.='<html xmlns="http://www.w3.org/1999/xhtml" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head><body style="font-size:8px;font-family:Arial, Helvetica, sans-serif">';
$html .= '<table width="100%" cellpadding="4" cellspacing="1">
                <tr>
                  <td style="vertical-align: top;"></td>
                  <td style="text-align:left;font-style:12px;">USER DATA</td>
                      <td align="right">
                         <!--<img src="" alt="logo" width="70" height="100"/>-->
                      </td>
                </tr>
              </table>';

$html .= '<table  bgcolor="#E1E1E1"  width="100%" cellpadding="4" cellspacing="1" >
                    <thead>
                        <tr bgcolor="#E1E1E1" style="color: #fff;">
                            <th width="25%" style="color: #000000; font-weight: bold; ">SERIAL NO</th>
                            <th width="25%" style="color: #000000; font-weight: bold; ">USER NAME</th>
                            <th width="25%" style="color: #000000; font-weight: bold; ">USER EMAIL</th>
                            <th width="25%" style="color: #000000; font-weight: bold; ">USER MOBILE</th>
                            
                        </tr>
                    </thead></table><table  bgcolor="#F4F2F2"  width="100%" cellpadding="4" cellspacing="1">
                    <tbody class="tb">';
        foreach ($query->result() as $key=>$row) {
            
            
            $html.='<tr >'
                    . '<td width="25%" align="left" bgcolor="#FFFFFF">' . $key . '</td>'
                    . '<td width="25%" align="left" bgcolor="#FFFFFF">' . $row->agentuser_name . '</td>'
                    . '<td  width="25%" align="left" bgcolor="#FFFFFF">'. $row->agentuser_email . '</td>'
                    . '<td  width="25%" align="center" bgcolor="#FFFFFF">' . $row->agentuser_mobile . '</td>'
                    . '</tr>';
        }
        $html.='</tbody></table></body></html>';
        $pdf->writeHTML($html, true, false, true, false, '');

        $filename = 'userdata'.'_'.$aid.'_'.date('Y-m-d');
//            $filelocation = base_url()."Images/pdfimg"; //Linux
//             $fileNL = $filelocation."/".$filename; //Linux
        ob_clean();
        // $root = realpath($_SERVER["DOCUMENT_ROOT"]);
        $filetosave = '/images/' . $filename . '.pdf';
        $output = $pdf->Output($filetosave, 'FI');
    }
    
    
    
    
    public function downpdf(){
        $this->create();
    }
    
    //for branch
    public function exportbranch() {
         $aid = $this->session->userdata['agentuser']['id'];
         $type = $this->uri->segment(3);
        $this->load->library('PHPExcel');
//        $this->load->library('PHPExcel/IOFactory');

        $objPHPExcel = new PHPExcel();
//        $reader = PHPExcel_IOFactory::createReader('Excel5');
        $objPHPExcel->getProperties()->setTitle("export")->setDescription("none");

        $objPHPExcel->setActiveSheetIndex(0);
        $query = $this->db->query("SELECT * FROM easamoveadmin_tbl_agent where agent_is_deleted=0 and agent_head_office='$aid'");
        // Field names in the first row
        $fields = $query->list_fields();
        $col = 0;
        foreach ($fields as $field) {
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, 1, $field);
            $col++;
        }

        // Fetching the table data
        $row = 2;
        foreach ($query->result() as $data) {
            $col = 0;
            foreach ($fields as $field) {
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $data->$field);
                $col++;
            }

            $row++;
        }

        $objPHPExcel->setActiveSheetIndex(0);

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

        // Sending headers to force the user to download the file
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Products_' . date('dMy') . '.csv"');
        header('Cache-Control: max-age=0');

//     $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "PDF");
        $objWriter->save('php://output');
    }
    public function createbranch() {
         $aid = $this->session->userdata['agentuser']['id'];
        $this->load->library('Pdf');
//        
        $pdf = new Pdf(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $pdf->SetAuthor('Nicola Asuni');
        $pdf->SetTitle('');
        $pdf->SetSubject('TCPDF Tutorial');
        $pdf->SetKeywords('Customer');

        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', '8'));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
        if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
            require_once(dirname(__FILE__) . '/lang/eng.php');
            $pdf->setLanguageArray($l);
        }

// ---------------------------------------------------------
// set font
        $pdf->SetFont('helvetica', '', 10);

// add a page
        $pdf->AddPage();

        $query = $this->db->query("SELECT * FROM easamoveadmin_tbl_agent where agent_is_deleted=0 and agent_head_office='$aid'");
        $html = '';
        
        $html.='<html xmlns="http://www.w3.org/1999/xhtml" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head><body style="font-size:8px;font-family:Arial, Helvetica, sans-serif">';
$html .= '<table width="100%" cellpadding="4" cellspacing="1">
                <tr>
                  <td style="vertical-align: top;"></td>
                  <td style="text-align:left;font-style:12px;">USER DATA</td>
                      <td align="right">
                         <!--<img src="" alt="logo" width="70" height="100"/>-->
                      </td>
                </tr>
              </table>';

$html .= '<table  bgcolor="#E1E1E1"  width="100%" cellpadding="4" cellspacing="1" >
                    <thead>
                        <tr bgcolor="#E1E1E1" style="color: #fff;">
                            <th width="25%" style="color: #000000; font-weight: bold; ">SERIAL NO</th>
                            <th width="25%" style="color: #000000; font-weight: bold; ">AGENT NAME</th>
                            <th width="25%" style="color: #000000; font-weight: bold; ">AGENT EMAIL</th>
                            <th width="25%" style="color: #000000; font-weight: bold; ">AGENT MOBILE</th>
                            
                        </tr>
                    </thead></table><table  bgcolor="#F4F2F2"  width="100%" cellpadding="4" cellspacing="1">
                    <tbody class="tb">';
        foreach ($query->result() as $key=>$row) {
            
            
            $html.='<tr >'
                    . '<td width="25%" align="left" bgcolor="#FFFFFF">' . $key . '</td>'
                    . '<td width="25%" align="left" bgcolor="#FFFFFF">' . $row->agent_name . '</td>'
                    . '<td  width="25%" align="left" bgcolor="#FFFFFF">'. $row->email_id . '</td>'
                    . '<td  width="25%" align="center" bgcolor="#FFFFFF">' . $row->phone_no . '</td>'
                    . '</tr>';
        }
        $html.='</tbody></table></body></html>';
        $pdf->writeHTML($html, true, false, true, false, '');

        $filename = 'branchdata'.'_'.$aid.'_'.date('Y-m-d');
//            $filelocation = base_url()."Images/pdfimg"; //Linux
//             $fileNL = $filelocation."/".$filename; //Linux
        ob_clean();
        // $root = realpath($_SERVER["DOCUMENT_ROOT"]);
        $filetosave = '/images/' . $filename . '.pdf';
        $output = $pdf->Output($filetosave, 'FI');
    }
    
    
    
    
    public function downbranchpdf(){
        $this->createbranch();
    }
}
