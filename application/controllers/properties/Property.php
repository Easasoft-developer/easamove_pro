<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Property extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('Agent_model');
        $this->load->model('Prop_model');
        $this->load->library('session');
        $this->load->library('form_validation');
        $this->load->library("pagination");

        // Your own constructor code
    }

    public function index() {
//        print_r($this->session->all_userdata());exit();
        if (isset($this->session->userdata['agentuser']['logged_in']) && $this->session->userdata['agentuser']['logged_in'] == TRUE) {
            $config = array();
            $config['full_tag_open'] = '<ul class="pagination pagination-sm">';
            $config['full_tag_close'] = '</ul>';
            $config['prev_link'] = 'Prev';
            $config['prev_tag_open'] = '<li>';
            $config['prev_tag_close'] = '</li>';
            $config['next_link'] = 'Next';
            $config['next_tag_open'] = '<li>';
            $config['next_tag_close'] = '</li>';
            $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
            $config['cur_tag_close'] = '</a></li>';
            $config['num_tag_open'] = '<li>';
            $config['num_tag_close'] = '</li>';
             
            $config['first_tag_open'] = '<li>';
            $config['first_tag_close'] = '</li>';
            $config['last_tag_open'] = '<li>';
            $config['last_tag_close'] = '</li>';
             
            $config['first_link'] = '&lt;&lt;';
            $config['last_link'] = '&gt;&gt;';
            $config["base_url"] = base_url() . "properties/Property/index";
            $config["total_rows"] = $this->Agent_model->record_sale_count();
            $config["per_page"] = 10;
            $config["uri_segment"] = 4;
            $choice = $config["total_rows"] / $config["per_page"];
            $config["num_links"] = round($choice);
            $this->pagination->initialize($config);
            $page = ($this->uri->segment(4))? $this->uri->segment(4) : 0;
            
            


            $this->load->view('common/header');
            $data1['salecount'] = $this->Agent_model->getSalepropcount();
            $data1['rentcount'] = $this->Agent_model->getRentpropcount();
            $data['saleproperty'] = $this->Agent_model->saleproperty($config["per_page"], $page);
            $data["links"] = $this->pagination->create_links();
            $data['countprop'] = $config["total_rows"];
            $data['perPage'] = $config["per_page"];
            $data['linkCount'] = $config["num_links"];
            if($this->input->post('status') || $this->input->post('search')){
                $search = array(
                    'typestatus' => $this->input->post('status'),
                    'searchtype' => $this->input->post('search')
                );
                $data['forata'] = $search;
            } 
            $this->load->view('common/propertymenu', $data1);
            $this->load->view('property/property',$data);
            $this->load->view('common/footer');
        } else {

            $this->load->view('login');
        }
    }

    public function sale() {
//        print_r($this->session->all_userdata());exit();
        if (isset($this->session->userdata['agentuser']['logged_in']) && $this->session->userdata['agentuser']['logged_in'] == TRUE) {
            $this->load->view('common/header');
            $data1['salecount'] = $this->Agent_model->getSalepropcount();
            $data1['rentcount'] = $this->Agent_model->getRentpropcount();
            $this->load->view('common/propertymenu', $data1);
            $data['sitestatus'] = $this->Agent_model->getSitestatus();
            $data['liststatus'] = $this->Agent_model->getListstatus();
            $data['proptype'] = $this->Agent_model->getProptype();
            $data['tenure'] = $this->Agent_model->getTenures();
            $data['transtype'] = $this->Agent_model->getTranstype();
            $data['buildcont'] = $this->Agent_model->getBuildcont();
            $data['lettype'] = $this->Agent_model->getLettype();
            $data['considerinfo'] = $this->Agent_model->getConsiderinfo();
            $data['outspace'] = $this->Agent_model->getOutspace();
            $data['parking'] = $this->Agent_model->getParking();
            $data['heattype'] = $this->Agent_model->getHeatingtype();
            $data['splfeature'] = $this->Agent_model->getSplfeature();
            $data['furnish'] = $this->Agent_model->getFurnishes();
            $data['access'] = $this->Agent_model->getAccessibility();
            $data['pricemod'] = $this->Agent_model->getPricemod(1);
            $data['content'] = $this->Agent_model->getContent();
            $this->load->view('property/sale', $data);
            $this->load->view('common/footer');
        } else {

            $this->load->view('login');
        }
    }

    public function rentlist() {
//        print_r($this->session->all_userdata());exit();
        if (isset($this->session->userdata['agentuser']['logged_in']) && $this->session->userdata['agentuser']['logged_in'] == TRUE) {



            $config = array();
            $config['full_tag_open'] = '<ul class="pagination pagination-sm">';
            $config['full_tag_close'] = '</ul>';
            $config['prev_link'] = 'Prev';
            $config['prev_tag_open'] = '<li>';
            $config['prev_tag_close'] = '</li>';
            $config['next_link'] = 'Next';
            $config['next_tag_open'] = '<li>';
            $config['next_tag_close'] = '</li>';
            $config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
            $config['cur_tag_close'] = '</a></li>';
            $config['num_tag_open'] = '<li>';
            $config['num_tag_close'] = '</li>';
             
            $config['first_tag_open'] = '<li>';
            $config['first_tag_close'] = '</li>';
            $config['last_tag_open'] = '<li>';
            $config['last_tag_close'] = '</li>';
             
            $config['first_link'] = '&lt;&lt;';
            $config['last_link'] = '&gt;&gt;';
            $config["base_url"] = base_url() . "properties/Property/rentlist";
            $config["total_rows"] = $this->Agent_model->record_rent_count();
            $config["per_page"] = 10;
            $config["uri_segment"] = 4;
            $choice = $config["total_rows"] / $config["per_page"];
            $config["num_links"] = round($choice);
            $this->pagination->initialize($config);
            $page = ($this->uri->segment(4))? $this->uri->segment(4) : 0;

            $this->load->view('common/header');
            $data1['salecount'] = $this->Agent_model->getSalepropcount();
            $data1['rentcount'] = $this->Agent_model->getRentpropcount();
            $data['rentproperty'] = $this->Agent_model->rentproperty($config["per_page"], $page);
            $data["links"] = $this->pagination->create_links();
            $data['countprop'] = $config["total_rows"];
            $data['perPage'] = $config["per_page"];
            $data['linkCount'] = $config["num_links"];
            if($this->input->post('status') || $this->input->post('search')){
                $search = array(
                    'typestatus' => $this->input->post('status'),
                    'searchtype' => $this->input->post('search')
                );
                $data['forata'] = $search;
            }   
            $this->load->view('common/propertymenu', $data1);
            $this->load->view('property/propertylist_rent', $data);
            $this->load->view('common/footer');
        } else {

            $this->load->view('login');
        }
    }

    public function rent() {
//        print_r($this->session->all_userdata());exit();
        if (isset($this->session->userdata['agentuser']['logged_in']) && $this->session->userdata['agentuser']['logged_in'] == TRUE) {
            $this->load->view('common/header');
            $data1['salecount'] = $this->Agent_model->getSalepropcount();
            $data1['rentcount'] = $this->Agent_model->getRentpropcount();
            $this->load->view('common/propertymenu', $data1);
            $data['sitestatus'] = $this->Agent_model->getSitestatus();
            $data['liststatus'] = $this->Agent_model->getListstatusr();
            $data['proptype'] = $this->Agent_model->getProptype();
            $data['tenure'] = $this->Agent_model->getTenures();
            $data['transtype'] = $this->Agent_model->getTranstype();
            $data['buildcont'] = $this->Agent_model->getBuildcont();
            $data['lettype'] = $this->Agent_model->getLettype();
            $data['feesapply'] = $this->Agent_model->getFeesapply();
            $data['considerinfo'] = $this->Agent_model->getConsiderinfo();
            $data['outspace'] = $this->Agent_model->getOutspace();
            $data['parking'] = $this->Agent_model->getParking();
            $data['heattype'] = $this->Agent_model->getHeatingtype();
            $data['splfeature'] = $this->Agent_model->getSplfeature();
            $data['furnish'] = $this->Agent_model->getFurnishes();
            $data['access'] = $this->Agent_model->getAccessibility();
            $data['pricemod'] = $this->Agent_model->getPricemod(2);
            $data['content'] = $this->Agent_model->getContent();
            $this->load->view('property/rent', $data);
            $this->load->view('common/footer');
        } else {

            $this->load->view('login');
        }
    }

    public function addProp() {
        $id = $this->session->userdata['agentuser']['id'];
        $parking=$this->input->post('parking');
        $outspace=$this->input->post('out-space');
        $specialfeat=$this->input->post('special-feat');
        if(!empty($outspace)){
            $outspace=implode(',',$this->input->post('out-space'));
        }else{
            $outspace='';
        }
        if(!empty($parking)){
            $parking=implode(',',$this->input->post('parking'));
        }else{
            $parking='';
        }
        if(!empty($specialfeat)){
            $specialfeat=implode(',',$this->input->post('special-feat'));
        }else{
            $specialfeat='';
        }
        if ($this->input->post('id') == 0) {
            $data = array(
                'property_type' => 1,
                'agent_id' => $id,
                'property_pstatus' => $this->input->post('p-status'),
                'property_lstatus' => $this->input->post('l-status'),
                'property_ptype' => $this->input->post('p-type'),
                'property_tenure' => $this->input->post('tenure'),
                'property_ptransact' => $this->input->post('p-transact'),
                'property_buildage' => $this->input->post('build-age'),
                'property_buildconst' => $this->input->post('build-const'),
//              'property_let-type' => $this->input->post('let-type'),                
                'property_name' => $this->input->post('p-name'),
                'property_sname' => $this->input->post('s-name'),
                'property_postalc' => $this->input->post('postal-c'),
                'property_city' => $this->input->post('city'),
                'property_county' => $this->input->post('county'),
                'property_ownrefer' => $this->input->post('own-refer'),
                'property_lat' => $this->input->post('Latitude'),
                'property_long' => $this->input->post('Longitude'),
                'property_outspace' =>  $outspace,
                'property_parking' => $parking,
                'property_heat' => $this->input->post('heat'),
                'property_specialfeat' => $specialfeat,
                'property_furnish' => $this->input->post('furnish'),
                'property_access' => $this->input->post('access'),
                'property_pricemod' => $this->input->post('price-mod'),
                'property_price' => $this->input->post('price'),
                'property_bedroom' => $this->input->post('bedroom'),
                'property_bathroom' => $this->input->post('bathroom'),
                'property_reception' => $this->input->post('reception'),
                'property_floors' => $this->input->post('floor'),
                'property_summary' => $this->input->post('summary'),
                'property_fulldesc' => $this->input->post('full-describe'),
                'property_created_on' => date('Y-m-d H:i:s'),
                'property_created_by' => $id,
            );
            
            $result = $this->Prop_model->addProp($data, $id = 0);
            if($result == 1){
                $this->session->set_flashdata('propertyAdd', 'Property successfully added');
                redirect('properties/Property');
            }
        } else {

            $data = array(
                'property_type' => 1,
                'agent_id' => $id,
                'property_pstatus' => $this->input->post('p-status'),
                'property_lstatus' => $this->input->post('l-status'),
                'property_ptype' => $this->input->post('p-type'),
                'property_tenure' => $this->input->post('tenure'),
                'property_ptransact' => $this->input->post('p-transact'),
                'property_buildage' => $this->input->post('build-age'),
                'property_buildconst' => $this->input->post('build-const'),
//            'property_let-type' => $this->input->post('let-type'),
                'property_name' => $this->input->post('p-name'),
                'property_sname' => $this->input->post('s-name'),
                'property_postalc' => $this->input->post('postal-c'),
                'property_city' => $this->input->post('city'),
                'property_county' => $this->input->post('county'),
                'property_ownrefer' => $this->input->post('own-refer'),
                'property_lat' => $this->input->post('Latitude'),
                'property_long' => $this->input->post('Longitude'),
                'property_outspace' =>  $outspace,
                'property_parking' => $parking,
                'property_heat' => $this->input->post('heat'),
                'property_specialfeat' => $specialfeat,
                'property_furnish' => $this->input->post('furnish'),
                'property_access' => $this->input->post('access'),
                'property_pricemod' => $this->input->post('price-mod'),
                'property_price' => $this->input->post('price'),
                'property_bedroom' => $this->input->post('bedroom'),
                'property_bathroom' => $this->input->post('bathroom'),
                'property_reception' => $this->input->post('reception'),
                'property_floors' => $this->input->post('floor'),
                'property_summary' => $this->input->post('summary'),
                'property_fulldesc' => $this->input->post('full-describe'),
                'property_modified_on' => date('Y-m-d H:i:s'),
                'property_modified_by' => $id,
            );
//            print_r($data);exit();
            $id = $this->input->post('id');
            $result = $this->Prop_model->addProp($data, $id);
            if($result == 1){
                $this->session->set_flashdata('propertyAdd', 'Property successfully updated');
                redirect('properties/Property');
            }
        }
    }

    public function getprop() {
        $result = $this->Prop_model->getProperty();
        $cnt = $this->Agent_model->getSalepropcount();
        $offset = $this->input->post('offset');
        $perpage = $this->input->post('row');
        $rows = 0;
        if ($cnt > 10) {
            $rows = $cnt / 10;
        }
        $diable = '';
        if ($rows == 0) {
            $diable = 'disabled';
        }
        $html = '';
        $html = '<tbody class="data-append">';
        if (!empty($result)) {
            foreach ($result as $key => $res) {
                $html.='<tr id="' . $res->property_id . '">
                   <td>' . ($key + 1) . '</td>
                   <td>' . $res->property_ownrefer . '</td>
                   <td>' . $res->property_price . '</td>
                   <td>' . $res->property_sname . '</td>
                   <td>' . $res->propertytype_name . '</td>
                   <td>' . date('d-m-Y', strtotime($res->property_created_on)) . '</td>
                   <td>' . $res->listingstatus_name . '</td>
                   <td>' . date('d-m-Y', strtotime($res->property_modified_on)) . '</td>
                   <td>' . $res->imgcnt . ' / ' . $res->floorcnt .  ' / ' . $res->epc .'</td>
                   <td>' . $res->sitestatus_name . '</td>';

                $html.='<td><a class="btn btn-primary btn-xs" href="' . base_url('properties/property/propEdit') . '/' . $res->property_id . '"><i class="fa fa-pencil"></i></button></td>';
                $html.='</tr>';
            }
        } else {
            $html.='<tr><td colspan="7">No Record Found</td></tr>';
        }
        $html.='</tbody>';
       
            $html.='<tfoot class="foot">
                        <tr>
                        <!-- <td colspan="2">Showing </td> -->
                        <td colspan="3"> 
                        <div class="btn-group">
                        '.$cnt.'
                        </div> Items
                        </td>';
             if ($rows > 0) {
                       $html.='<td colspan="11">
                        <ul class="pagination pagination-sm">';
            if ($diable != 'disabled') {
                $html.='<li class="' . $diable . '"><a href="javascript:;"  class="prev"><i class="fa fa-arrow-left"></i> Prev</a></li>';
            } else {
                $html.='<li class="' . $diable . '"><a href="javascript:;"><i class="fa fa-arrow-left"></i> Prev</a></li>';
            }
            for ($i = 1; $i <= $rows + 1; $i++) {
                if ($i == $offset) {
                    $cls = 'active';
                } else {
                    $cls = '';
                }
                $html.='<li class="' . $cls . '"><a href="javascript:;" class="offset">' . $i . '</a></li>';
            }
            if ($diable != 'disabled') {
                $html.='<li class="' . $diable . '"><a href="javascript:;"  class="next"><i class="fa fa-arrow-right"></i> Next</a></li>';
            } else {
                $html.='<li class="' . $diable . '"><a href="javascript:;" ><i class="fa fa-arrow-right"></i> Next</a></li>';
            }
            $html.='</ul>
                        </td>';
             }
             
                        $html.='</tr>
                        </tfoot>';
        
        echo $html;
    }

    public function propEdit() {
//        print_r($this->session->all_userdata());exit();
        if (isset($this->session->userdata['agentuser']['logged_in']) && $this->session->userdata['agentuser']['logged_in'] == TRUE) {
            $id = $this->uri->segment(4);
            $data['property'] = $this->Prop_model->getPropertybyid($id);
            $data['keyfeat'] = $this->Prop_model->getKeyfeature($id);
            $data['images'] = $this->Prop_model->getImages($id);
            $data['contents'] = $this->Prop_model->getContents($id);
            $this->load->view('common/header');
            $data1['salecount'] = $this->Agent_model->getSalepropcount();
            $data1['rentcount'] = $this->Agent_model->getRentpropcount();
            $this->load->view('common/propertymenu', $data1);
            $data['sitestatus'] = $this->Agent_model->getSitestatus();
            $data['liststatus'] = $this->Agent_model->getListstatus();
            $data['proptype'] = $this->Agent_model->getProptype();
            $data['tenure'] = $this->Agent_model->getTenures();
            $data['transtype'] = $this->Agent_model->getTranstype();
            $data['buildcont'] = $this->Agent_model->getBuildcont();
            $data['lettype'] = $this->Agent_model->getLettype();
            $data['considerinfo'] = $this->Agent_model->getConsiderinfo();
            $data['outspace'] = $this->Agent_model->getOutspace();
            $data['parking'] = $this->Agent_model->getParking();
            $data['heattype'] = $this->Agent_model->getHeatingtype();
            $data['splfeature'] = $this->Agent_model->getSplfeature();
            $data['furnish'] = $this->Agent_model->getFurnishes();
            $data['access'] = $this->Agent_model->getAccessibility();
            $data['pricemod'] = $this->Agent_model->getPricemod(1);
            $data['content'] = $this->Agent_model->getContent();
            $this->load->view('property/sale', $data);
            $this->load->view('common/footer');
        } else {

            $this->load->view('login');
        }
    }
    
    public function addrentProp() {
        $parking=$this->input->post('parking');
        $outspace=$this->input->post('out-space');
        $specialfeat=$this->input->post('special-feat');
        if(!empty($outspace)){
            $outspace=implode(',',$this->input->post('out-space'));
        }else{
            $outspace='';
        }
        if(!empty($parking)){
            $parking=implode(',',$this->input->post('parking'));
        }else{
            $parking='';
        }
        if(!empty($specialfeat)){
            $specialfeat=implode(',',$this->input->post('special-feat'));
        }else{
            $specialfeat='';
        }
        $id = $this->session->userdata['agentuser']['id'];
        if ($this->input->post('id') == 0) {
            $data = array(
                'property_type' => 2,
                 'agent_id' => $id,
                'property_pstatus' => $this->input->post('p-status'),
                'property_lstatus' => $this->input->post('l-status'),
                'property_ptype' => $this->input->post('p-type'),
                'property_tenure' => $this->input->post('tenure'),
                'property_ptransact' => $this->input->post('p-transact'),
                'property_buildage' => $this->input->post('build-age'),
                'property_buildconst' => $this->input->post('build-const'),
                'property_lettype' => $this->input->post('let-type'),
                'property_dateavailable' => date('Y-m-d H:i:s', strtotime($this->input->post('dateavail'))),
                'property_contractlen' => $this->input->post('contractlen'),
                'property_depositamount' => $this->input->post('depositamount'),
                'property_considerinfo'=>$this->input->post('let-info1'),
                'property_feesapply' => $this->input->post('fees-aply'),
                'feesapply_details' => $this->input->post('feesapply_details'),
                'property_name' => $this->input->post('p-name'),
                'property_sname' => $this->input->post('s-name'),
                'property_postalc' => $this->input->post('postal-c'),
                'property_city' => $this->input->post('city'),
                'property_county' => $this->input->post('county'),
                'property_ownrefer' => $this->input->post('own-refer'),
                'property_lat' => $this->input->post('Latitude'),
                'property_long' => $this->input->post('Longitude'),
                'property_outspace' =>  $outspace,
                'property_parking' => $parking,
                'property_heat' => $this->input->post('heat'),
                'property_specialfeat' => $specialfeat,
                'property_furnish' => $this->input->post('furnish'),
                'property_access' => $this->input->post('access'),
                'property_pricemod' => $this->input->post('price-mod'),
                'property_price' => $this->input->post('price'),
                'property_bedroom' => $this->input->post('bedroom'),
                'property_bathroom' => $this->input->post('bathroom'),
                'property_reception' => $this->input->post('reception'),
                'property_floors' => $this->input->post('floor'),
                'property_summary' => $this->input->post('summary'),
                'property_fulldesc' => $this->input->post('full-describe'),
                'property_created_on' => date('Y-m-d H:i:s'),
                'property_created_by' => $id,
            );
		//print_r($data);exit();
            $result = $this->Prop_model->addProp($data, $id = 0);
            if($result == 1){
                $this->session->set_flashdata('propertyAdd', 'Property successfully added');
                redirect('properties/Property/rentlist');
            }
        } else {

            $data = array(
                'property_type' => 2,
                 'agent_id' => $id,
                'property_pstatus' => $this->input->post('p-status'),
                'property_lstatus' => $this->input->post('l-status'),
                'property_ptype' => $this->input->post('p-type'),
                'property_tenure' => $this->input->post('tenure'),
                'property_ptransact' => $this->input->post('p-transact'),
                'property_buildage' => $this->input->post('build-age'),
                'property_buildconst' => $this->input->post('build-const'),
                'property_lettype' => $this->input->post('let-type'),
                'property_dateavailable' => date('Y-m-d H:i:s', strtotime($this->input->post('dateavail'))),
                'property_contractlen' => $this->input->post('contractlen'),
                'property_depositamount' => $this->input->post('depositamount'),
                'property_considerinfo'=>$this->input->post('let-info1'),
                'property_feesapply' => $this->input->post('fees-aply'),
                'feesapply_details' => $this->input->post('feesapply_details'),
                'property_name' => $this->input->post('p-name'),
                'property_sname' => $this->input->post('s-name'),
                'property_postalc' => $this->input->post('postal-c'),
                'property_city' => $this->input->post('city'),
                'property_county' => $this->input->post('county'),
                'property_ownrefer' => $this->input->post('own-refer'),
                'property_lat' => $this->input->post('Latitude'),
                'property_long' => $this->input->post('Longitude'),
                'property_outspace' =>  $outspace,
                'property_parking' => $parking,
                'property_heat' => $this->input->post('heat'),
                'property_specialfeat' => $specialfeat,
                'property_furnish' => $this->input->post('furnish'),
                'property_access' => $this->input->post('access'),
                'property_pricemod' => $this->input->post('price-mod'),
                'property_price' => $this->input->post('price'),
                'property_bedroom' => $this->input->post('bedroom'),
                'property_bathroom' => $this->input->post('bathroom'),
                'property_reception' => $this->input->post('reception'),
                'property_floors' => $this->input->post('floor'),
                'property_summary' => $this->input->post('summary'),
                'property_fulldesc' => $this->input->post('full-describe'),
                'property_modified_on' => date('Y-m-d H:i:s'),
                'property_modified_by' => $id,
            );
//            print_r($data);exit();
            $id = $this->input->post('id');
            $result = $this->Prop_model->addProp($data, $id);
            if($result == 1){
                $this->session->set_flashdata('propertyAdd', 'Property successfully added');
                redirect('properties/Property/rentlist');
            }
        }
    }

    public function proprentEdit() {
//        print_r($this->session->all_userdata());exit();
        if (isset($this->session->userdata['agentuser']['logged_in']) && $this->session->userdata['agentuser']['logged_in'] == TRUE) {
            $id = $this->uri->segment(4);
            $data['property'] = $this->Prop_model->getPropertybyid($id);
            $data['keyfeat'] = $this->Prop_model->getKeyfeature($id);
            $data['images'] = $this->Prop_model->getImages($id);
            $data['contents'] = $this->Prop_model->getContents($id);
            $this->load->view('common/header');
            $data1['salecount'] = $this->Agent_model->getSalepropcount();
            $data1['rentcount'] = $this->Agent_model->getRentpropcount();
            $this->load->view('common/propertymenu', $data1);
            $data['sitestatus'] = $this->Agent_model->getSitestatus();
            $data['liststatus'] = $this->Agent_model->getListstatusr();
            $data['proptype'] = $this->Agent_model->getProptype();
            $data['tenure'] = $this->Agent_model->getTenures();
            $data['transtype'] = $this->Agent_model->getTranstype();
            $data['buildcont'] = $this->Agent_model->getBuildcont();
            $data['lettype'] = $this->Agent_model->getLettype();
            $data['feesapply'] = $this->Agent_model->getFeesapply();
            $data['considerinfo'] = $this->Agent_model->getConsiderinfo();
            $data['outspace'] = $this->Agent_model->getOutspace();
            $data['parking'] = $this->Agent_model->getParking();
            $data['heattype'] = $this->Agent_model->getHeatingtype();
            $data['splfeature'] = $this->Agent_model->getSplfeature();
            $data['furnish'] = $this->Agent_model->getFurnishes();
            $data['access'] = $this->Agent_model->getAccessibility();
            $data['pricemod'] = $this->Agent_model->getPricemod(2);
            $data['content'] = $this->Agent_model->getContent();
            $this->load->view('property/rent', $data);
            $this->load->view('common/footer');
        } else {

            $this->load->view('login');
        }
    }

    public function getrentprop() {
        $result = $this->Prop_model->getRentProperty();
        $cnt = $this->Agent_model->getRentpropcount();
        $offset = $this->input->post('offset');
        $perpage = $this->input->post('row');
        $rows = 0;
        if ($cnt > 10) {
            $rows = $cnt / 10;
        }
        $diable = '';
        if ($rows == 0) {
            $diable = 'disabled';
        }


        if($offset == 1){
            $key = 1;
        } else if($offset == 2){
            $key = 11;
        } else if($offset == 3){
            $key = 21;
        }

        $html = '';
        $html = '<tbody class="data-append">';
        if (!empty($result)) {
            foreach ($result as $res) {
                echo $key;
                $html.='<tr id="' . $res->property_id . '">
                   <td>' . $key . '</td>
                   <td>' . $res->property_ownrefer . '</td>
                   <td>' . $res->property_price . '</td>
                   <td>' . $res->property_sname . '</td>
                   <td>' . $res->propertytype_name . '</td>
                   <td>' . date('d-m-Y', strtotime($res->property_created_on)) . '</td>
                   <td>' . $res->listingstatus_name . '</td>
                   <td>' . date('d-m-Y', strtotime($res->property_modified_on)) . '</td>
                   <td>' . $res->imgcnt . ' / ' . $res->floorcnt . ' / ' . $res->epc . '</td>
                   <td>' . $res->sitestatus_name . '</td>';
                $html.='<td><a class="btn btn-primary btn-xs" href="' . base_url('properties/property/proprentEdit') . '/' . $res->property_id . '"><i class="fa fa-pencil"></i></button></td>';
                $html.='</tr>';
                $key++;
            }
        } else {
            $html.='<tr><td colspan="7">No Record Found</td></tr>';
        }
        $html.='</tbody>';
       
            $html.='<tfoot class="foot">
                        <tr>
                        <!-- <td colspan="2">Showing </td> -->
                        <td colspan="3"> 
                        <div class="btn-group">'.($key-1).' of '.$cnt.'
                        
                        </div> results
                        </td>';
             if ($rows > 1) {
                        $html.='<td colspan="11">
                        <ul class="pagination pagination-sm">';
            if ($diable != 'disabled') {
                $html.='<li class="' . $diable . '"><a href="javascript:;"  class="prev"><i class="fa fa-arrow-left"></i> Prev</a></li>';
            } else {
                $html.='<li class="' . $diable . '"><a href="javascript:;" ><i class="fa fa-arrow-left"></i> Prev</a></li>';
            }
            for ($i = 1; $i <= $rows + 1; $i++) {
                if ($i == $offset) {
                    $cls = 'active';
                } else {
                    $cls = '';
                }
                $html.='<li class="' . $cls . '"><a href="javascript:;" class="offset">' . $i . '</a></li>';
            }
            if ($diable != 'disabled') {
                $html.='<li class="' . $diable . '"><a href="javascript:;"  class="next"><i class="fa fa-arrow-right"></i> Next</a></li>';
            } else {
                $html.='<li class="' . $diable . '"><a href="javascript:;" ><i class="fa fa-arrow-right"></i> Next</a></li>';
            }
            $html.='</ul>
                        </td>';
             }
                        $html.='</tr>
                        </tfoot>';
        
        echo $html;
    }

    public function checkref() {

        $id = $this->input->post('code');
        $query = $this->db->query("select * from pro_tbl_property where lower(property_ownrefer)=lower('$id')");
        if ($query->num_rows() > 0) {
            echo 1;
        } else {
            echo 0;
        }
    }

    public function getdashboardprop() {
        $result = $this->Prop_model->getAllProperty();

        $html = '';
        $str = '';
        $html = '<tbody class="data-append">';
        if (!empty($result)) {
            foreach ($result as $key => $res) {
                if (strlen($res->property_sname) > 50) {
                    $str = substr($res->property_sname, 0, 50) . '...';
                }else{
                    $str = $res->property_sname;
                }

                $html.='<tr id="' . $res->property_id . '">
                   <td class="text-center">' . ($key + 1) . '</td>';
                if ($res->property_type == 1) {
                    $type = 'Sale';
                } elseif ($res->property_type == 2) {
                    $type = 'Rent';
                } else {
                    $type = '';
                }
                $html.='<td>' . $type . '</td>
                   <td>' . $str . '</td>
                   <td>' . date('d-m-Y', strtotime($res->property_created_on)) . '</td><td>' . date('d-m-Y', strtotime($res->property_modified_on)) . '</td>';
                $html.='</tr>';
            }
        } else {
            $html.='<tr><td colspan="4">No Record Found</td></tr>';
        }
        $html.='</tbody>';

        echo $html;
    }

    public function getBranch() {
        $result = $this->Agent_model->getBranch();
        $html = '';
        $html = '<tbody class="data-append">';
        if (!empty($result)) {
            foreach ($result as $key => $res) {

                if (strlen($res->street) > 25) {
                    $str = substr($res->street, 0, 25) . '...';
                }

                $html.='<tr id="' . $res->agent_id . '">
                   <td  class="text-center">' . ($key + 1) . '</td>
                   <td>' . $res->agent_name . '</td>
                   <td>' . $str . '</td>';
                $html.='</tr>';
            }
        } else {
            $html.='<tr><td colspan="4">No Record Found</td></tr>';
        }
        $html.='</tbody>';
        echo $html;
    }

    public function showmap() {
        $id = $this->uri->segment(4);

        $this->load->view('common/header');
        $data1['salecount'] = $this->Agent_model->getSalepropcount();
        $data1['rentcount'] = $this->Agent_model->getRentpropcount();
        $this->load->view('common/propertymenu', $data1);
        $data['prop_type'] = $id;
        $this->load->view('property/showmap', $data);
        $this->load->view('common/footer');
    }

    public function getmarker() {
        $pid = $this->input->get('proptype');
        $result = $this->Prop_model->getmarker($pid);
        $json = array();
        foreach ($result as $val) {
            $data = array(
                'pname' => $val->property_name,
                'address' => $val->property_sname,
                'lat' => $val->property_lat,
                'long' => $val->property_long,
                'image' => $val->property_image
            );
            array_push($json, $data);
        }
        echo json_encode($json);
    }

    public function export() {
        $aid = $this->session->userdata['agentuser']['id'];
        $type = $this->uri->segment(4);
        $status = $this->uri->segment(5);
        $whr='';
        if($status!=0){
            $whr="and lower(sitestatus_name)=lower('$status')";
        }
        $this->load->library('PHPExcel');
        
//        $this->load->library('PHPExcel/IOFactory');

        $objPHPExcel = new PHPExcel();
//        $reader = PHPExcel_IOFactory::createReader('Excel5');
        $objPHPExcel->getProperties()->setTitle("export")->setDescription("none");

        $objPHPExcel->setActiveSheetIndex(0);
        $query = $this->db->query("SELECT property_ownrefer,property_price,property_sname,propertytype_name,property_created_on,listingstatus_name,property_modified_on,property_ownrefer,sitestatus_name FROM pro_tbl_property a left join easamoveadmin_tbl_propertytype b on (a.property_type=b.propertytype_id) left join easamoveadmin_tbl_sitestatus c on (a.property_pstatus=c.sitestatus_id) left join easamoveadmin_tbl_listingstatus d on (a.property_lstatus=d.listingstatus_id) where  agent_id=$aid and property_type=$type $whr");
        // Field names in the first row
        $fields = $query->list_fields();
        $col = 0;
        foreach ($fields as $field) {
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, 1, $field);
            $col++;
        }

        // Fetching the table data
        $row = 2;
        foreach ($query->result() as $data) {
            $col = 0;
            foreach ($fields as $field) {
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $data->$field);
                $col++;
            }

            $row++;
        }

        $objPHPExcel->setActiveSheetIndex(0);

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');

        // Sending headers to force the user to download the file
        header('Content-Type: application/csv');
        header('Content-Disposition: attachment;filename="Products_' . date('dMy') . '.csv"');
        header('Cache-Control: max-age=0');

//     $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "PDF");
        $objWriter->save('php://output');
    }

    public function create($type,$status) {
       // echo $type.'--'.$status;exit();
        $whr='';
        if($status!='0'&&$status!='undefined'&&$status!='All'){
            $whr="and lower(c.sitestatus_name)=lower('$status')";
        }
//        echo $status;
//        echo "SELECT * FROM pro_tbl_property a left join easamoveadmin_tbl_propertytype b on (a.property_type=b.propertytype_id) left join easamoveadmin_tbl_sitestatus c on (a.property_pstatus=c.sitestatus_id) left join easamoveadmin_tbl_listingstatus d on (a.property_lstatus=d.listingstatus_id) WHERE agent_id=$aid and property_type=$type $whr";exit();
        $aid = $this->session->userdata['agentuser']['id'];
        
        $this->load->library('Pdf');
//        
        $pdf = new Pdf(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $pdf->SetAuthor('Nicola Asuni');
        $pdf->SetTitle('');
        $pdf->SetSubject('TCPDF Tutorial');
        $pdf->SetKeywords('Customer');

        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', '8'));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
        if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
            require_once(dirname(__FILE__) . '/lang/eng.php');
            $pdf->setLanguageArray($l);
        }

// ---------------------------------------------------------
// set font
        $pdf->SetFont('helvetica', '', 10);

// add a page
        $pdf->AddPage();
       
        
        $query = $this->db->query("SELECT * FROM pro_tbl_property a left join easamoveadmin_tbl_propertytype b on (a.property_type=b.propertytype_id) left join easamoveadmin_tbl_sitestatus c on (a.property_pstatus=c.sitestatus_id) left join easamoveadmin_tbl_listingstatus d on (a.property_lstatus=d.listingstatus_id) WHERE agent_id=$aid and property_is_deleted=0 and property_type=$type $whr");
//echo $this->db->last_query();exit();
       
        $html = '';

        $html.='<html xmlns="http://www.w3.org/1999/xhtml" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head><body style="font-size:8px;font-family:Arial, Helvetica, sans-serif">';
       

        $html .= '<table  bgcolor="#E1E1E1"  width="100%" cellpadding="4" cellspacing="1" >
                    <thead>
                        <tr bgcolor="#E1E1E1" style="color: #fff;">
                            <th width="5%" style="color: #000000; font-weight: bold; ">S.NO</th>
                            <th width="10%" style="color: #000000; font-weight: bold; ">PROPERTY REF NO</th>
                            <th width="10%" style="color: #000000; font-weight: bold; ">PROPERTY PRICE</th>
                            <th width="15%" style="color: #000000; font-weight: bold; ">PROPERTY ADDRESS</th>
                            <th width="10%" style="color: #000000; font-weight: bold; ">PROPERTY TYPE</th>
                            <th width="10%" style="color: #000000; font-weight: bold; ">PROPERTY LISTED</th>
                            <th width="10%" style="color: #000000; font-weight: bold; ">PROPERTY LISTING STATUS</th>
                            <th width="10%" style="color: #000000; font-weight: bold; ">PROPERTY MODIFIED</th>
                            <th width="10%" style="color: #000000; font-weight: bold; ">PROPERTY PHOTO/FLOOR PLAN/EPC</th>
                            <th width="10%" style="color: #000000; font-weight: bold; ">PROPERTY SITE STATUS</th>
                        </tr>
                    </thead></table><table  bgcolor="#F4F2F2"  width="100%" cellpadding="4" cellspacing="1">
                    <tbody class="tb">';
        foreach ($query->result() as $key => $row) {


            $html.='<tr >'
                    . '<td width="5%" align="left" bgcolor="#FFFFFF">' . ($key+1) . '</td>'
                    . '<td width="10%" align="left" bgcolor="#FFFFFF">' . $row->property_ownrefer . '</td>'
                     . '<td width="10%" align="left" bgcolor="#FFFFFF">' . $row->property_price . '</td>'
                    . '<td  width="15%" align="left" bgcolor="#FFFFFF">' . $row->property_sname . '</td>'
                    . '<td  width="10%" align="center" bgcolor="#FFFFFF">' . $row->propertytype_name . '</td>'
                    . '<td  width="10%" align="center" bgcolor="#FFFFFF">' . $row->property_created_on . '</td>'
                    . '<td  width="10%" align="center" bgcolor="#FFFFFF">' . $row->listingstatus_name . '</td>'
                    . '<td  width="10%" align="center" bgcolor="#FFFFFF">' . $row->property_modified_on . '</td>'
                    . '<td  width="10%" align="center" bgcolor="#FFFFFF">' . $row->property_ownrefer . '</td>'
                    . '<td  width="10%" align="center" bgcolor="#FFFFFF">' . $row->sitestatus_name . '</td>'
                    . '</tr>';
        }
        $html.='</tbody></table></body></html>';
        $pdf->writeHTML($html, true, false, true, false, '');

        $filename = 'property'.'_'.$aid.'_'.date('Y-m-d');
//            $filelocation = base_url()."Images/pdfimg"; //Linux
//             $fileNL = $filelocation."/".$filename; //Linux
        ob_clean();
          //$root = $_SERVER["DOCUMENT_ROOT"];
        $filetosave =  '/images/'.$filename.'.pdf';
        $output = $pdf->Output($filetosave, 'FI');
    }

    public function downpdf() {
        $type = $this->input->post('type');
        $status = $this->input->post('sel');
        $this->create($type,$status);
    }
    function generateRandomString($length) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function upload() {

        $aid = $this->session->userdata['agentuser']['id'];
        $aname = $this->session->userdata['agentuser']['name'];
        $generatekey = $this->generateRandomString(6);

        $upload = file_get_contents('php://input');
        // print_r( $upload );
//        $contentupload = $_FILES['upload-file'];
        $config = array(
            'upload_path' => 'images/tempimg',
            'allowed_types' => 'jpg|gif|png|jpeg|gif|PNG|JPG|tiff|tif',
            'overwrite' => 1,
        );
        $this->load->library('upload', $config);
        $this->load->library('image_lib');


        $fn = (isset($_SERVER['HTTP_X_FILENAME']) ? $_SERVER['HTTP_X_FILENAME'] : false);
        // print_r($_SERVER);
        $fn = $aname." ".$aid." ".$fn;
        // echo '3'.$fn;exit();
        if ($fn) {

            $repl=array(' ','-',':');
            $fn=  str_replace($repl, '_', $fn);

            // AJAX call
            file_put_contents(
                    'images/tempimg/' . $fn, file_get_contents('php://input')
            );
            $config1 = array(
                'image_library' => 'gd2',
                'source_image' => 'images/tempimg/' . $fn, //path to the uploaded image
                'new_image' => 'thumb_' . $fn, //path to
                'maintain_ratio' => true,
                'width' => 50,
                'height' => 50
            );
//                $config['file_name'] ='thumb_18';
            $this->image_lib->initialize($config1);
            $this->image_lib->resize();
            $config2 = array(
                'image_library' => 'gd2',
                'source_image' => 'images/tempimg/' . $fn, //path to the uploaded image
                'new_image' => 'medium_' . $fn, //path to
                'maintain_ratio' => true,
                'width' => 125,
                'height' => 125
            );
            $this->image_lib->initialize($config2);
            $this->image_lib->resize();

            echo "$fn uploaded";
            
            exit();
        } else {

            // form submit
            $files = $_FILES['fileupload'];

            foreach ($files['error'] as $id => $err) {
                if ($err == UPLOAD_ERR_OK) {
                    $fn = $files['name'][$id];
                    move_uploaded_file(
                            $files['tmp_name'][$id], 'images/tempimg/' . $fn
                    );
                    echo "<p>File $fn uploaded.</p>";
                }
            }
        }
    }
    public function deleteimg(){
         $this->db->where_in('propimg_l', $_GET['file']);
         $this->db->delete('pro_tbl_propimg');
        unlink('images/tempimg/'.$_GET['file']);
        unlink('images/tempimg/medium_'.$_GET['file']);
        unlink('images/tempimg/thumb_'.$_GET['file']);
        unlink('images/mainimg/'.$_GET['file']);
        unlink('images/mainimg/medium_'.$_GET['file']);
        unlink('images/mainimg/thumb_'.$_GET['file']);
    }

}
