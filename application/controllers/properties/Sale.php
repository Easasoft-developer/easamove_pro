<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Sale extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
         $this->load->library('session');

        // Your own constructor code
    }

    public function index() {
        $this->load->library('session');
        
        if (isset($this->session->userdata['adminuser']['logged_in'])&& $this->session->userdata['adminuser']['logged_in']== TRUE) {
           $this->load->view('common/header');
           $this->load->view('property/property');
            $this->load->view('property/sale');
            $this->load->view('common/footer');
           
        } else {
            
        $this->load->view('login');
        }
    }

}
