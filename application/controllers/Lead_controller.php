<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lead_controller extends CI_Controller {

	public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('Lead_model');
        $this->load->library('session');
        $this->load->library("pagination");

        // Your own constructor code
    }

	public function index()
	{
		if (isset($this->session->userdata['agentuser']['logged_in']) && $this->session->userdata['agentuser']['logged_in'] == TRUE) {
			$data['saleCount']=$this->Lead_model->saleCount();
			$data['rentCount']=$this->Lead_model->rentCount();
			$data['valuationCount']=$this->Lead_model->valuationCount();
			$this->load->view('common/header');
            $this->load->view('common/leadmenu');
            $this->load->view('lead/lead_summary', $data);
            $this->load->view('common/footer');
		}
	}

	//For sale leads
	public function forsale()
	{
		if (isset($this->session->userdata['agentuser']['logged_in']) && $this->session->userdata['agentuser']['logged_in'] == TRUE) {
			$this->load->view('common/header');
            $this->load->view('common/leadmenu');
            $this->load->view('lead/lead_sale');
            $this->load->view('common/footer');
		}
	}

	//For sale leads
	public function rent()
	{
		if (isset($this->session->userdata['agentuser']['logged_in']) && $this->session->userdata['agentuser']['logged_in'] == TRUE) {
			$this->load->view('common/header');
            $this->load->view('common/leadmenu');
            $this->load->view('lead/lead_rent');
            $this->load->view('common/footer');
		}
	}

	//For sale leads
	public function valuation()
	{
		if (isset($this->session->userdata['agentuser']['logged_in']) && $this->session->userdata['agentuser']['logged_in'] == TRUE) {
			$this->load->view('common/header');
            $this->load->view('common/leadmenu');
            $this->load->view('lead/lead_valuation');
            $this->load->view('common/footer');
		}
	}

	//search leads in way
	public function searchLeads(){
		$query = $this->Lead_model->searchLeads();
		$html="";
        if(!empty($query)){
        foreach ($query as $key => $leads) {
            $html.='<tr>
            	<td>'.date('d-m-Y', strtotime($leads->created_on)).'</td>
                <td>' . $leads->user_name . '<br>'. $leads->user_email .'<br>'.$leads->user_phone.'<br>'.$leads->user_postcode.'</td>
                <td>' . $leads->user_status . '</td>
                <td><a href="#">' . $leads->property_name."<br>".$leads->property_sname.", ".$leads->property_city.", ".$leads->property_postalc . '</a></td>
                <td>' . $leads->user_message . '</td>';

            
        }
        }else{
             $html.='1';
        }
        echo $html;
	}

	//download pdf
	public function createbranch() {
        $aid = $this->session->userdata['agentuser']['id'];
        $this->load->library('Pdf');
//        
        $pdf = new Pdf(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $pdf->SetAuthor('Nicola Asuni');
        $pdf->SetTitle('');
        $pdf->SetSubject('TCPDF Tutorial');
        $pdf->SetKeywords('Customer');

        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', '8'));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
        if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
            require_once(dirname(__FILE__) . '/lang/eng.php');
            $pdf->setLanguageArray($l);
        }

// ---------------------------------------------------------
// set font
        $pdf->SetFont('helvetica', '', 10);

// add a page
        $pdf->AddPage();


        $date=$this->input->post('date');
        $type=$this->input->post('type');
        $sept = explode("-", $date);
        $whr1=$whr2="";
        if($type == 'sale'){
            $whr1="AND b.property_lstatus in (5,6)";
        }else if($type=='rent'){
            $whr1="AND b.property_lstatus in (2,3)";
        }
        $datetrim1 = trim($sept[0]);
        $datetrim2 = trim($sept[1]);

        if($date!=""){
            if($sept[0]!="" && $sept[1]!=""){
                $whr2="AND a.created_on >= '$datetrim1' AND a.created_on <= '$datetrim2'";
            }
        }



        $query = $this->db->query("SELECT a.property_id,a.agent_id,a.user_name,a.user_email,a.user_phone,a.user_postcode,a.enquiry_status,a.user_message,a.user_status,a.created_on,b.property_name,b.property_sname,b.property_city,b.property_postalc,b.property_lstatus FROM  portal_tbl_contact_agent a LEFT JOIN pro_tbl_property b on (a.property_id=b.property_id) where a.agent_id=$aid AND a.property_id!=0 $whr1 $whr2");
        $html = '';
        
        $html.='<html xmlns="http://www.w3.org/1999/xhtml" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head><body style="font-size:8px;font-family:Arial, Helvetica, sans-serif">';
$html .= '<table width="100%" cellpadding="4" cellspacing="1">
                <tr>
                  <td style="vertical-align: top;"></td>
                  <td style="text-align:left;font-style:12px;">ForSale Leads</td>
                      <td align="right">
                         <!--<img src="" alt="logo" width="70" height="100"/>-->
                      </td>
                </tr>
              </table>';

$html .= '<table  bgcolor="#E1E1E1"  width="100%" cellpadding="4" cellspacing="1" >
                    <thead>
                        <tr bgcolor="#E1E1E1" style="color: #fff;">
                            <th width="20%" style="color: #000000; font-weight: bold; ">Date</th>
                            <th width="20%" style="color: #000000; font-weight: bold; ">Customer details</th>
                            <th width="20%" style="color: #000000; font-weight: bold; ">Customer status</th>
                            <th width="20%" style="color: #000000; font-weight: bold; ">Property</th>
                            <th width="20%" style="color: #000000; font-weight: bold; ">Message</th>
                            
                        </tr>
                    </thead></table><table  bgcolor="#F4F2F2"  width="100%" cellpadding="4" cellspacing="1">
                    <tbody class="tb">';
        foreach ($query->result() as $key=>$leads) {
            
            
            $html.='<tr >'
                    . '<td width="20%" align="left" bgcolor="#FFFFFF">' . date('d-m-Y', strtotime($leads->created_on)) . '</td>'
                    . '<td width="20%" align="left" bgcolor="#FFFFFF">'  . $leads->user_name . '<br>'. $leads->user_email .'<br>'.$leads->user_phone.'<br>'.$leads->user_postcode. '</td>'
                    . '<td  width="20%" align="left" bgcolor="#FFFFFF">'. $leads->user_status . '</td>'
                    . '<td  width="20%" align="center" bgcolor="#FFFFFF">' . $leads->property_name."<br>".$leads->property_sname.", ".$leads->property_city.", ".$leads->property_postalc . '</td>'
                    .'<td  width="20%" align="center" bgcolor="#FFFFFF">' . $leads->user_message . '</td>'
                    . '</tr>';
        }
        $html.='</tbody></table></body></html>';
        $pdf->writeHTML($html, true, false, true, false, '');

        $filename = 'leadsprint'.'_'.$aid.'_'.date('Y-m-d');
//            $filelocation = base_url()."Images/pdfimg"; //Linux
//             $fileNL = $filelocation."/".$filename; //Linux
        ob_clean();
        // $root = realpath($_SERVER["DOCUMENT_ROOT"]);
        $filetosave = '/images/pdf/' . $filename . '.pdf';
        $output = $pdf->Output($filetosave, 'FI');
    }


	public function leadsprint(){
        $this->createbranch();
    }    

    //search valuation
    public function searchValuation(){
    	$query = $this->Lead_model->searchValuation();
		$html="";
        if(!empty($query)){
        foreach ($query as $key => $valuation) {
            $html.='<tr>
            	<td>'.date('d-m-Y', strtotime($valuation->created_on)).'</td>
                <td>' . $valuation->appraisal_name . '<br>'. $valuation->appraisal_email .'<br>'.$valuation->appraisal_phone.'</td>
                <td>' . $valuation->enquiry_type_appraisal . '</td>
                <td><a href="#">' . $valuation->appraisal_address . '</a></td>
                <td>' . $valuation->appraisal_message . '</td>';

            
        }
        }else{
             $html.='1';
        }
        echo $html;
    }

    //valuation pdf generator
    public function createValuation(){
    	$aid = $this->session->userdata['agentuser']['id'];
        $this->load->library('Pdf');
//        
        $pdf = new Pdf(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $pdf->SetAuthor('Nicola Asuni');
        $pdf->SetTitle('');
        $pdf->SetSubject('TCPDF Tutorial');
        $pdf->SetKeywords('Customer');

        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', '8'));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
        if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
            require_once(dirname(__FILE__) . '/lang/eng.php');
            $pdf->setLanguageArray($l);
        }

// ---------------------------------------------------------
// set font
        $pdf->SetFont('helvetica', '', 10);

// add a page
        $pdf->AddPage();


        $date=$this->input->post('date');
        $sept = explode("-", $date);
        $whr1=$whr2="";
        $datetrim1 = trim($sept[0]);
        $date1 = date('Y-m-d',strtotime($datetrim1));
        $datetrim2 = trim($sept[1]);
        $date2 = date('Y-m-d',strtotime($datetrim2));

        if($date!=""){
            if($sept[0]!="" && $sept[1]!=""){
                $whr2="AND created_on between '$date1' AND '$date2'";
            }
        }



        $query = $this->db->query("SELECT * FROM portal_tbl_agent_valuation WHERE FIND_IN_SET('$aid', appraisal_agent_id) $whr2");
       
        $html = '';
        
        $html.='<html xmlns="http://www.w3.org/1999/xhtml" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head><body style="font-size:8px;font-family:Arial, Helvetica, sans-serif">';
$html .= '<table width="100%" cellpadding="4" cellspacing="1">
                <tr>
                  <td style="vertical-align: top;"></td>
                  <td style="text-align:left;font-style:12px;">valuation Leads</td>
                      <td align="right">
                         <!--<img src="" alt="logo" width="70" height="100"/>-->
                      </td>
                </tr>
              </table>';

$html .= '<table  bgcolor="#E1E1E1"  width="100%" cellpadding="4" cellspacing="1" >
                    <thead>
                        <tr bgcolor="#E1E1E1" style="color: #fff;">
                            <th width="20%" style="color: #000000; font-weight: bold; ">Date</th>
                            <th width="20%" style="color: #000000; font-weight: bold; ">Customer details</th>
                            <th width="20%" style="color: #000000; font-weight: bold; ">Customer status</th>
                            <th width="20%" style="color: #000000; font-weight: bold; ">Property area</th>
                            <th width="20%" style="color: #000000; font-weight: bold; ">Message</th>
                            
                        </tr>
                    </thead></table><table  bgcolor="#F4F2F2"  width="100%" cellpadding="4" cellspacing="1">
                    <tbody class="tb">';
        foreach ($query->result() as $key=>$valuation) {
            
            
            $html.='<tr >'
                    . '<td width="20%" align="left" bgcolor="#FFFFFF">' . date('d-m-Y', strtotime($valuation->created_on)) . '</td>'
                    . '<td width="20%" align="left" bgcolor="#FFFFFF">'  . $valuation->appraisal_name . '<br>'. $valuation->appraisal_email .'<br>'.$valuation->appraisal_phone. '</td>'
                    . '<td  width="20%" align="left" bgcolor="#FFFFFF">'. $valuation->enquiry_type_appraisal . '</td>'
                    . '<td  width="20%" align="center" bgcolor="#FFFFFF">' . $valuation->appraisal_address . '</td>'
                    .'<td  width="20%" align="center" bgcolor="#FFFFFF">' . $valuation->appraisal_message . '</td>'
                    . '</tr>';
        }
        $html.='</tbody></table></body></html>';
        $pdf->writeHTML($html, true, false, true, false, '');

        $filename = 'leadsvaluation'.'_'.$aid.'_'.date('Y-m-d');
//            $filelocation = base_url()."Images/pdfimg"; //Linux
//             $fileNL = $filelocation."/".$filename; //Linux
        ob_clean();
        // $root = realpath($_SERVER["DOCUMENT_ROOT"]);
        $filetosave = '/images/pdf/' . $filename . '.pdf';
        $output = $pdf->Output($filetosave, 'FI');
    }

    public function leadValuationPdf(){
        $this->createValuation();
    }

    //Export as excel
    public function exportValuation(){
    	$aid = $this->session->userdata['agentuser']['id'];
        $type = $this->uri->segment(3);
        $this->load->library('PHPExcel');
//        $this->load->library('PHPExcel/IOFactory');

        $objPHPExcel = new PHPExcel();
//        $reader = PHPExcel_IOFactory::createReader('Excel5');
        $objPHPExcel->getProperties()->setTitle("export")->setDescription("none");

        // $date=$type;
        // $sept = explode("-", $date);
        // $whr1=$whr2="";
        // $datetrim1 = trim($sept[0]);
        // $date1 = date('Y-m-d',strtotime($datetrim1));
        // $datetrim2 = trim($sept[1]);
        // $date2 = date('Y-m-d',strtotime($datetrim2));

        // if($date!=""){
        //     if($sept[0]!="" && $sept[1]!=""){
        //         $whr2="AND created_on between '$date1' AND '$date2'";
        //     }
        // }

        $objPHPExcel->setActiveSheetIndex(0);
        $query = $this->db->query("SELECT * FROM portal_tbl_agent_valuation WHERE FIND_IN_SET('$aid', appraisal_agent_id)");
        // Field names in the first row
        $fields = $query->list_fields();
        $col = 0;
        foreach ($fields as $field) {
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, 1, $field);
            $col++;
        }

        // Fetching the table data
        $row = 2;
        foreach ($query->result() as $data) {
            $col = 0;
            foreach ($fields as $field) {
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $data->$field);
                $col++;
            }

            $row++;
        }

        $objPHPExcel->setActiveSheetIndex(0);

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

        // Sending headers to force the user to download the file
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="leadvaluation'.'_'.$aid.'_'.date('Y-m-d').'.csv"');
        header('Cache-Control: max-age=0');

//     $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "PDF");
        $objWriter->save('php://output');
    }

    //Export as forsale and rent
    public function exportLeads(){
    	$aid = $this->session->userdata['agentuser']['id'];
        $type = $this->uri->segment(3);
        $this->load->library('PHPExcel');
//        $this->load->library('PHPExcel/IOFactory');

        $objPHPExcel = new PHPExcel();
//        $reader = PHPExcel_IOFactory::createReader('Excel5');
        $objPHPExcel->getProperties()->setTitle("export")->setDescription("none");

        //$type=$type;
        $whr1=$whr2="";
        if($type == 'sale'){
            $whr1="AND b.property_lstatus in (2,3)";
        }else if($type=='rent'){
            $whr1="AND b.property_lstatus in (5,6)";
        }

        $objPHPExcel->setActiveSheetIndex(0);
        $query = $this->db->query("SELECT a.property_id,a.agent_id,a.user_name,a.user_email,a.user_phone,a.user_postcode,a.enquiry_status,a.user_message,a.user_status,a.created_on,b.property_name,b.property_sname,b.property_city,b.property_postalc,b.property_lstatus FROM  portal_tbl_contact_agent a LEFT JOIN pro_tbl_property b on (a.property_id=b.property_id) where a.agent_id=$aid AND a.property_id!=0 $whr1");
        // Field names in the first row
        $fields = $query->list_fields();
        $col = 0;
        foreach ($fields as $field) {
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, 1, $field);
            $col++;
        }

        // Fetching the table data
        $row = 2;
        foreach ($query->result() as $data) {
            $col = 0;
            foreach ($fields as $field) {
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $data->$field);
                $col++;
            }

            $row++;
        }

        $objPHPExcel->setActiveSheetIndex(0);

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

        // Sending headers to force the user to download the file
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="leads'.'_'.$aid.'_'.date('Y-m-d').'.csv"');
        header('Cache-Control: max-age=0');

//     $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "PDF");
        $objWriter->save('php://output');
    }

    //letting information text 
    public function letting(){
        $data['leadinfo'] = $this->Lead_model->getAgentLettingInformation();
        $this->load->view('lead_information',$data);
    }

    //update letting information
    public function updateletting(){
        $value = $this->input->post('Letting_information');
        $data = array(
            'letting_information' => $value
        );
        $result = $this->Lead_model->updateLetting($data);
        if($result){
            $this->session->set_flashdata('lettingInformation', 'Letting information updated successfully');
            redirect('Lead_controller/letting');
        }
    }


}
