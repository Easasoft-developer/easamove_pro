<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Account extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('Lead_model');
        $this->load->model('Agent_model');
        $this->load->library('session');
        $this->load->library('form_validation');
        $this->load->model('Agent_model');
        $this->load->model('Prop_model');


        // Your own constructor code
    }

    public function index() {
        if (isset($this->session->userdata['agentuser']['logged_in']) && $this->session->userdata['agentuser']['logged_in'] == TRUE) {
            
            $data['salecount'] = $this->Agent_model->getSalepropcount();
            $data['rentcount'] = $this->Agent_model->getRentpropcount();
            $data['saleCount']=$this->Lead_model->saleCount();
            $data['rentCount']=$this->Lead_model->rentCount();
            $data['valuationCount']=$this->Lead_model->valuationCount();
            $this->load->view('dashboard', $data);
        } else {

            $this->load->view('login');
        }
    }

    public function login() {
        if($this->input->post('UserName')!=''&&$this->input->post('Password')!=''){
        $data = array(
            'email_id' => $this->input->post('UserName'),
            'agent_password' => $this->input->post('Password'),
            'agent_is_deleted' => 0
        );
        $res = $this->Agent_model->login($data);
//        print_r($res);exit();
        if (!empty($res)) {
           
            // $data['salecount'] = $this->Agent_model->getSalepropcount();
            // $data['rentcount'] = $this->Agent_model->getRentpropcount();
            // $this->load->view('dashboard', $data);
            redirect('dashboard');
        } else {
            if (isset($this->session->userdata['agentuser']['logged_in']) && $this->session->userdata['agentuser']['logged_in'] == TRUE) {
                // $data['salecount'] = $this->Agent_model->getSalepropcount();
                // $data['rentcount'] = $this->Agent_model->getRentpropcount();
                // $this->load->view('dashboard', $data);
                redirect('dashboard');
            } else {
                $this->session->set_flashdata('logincheck', 'Invalid Username (or) Password');

                $this->load->view('login');
            }
        }
        }else{
            $this->load->view('login');
        }
    }

    public function logout() {
        $session_array = array(
            'id' => '',
            'name' => '',
            'email' => '',
            'logged_in' => FALSE
        );

        $this->session->unset_userdata('agentuser');

        redirect();
    }

    public function verify($key) {

//        $key = $this->input->get('key');
        if ($key != '') {
            $res = $this->Agent_model->verify($key);
            if ($res) {
                $this->load->library('email');
                $config['charset'] = 'iso-8859-1';
                $config['wordwrap'] = TRUE;
                $config['mailtype'] = 'html';
                $this->email->initialize($config);
                $mydata['name'] = $res[0]->agent_name;
                $mydata['email'] = $res[0]->email_id;
                $mydata['password'] = $res[0]->agent_password;

                $message = $this->load->view('mail_template/agent_verify', $mydata, true);
                $this->email->from('info@easasoft.com', 'Easasoft');

                $this->email->to($res[0]->email_id);

                $this->email->subject('Welcome to Easamove');

                $this->email->message($message);

                $this->email->send();
            }
            $this->session->set_flashdata('logincheck', 'Agent verified successfully, your account detail has been sent to your registered email id');
        } else {
            $this->session->set_flashdata('logincheck', '');
        }
        $this->load->view('login');
    }

    public function forgotpassword() {
        $email = $this->input->post('UserEmail');
        if ($email != '') {
            $this->Agent_model->forgot_email($email);
            $this->session->set_flashdata('forgot', 'Email Send to your mail address');
        } else {
            $this->session->set_flashdata('forgot', '');
        }
        $this->load->view('forgotpassword');
    }

    public function resetpassword() {
        $data['getId'] = $this->input->get('user');
        $this->load->view('resetpassword', $data);
    }

    public function reset() {
        $result = $this->Agent_model->resetPass();
        if ($result > 0) {
           
//            $this->load->view('login', $data);
            redirect();
        } else {
            $data['message'] = "There is error in clicked link please reset again by clicking forgot password";
            $this->load->view('resetpassword', $data);
        }
    }

}
