<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
         $this->load->library('session');
         $this->load->model('Agent_model');
          $this->load->model('Prop_model');
          $this->load->model('Lead_model');

        // Your own constructor code
    }

    public function index() {
        if (isset($this->session->userdata['agentuser']['logged_in']) && $this->session->userdata['agentuser']['logged_in'] == TRUE) {
            $data['salecount'] = $this->Agent_model->getSalepropcount();
            $data['rentcount'] = $this->Agent_model->getRentpropcount();
            $data['soldHouse'] = $this->Agent_model->getSoldHouse();
            $data['saleCount']=$this->Lead_model->saleCount();
            $data['rentCount']=$this->Lead_model->rentCount();
            $data['valuationCount']=$this->Lead_model->valuationCount();
            $this->load->view('dashboard',$data);
        } else {

            $this->load->view('login');
        }
    }

}
