<?php

class Report_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function activitySale() {
        $agentid = $this->session->userdata['agentuser']['id'];
        $d=$this->input->post('date');
        $temp=  explode('-', $d);
        $date1=date('Y-m-d',strtotime($temp[0]));
        $date2=date('Y-m-d',strtotime($temp[1]));

        $query = $this->db->query("SELECT
  (SELECT COUNT(*) FROM pro_tbl_property WHERE lower(property_postalc) like lower('%HA1%') and property_type=1 and agent_id =$agentid and property_created_on between '$date1' and '$date2') as HA1, 
  (SELECT COUNT(*) FROM pro_tbl_property WHERE lower(property_postalc) like lower('%HA2%') and property_type=1 and agent_id =$agentid and property_created_on between '$date1' and '$date2') as HA2,
  (SELECT COUNT(*) FROM pro_tbl_property WHERE lower(property_postalc) like lower('%HA3%') and property_type=1 and agent_id =$agentid and property_created_on between '$date1' and '$date2') as HA3,
 (SELECT COUNT(*) FROM pro_tbl_property WHERE lower(property_postalc) like lower('%HA4%') and property_type=1 and agent_id =$agentid and property_created_on between '$date1' and '$date2') as HA4,
 (SELECT COUNT(*) FROM pro_tbl_property WHERE lower(property_postalc) like lower('%HA5%') and property_type=1 and agent_id =$agentid and property_created_on between '$date1' and '$date2') as HA5,
        (SELECT COUNT(*) FROM pro_tbl_property WHERE property_postalc not REGEXP '(HA1|HA2|HA3|HA4|HA5)' and property_type=1 and agent_id =$agentid and property_created_on between '$date1' and '$date2') as other ");
        return $query->result();
    }

    public function activityRent() {
        $agentid = $this->session->userdata['agentuser']['id'];
$d=$this->input->post('date');
        $temp=  explode('-', $d);
        $date1=date('Y-m-d',strtotime($temp[0]));
        $date2=date('Y-m-d',strtotime($temp[1]));
        $query = $this->db->query("SELECT
  (SELECT COUNT(*) FROM pro_tbl_property WHERE lower(property_postalc) like lower('%HA1%') and property_type=2 and agent_id =$agentid and property_created_on between '$date1' and '$date2') as HA1, 
  (SELECT COUNT(*) FROM pro_tbl_property WHERE lower(property_postalc) like lower('%HA2%') and property_type=2 and agent_id =$agentid and property_created_on between '$date1' and '$date2') as HA2,
  (SELECT COUNT(*) FROM pro_tbl_property WHERE lower(property_postalc) like lower('%HA3%') and property_type=2 and agent_id =$agentid and property_created_on between '$date1' and '$date2') as HA3,
 (SELECT COUNT(*) FROM pro_tbl_property WHERE lower(property_postalc) like lower('%HA4%') and property_type=2 and agent_id =$agentid and property_created_on between '$date1' and '$date2') as HA4,
 (SELECT COUNT(*) FROM pro_tbl_property WHERE lower(property_postalc) like lower('%HA5%') and property_type=2 and agent_id =$agentid and property_created_on between '$date1' and '$date2') as HA5,
        (SELECT COUNT(*) FROM pro_tbl_property WHERE property_postalc not REGEXP '(HA1|HA2|HA3|HA4|HA5)' and property_type=2 and agent_id =$agentid and property_created_on between '$date1' and '$date2') as other");
        return $query->result();
    }
    public function getsaleemailcount(){
        $agentid = $this->session->userdata['agentuser']['id'];
        $query=  $this->db->query("SELECT * from portal_tbl_contact_agent a join pro_tbl_property b on (a.property_id=b.property_id and b.property_type=1) where a.agent_id=$agentid");
        return $query->num_rows();
    }
     public function getrentemailcount(){
        $agentid = $this->session->userdata['agentuser']['id'];
        $query=  $this->db->query("SELECT * from portal_tbl_contact_agent a join pro_tbl_property b on (a.property_id=b.property_id and b.property_type=2) where a.agent_id=$agentid");
        return $query->num_rows();
    }

}
