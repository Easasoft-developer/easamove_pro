<?php

class Myaccount_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function getAgentDetail() {
        $agentid = $this->session->userdata['agentuser']['id'];
        $query = $this->db->query("SELECT * FROM easamoveadmin_tbl_agent where agent_id=$agentid");
        return $query->result();
    }

    //get agent images
    public function agentImage(){
        $agentid = $this->session->userdata['agentuser']['id'];
        $query = $this->db->query("SELECT * FROM pro_tbl_agentimage where agent_id=$agentid");
        return $query->result();
    }

    public function updateAgent($data, $ageng_id) {
        $this->db->where('agent_id', $ageng_id);
        $query = $this->db->update('easamoveadmin_tbl_agent', $data);

        if (!empty($_FILES['fileupload']['name'][0])) {
            //$agenImg = $this->db->query("DELETE FROM pro_tbl_agentimage WHERE agent_id='$ageng_id'");
            if ($this->upload_files($ageng_id, $_FILES['fileupload']) === FALSE) {
                //$data['error'] = $this->upload->display_errors('<div class="alert alert-danger">', '</div>');
                //$aff = $this->db->affected_rows();
                return 0;
            }else{
                return 1;
            }
        }else{
            return 1;
        }
    }

    //calling this function to move image and save in 'charity_gallery' 
    private function upload_files($title, $files)
    {
        $config = array(
            'upload_path'   => 'images/agentimage/',
            'allowed_types' => 'jpg|gif|png',
            'overwrite'     => 1,                       
        );

        $this->load->library('upload', $config);

        $images = array();
        $caption = $this->input->post('caption');

        foreach ($files['name'] as $key => $image) {
            $_FILES['fileupload[]']['name']= $files['name'][$key];
            $_FILES['fileupload[]']['type']= $files['type'][$key];
            $_FILES['fileupload[]']['tmp_name']= $files['tmp_name'][$key];
            $_FILES['fileupload[]']['error']= $files['error'][$key];
            $_FILES['fileupload[]']['size']= $files['size'][$key];

            $fileName = $title .'_'. $image;

            $images[] = $fileName;

            $config['file_name'] = $fileName;
            $imgName = $fileName;
            $this->upload->initialize($config);

            $capti = $caption[$key];

            if ($this->upload->do_upload('fileupload[]')) {
                $this->upload->data();
                //echo "how"."INSERT INTO `charity_gallery` (charity_id, image_name, created_on) VALUES ('$title', '$imgName', '$createdOn')";
                $query = $this->db->query("INSERT INTO `pro_tbl_agentimage` (agent_id, agent_image, agent_caption) VALUES ('$title', '$imgName', '$capti')");
            } else {
                return false;
            }
        }

        return true;
    }

    public function deleteImages(){
        $data = $this->input->post('id');
        $dataimg = $this->input->post('url');
        $query = $this->db->query("DELETE FROM pro_tbl_agentimage WHERE agentimg_id='$data'");
        if($query){
            $path = base_url()."images/agentimage/".$dataimg;
            unlink($path);
            return 1;
        }
    }

    public function updatepassword($pass){
        $userid = $this->session->userdata['agentuser']['useragentid'];
        $query = $this->db->query("UPDATE pro_tbl_agentuser SET agentuser_password = '$pass' WHERE agentuser_id='$userid'");
        if($query){
            return 1;
        }else{
            return 0;
        }
    }

    public function agentMemeber(){
        $query = $this->db->query("SELECT * FROM easamoveadmin_tbl_agentmembers where member_is_deleted=0");
        return $query->result();
    }

}
