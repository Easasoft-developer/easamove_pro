<?php

class Agent_model extends CI_Model {

    function __construct() {
        parent::__construct();
        
         
    }

//for country
    public function login($data) {
//        print_r($data);exit();
//        $query = $this->db->insert('easamoveadmin_tbl_country', $data);
        $query = $this->db->get_where('easamoveadmin_tbl_agent', $data);        
        $datas=$query->result();
        if(!empty($datas)){
             $session_array = array(
                'id' => $datas[0]->agent_id,
                'name' => $datas[0]->agent_name,
                'email' => $datas[0]->email_id,
                 'access' => 1,
                'logged_in' => TRUE,
                'usertype' => $datas[0]->agent_head_office
            );
            $this->session->set_userdata('agentuser', $session_array);
            return $datas;
        }else{
            $data=array(
                'agentuser_email'=>$data['email_id'],
                'agentuser_password'=>$data['agent_password'],
                'agentuser_is_deleted' => 0
            );
            $query = $this->db->get_where('pro_tbl_agentuser', $data);
            $datas1=$query->result();

            if(!empty($datas1)){
            $agentid=$datas1[0]->agent_id;
            $agentuserid = $datas1[0]->agentuser_id;
             $query1 = $this->db->query("SELECT * FROM easamoveadmin_tbl_agent where agent_id=$agentid AND agent_is_deleted=0");
         $res= $query1->result();
            $session_array = array(
                'id' => $res[0]->agent_id,
                'name' => $res[0]->agent_name,
                'email' => $res[0]->email_id,
                'access'=>$datas1[0]->agentuser_access,
                'logged_in' => TRUE,
                'usertype' => $res[0]->agent_head_office,
                'useragentid' => $agentuserid
            );
            $this->session->set_userdata('agentuser', $session_array);
             return $datas;
            }else{
                return false;
            }
        }
//       print_r($data);exit();
    }

    public function forgot_email($email) {
        //genertating random 5 letter string
        $pass = $this->generateRandomString(6);

        $query = $this->db->query("SELECT agentuser_name FROM pro_tbl_agentuser WHERE agentuser_email = '$email'");
        $row = $query->row();

        $this->db->query("update pro_tbl_agentuser set agentuser_passkey = '" . $pass . "' where agentuser_email='$email'");

        $this->load->library('email');        
        $config['protocol'] = "smtp";
        $config['smtp_host'] = "ssl://mail.easamove.co.uk";
        $config['smtp_port'] = "465";
        $config['smtp_user'] = "info@demo.easamove.co.uk"; 
        $config['smtp_pass'] = "wow123";
        $config['charset'] = "utf-8";
        $config['mailtype'] = "html";
        $config['newline'] = "\r\n";
        $this->email->initialize($config); 

        $mydata['email'] =$email;

        $mydata['link'] = base_url() . 'Account/resetpassword?user=' . $pass;
        $mydata['name'] = ucfirst($row->agentuser_name);

        $message = $this->load->view('mail_template/forgot_password', $mydata, true);
        //$message = $pass;   


         $this->email->from('info@demo.easamove.co.uk', 'Easapro reset password');
        $this->email->to($email);
        $this->email->subject('Request for forgot Password');
        $this->email->message($message);
        $this->email->send();
        return 1;
    }

    function generateRandomString($length) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function verify($key) {
        $query = $this->db->query("SELECT * from pro_tbl_agentuser where agentuser_is_deleted=0 and agentuser_verifykey='$key' AND agentuser_active=0");
        if ($query->num_rows() > 0) {
            $this->db->where('agentuser_verifykey', $key);
            $data = array(
                'agentuser_active' => 1
            );
            $this->db->update('pro_tbl_agentuser', $data);
            return $query->result();
        } else {
            return FALSE;
        }
    }
     public function resetPass(){
        $fkpss = $this->input->post('fkpassword');
        $newpass = $this->input->post('newpassword');
        if($fkpss != ''){
            $query = $this->db->query("UPDATE `pro_tbl_agentuser` SET agentuser_password = '$newpass' WHERE agentuser_passkey = '$fkpss'");
            return 1;
        }else{
            return 0;
        }
    }

    public function verifyagent($key) {
        $query = $this->db->query("SELECT * from easamoveadmin_tbl_agent where agent_is_deleted=0 and agent_key='$key'");
        if ($query->num_rows() > 0) {
            $this->db->where('agent_key', $key);
            $data = array(
                'agent_verified' => 1
            );
            $this->db->update('easamoveadmin_tbl_agent', $data);
            return $query->result();
        } else {
            return FALSE;
        }
    }

    public function getSitestatus() {
        $query = $this->db->query('SELECT * FROM easamoveadmin_tbl_sitestatus where sitestatus_is_deleted=0');
        return $query->result();
    }

    public function getListstatus() {
        $query = $this->db->query('SELECT * FROM easamoveadmin_tbl_listingstatus where listingstatus_is_deleted=0 and isforsale=1');
        return $query->result();
    }
    public function getListstatusr(){
          $query = $this->db->query('SELECT * FROM easamoveadmin_tbl_listingstatus where listingstatus_is_deleted=0 and isforsale=0');
        return $query->result();
    }

    public function getProptype() {
        $query = $this->db->query('SELECT * FROM easamoveadmin_tbl_propertytype where propertytype_is_deleted=0');
        return $query->result();
    }

    public function getTenures() {
        $query = $this->db->query('SELECT * FROM easamoveadmin_tbl_tenures where tenures_is_deleted=0');
        return $query->result();
    }

    public function getTranstype() {
        $query = $this->db->query('SELECT * FROM easamoveadmin_tbl_transactiontype where transactiontype_is_deleted=0');
        return $query->result();
    }

    public function getBuildcont() {
        $query = $this->db->query('SELECT * FROM easamoveadmin_tbl_buildcondition where buildcondition_is_deleted=0');
        return $query->result();
    }

    public function getLettype() {
        $query = $this->db->query('SELECT * FROM easamoveadmin_tbl_lettingtype where lettingtype_is_deleted=0');
        return $query->result();
    }

    public function getFeesapply() {
        $query = $this->db->query('SELECT * FROM easamoveadmin_tbl_feesapply');
        return $query->result();
    }

    public function getConsiderinfo() {
        $query = $this->db->query('SELECT * FROM easamoveadmin_tbl_considerinfo where considerinfo_is_deleted=0');
        return $query->result();
    }

    public function getOutspace() {
        $query = $this->db->query('SELECT * FROM easamoveadmin_tbl_outsidespace where outsidespace_is_deleted=0');
        return $query->result();
    }

    public function getParking() {
        $query = $this->db->query('SELECT * FROM easamoveadmin_tbl_parking where parking_is_deleted=0');
        return $query->result();
    }

    public function getHeatingtype() {
        $query = $this->db->query('SELECT * FROM easamoveadmin_tbl_heatingtype where heatingtype_is_deleted=0');
        return $query->result();
    }

    public function getSplfeature() {
        $query = $this->db->query('SELECT * FROM easamoveadmin_tbl_specialfeature where specialfeature_is_deleted=0');
        return $query->result();
    }

    public function getFurnishes() {
        $query = $this->db->query('SELECT * FROM easamoveadmin_tbl_furnishes where furnishes_is_deleted=0');
        return $query->result();
    }

    public function getAccessibility() {
        $query = $this->db->query('SELECT * FROM easamoveadmin_tbl_accessibility where accessibility_is_deleted=0');
        return $query->result();
    }

    public function getPricemod($id) {
        $query = $this->db->query("SELECT * FROM easamoveadmin_tbl_pricemodifier where pricemodifier_for=$id and pricemodifier_is_deleted=0");
        return $query->result();
    }

    public function addAgentuser($data) {
        $query = $this->db->insert('pro_tbl_agentuser', $data);
        return $this->db->affected_rows();
    }

    public function getContent() {
        $query = $this->db->query('SELECT * FROM easamoveadmin_tbl_content where content_is_deleted=0');
        return $query->result();
    }

    public function getAgentuser() {
$agentid = $this->session->userdata['agentuser']['id'];
        $row = $whr = $text = $whr1 = '';
        $row = $this->input->post('row');
        $text = $this->input->post('text');
        $offset = $this->input->post('offset');
        if ($offset == 1) {
            $offset = 0 * $row;
        } else {
            $offset = ($offset - 1) * $row;
        }
        if ($row != 0) {
            $whr = "limit $row";
        }
        if ($text != '' && $text != 'undefined') {
            $whr1 = "and Concat(agentuser_name, '', agentuser_email, '', agentuser_mobile,'',agentuser_userrole) like '%$text%'";
        }
//        echo "SELECT * FROM pro_tbl_agentuser where agentuser_is_deleted=0 $whr1 $whr OFFSET $offset";
        $query = $this->db->query("SELECT * FROM pro_tbl_agentuser where agentuser_is_deleted=0 and  agentuser_access!=1 and agent_id=$agentid $whr1 $whr OFFSET $offset");
        return $query->result();
    }

    public function updateAgentuser($data, $id) {
        $this->db->where('agentuser_id', $id);
        $query = $this->db->update('pro_tbl_agentuser', $data);
        return $query;
    }

    public function deleteUser($data, $id) {

        $this->db->where('agentuser_id', $id);
        $query = $this->db->update('pro_tbl_agentuser', $data);
        echo 1;
    }

    public function getUserdata($id) {

        $query = $this->db->query("SELECT * FROM pro_tbl_agentuser where agentuser_id=$id");
        return $query->result();
    }

    public function getUsercnt() {
        $agentid = $this->session->userdata['agentuser']['id'];
        $query = $this->db->query("SELECT * FROM pro_tbl_agentuser where agentuser_is_deleted=0 and agent_id=$agentid and agentuser_access!=1");
        return $query->num_rows();
    }

    public function getbranchcnt() {
//        $agentid = $this->session->userdata['agentuser']['id'];
        $id = $this->session->userdata['agentuser']['id'];
        $query = $this->db->query("SELECT * FROM  easamoveadmin_tbl_agent where agent_is_deleted=0 and agent_head_office=$id");
        return $query->num_rows();
    }

    public function getAgenttype() {

        $query = $this->db->query("SELECT * from easamoveadmin_tbl_agenttype where agent_is_deleted=0 order by agent_id desc");
        return $query->result();
    }

    public function addAgent($data) {
        $query = $this->db->insert('easamoveadmin_tbl_agent', $data);
        $key = $this->generateRandomString(16);
            $pass = $this->generateRandomString(8);
            $data = array(
                'agentuser_name' => $this->input->post('Forename'),
                'agentuser_email' => $this->input->post('Email'),
                'agentuser_password' => $pass,
                'agentuser_mobile' => $this->input->post('Phone'),
                'agentuser_userrole' => 'admin',
                'agentuser_access' => 1,
                'agent_id' => $this->db->insert_id(),
                'agentuser_created_on' => date('yy-mm-dd H:i:s'),
                'agentuser_created_by' => 1,
                'agentuser_verifykey' => $key
            );
            $query = $this->db->insert('pro_tbl_agentuser', $data);

            $this->load->library('email');        
            $config['protocol'] = "smtp";
            $config['smtp_host'] = "ssl://mail.easamove.co.uk";
            $config['smtp_port'] = "465";
            $config['smtp_user'] = "info@demo.easamove.co.uk"; 
            $config['smtp_pass'] = "wow123";
            $config['charset'] = "utf-8";
            $config['mailtype'] = "html";
            $config['newline'] = "\r\n";
            $this->email->initialize($config);
            $mydata['name'] = $this->input->post('Forename');

            $mydata['link'] = base_url() . 'settings/verify/' . $key;

            $message = $this->load->view('mail_template/welcome_message', $mydata, true);
            $this->email->from('info@demo.easamove.co.uk', 'Member in easapro');

            $this->email->to($this->input->post('Email'));

            $this->email->subject('Welcome to Easamove');

            $this->email->message($message);

            $this->email->send();
        return $query;
    }

    public function getAgent($id = 0) {
        $whr = '';
        if ($id != '' && $id != 0) {
            $whr = "and a.agent_id=$id";
        }
        $query = $this->db->query("SELECT a.*,b.agent_name as type FROM easamoveadmin_tbl_agent a left join easamoveadmin_tbl_agenttype b on (a.agent_type=b.agent_id) where a.agent_is_deleted=0 $whr");
        return $query->result();
    }

    public function updateAgent($data, $id) {
        $this->db->where('agent_id', $id);
        $query = $this->db->update('easamoveadmin_tbl_agent', $data);
        if($query){
            return 1;
        }else{
            return 0;
        }
        
    }

    public function deleteAgent($data, $id) {
        $aid = $this->session->userdata['agentuser']['id'];
        $this->db->where('agent_id', $id);
        $this->db->update('easamoveadmin_tbl_agent', $data);
        //echo $this->db->last_query();
        $getprop = $this->db->query("SELECT property_id FROM pro_tbl_property WHERE agent_id='$id'");
        foreach($getprop->result() as $delete){
            
            $prop_id = $delete->property_id;
            $propImg = array(
                'propimg_is_deleted' => 1,
                'propimg_modified_on' => date('yy-mm-dd H:i:s'),
                'propimg_modified_by' => $aid
            );
            $this->db->where('property_id', $prop_id);
            $this->db->update('pro_tbl_propimg', $propImg);

            //echo $this->db->last_query();



        }
        $prop = array(
            'property_is_deleted' => 1,
            'property_modified_on' => date('yy-mm-dd H:i:s'),
            'property_modified_by' => $aid
        );
        $this->db->where('agent_id', $id);
        $query = $this->db->update('pro_tbl_property', $prop);

        //echo $this->db->last_query();

        $agentusers = array(
            'agentuser_is_deleted' => 1,
            'agentuser_modified_by' => $aid,            
        );

        $this->db->where('agent_id', $id);
        $this->db->update('pro_tbl_agentuser', $agentusers);
        
        // echo $this->db->last_query();
        // exit();
        echo 1;
    }

    public function getAgentbranch() {
        $agentid=$this->session->userdata['agentuser']['id'];
        $aid = $this->session->userdata['agentuser']['id'];
        $row = $whr = $text = $whr1 = '';
        $row = $this->input->post('row');
        $text = $this->input->post('text');
        $offset = $this->input->post('offset');
        if ($offset == 1) {
            $offset = 0 * $row;
        } else {
            $offset = ($offset - 1) * $row;
        }
        if ($row != 0) {
            $whr = "limit $row";
        }
        if ($text != '' && $text != 'undefined') {
            $whr1 = "and Concat(agent_refno, '', agent_name, '', email_id,'',phone_no,'',post_code) like '%$text%'";
        }
//        echo "SELECT * FROM pro_tbl_agentuser where agentuser_is_deleted=0 $whr1 $whr OFFSET $offset";
        $query = $this->db->query("SELECT * FROM easamoveadmin_tbl_agent where agent_is_deleted=0 and agent_head_office='$aid' $whr1 $whr OFFSET $offset");
        return $query->result();
    }

    public function getAgentdata($id = 0) {
        $whr = '';
        if ($id != '' && $id != 0) {
            $whr = "and a.agent_id=$id";
        }
        $query = $this->db->query("SELECT a.*,b.agent_name as type FROM easamoveadmin_tbl_agent a left join easamoveadmin_tbl_agenttype b on (a.agent_type=b.agent_id) where a.agent_is_deleted=0 $whr");
        return $query->result();
    }

    public function getSalepropcount() {
        $sel = $this->input->post('sel');
        $whr='';
        if($sel!=''&&$sel!='0'){
            $whr="and lower(c.sitestatus_name)=lower('$sel')";
        }
        $agentid=$this->session->userdata['agentuser']['id'];
//        echo "SELECT * FROM  pro_tbl_property a left join easamoveadmin_tbl_sitestatus c on (a.property_pstatus=c.sitestatus_id) where property_is_deleted=0 and property_type=1 and agent_id=$agentid $whr";
         $query = $this->db->query("SELECT * FROM  pro_tbl_property a left join easamoveadmin_tbl_sitestatus c on (a.property_pstatus=c.sitestatus_id) where property_is_deleted=0 and property_type=1 and agent_id=$agentid $whr");
        return $query->num_rows();
    }

    public function getRentpropcount() {
          $sel = $this->input->post('sel');

        $whr='';
        if($sel!=''&&$sel!='0'){
            $whr="and lower(c.sitestatus_name)=lower('$sel')";
        }
        $agentid=$this->session->userdata['agentuser']['id'];
        $query = $this->db->query("SELECT * FROM  pro_tbl_property a left join easamoveadmin_tbl_sitestatus c on (a.property_pstatus=c.sitestatus_id) where property_is_deleted=0 and property_type=2 and agent_id=$agentid $whr");
        //echo $this->db->last_query();
        return $query->num_rows();
    }

    public function getBranch() {
        $aid = $this->session->userdata['agentuser']['id'];
        $query = $this->db->query("SELECT * FROM easamoveadmin_tbl_agent where agent_is_deleted=0 and agent_head_office='$aid'");
        return $query->result();
    }

    public function record_sale_count(){
        $aid = $this->session->userdata['agentuser']['id'];
        $sel = $this->input->post('status');
        $text = $this->input->post('search');
        $whr1=$whr2="";
        if ($sel != '' && $sel != 'All') {
            $whr1 = "and lower(c.sitestatus_name)=lower('$sel') ";
        }

        if ($text != '' && $text != 'undefined') {
            $whr2 = "and Concat(property_name, '', property_name, '', property_sname,'',property_city,'',property_ownrefer, '',b.propertytype_name) like '%$text%'";
        }
        $query = $this->db->query("SELECT *,(select count(*) from pro_tbl_propimg where property_id=a.property_id) as imgcnt,(select count(*) from pro_tbl_propcontent where property_id=a.property_id and propcontent_type='floor') as floorcnt,(select count(*) from pro_tbl_propcontent where property_id=a.property_id and propcontent_type='epc') as epc  FROM pro_tbl_property a 
left join easamoveadmin_tbl_propertytype b on (a.property_ptype=b.propertytype_id)
left join easamoveadmin_tbl_sitestatus c on (a.property_pstatus=c.sitestatus_id)
left join easamoveadmin_tbl_listingstatus d on (a.property_lstatus=d.listingstatus_id) where property_is_deleted=0 and property_type=1 and agent_id=$aid $whr1 $whr2");

        return $query->num_rows();
    }



    public function saleproperty($limit, $start){
        $aid = $this->session->userdata['agentuser']['id'];
        $sel = $this->input->post('status');
        $text = $this->input->post('search');
        $whr1=$whr2="";
        if ($sel != '' && $sel != 'All') {
            $whr1 = "and lower(c.sitestatus_name)=lower('$sel') ";
        }

        if ($text != '' && $text != 'undefined') {
            $whr2 = "and Concat(property_name, '', property_name, '', property_sname,'',property_city,'',property_ownrefer, '',b.propertytype_name) like '%$text%'";
        }
        $query = $this->db->query("SELECT a.*,b.*,c.*,d.*,e.propimg_s,(select count(*) from pro_tbl_propimg where property_id=a.property_id) as imgcnt,(select count(*) from pro_tbl_propcontent where property_id=a.property_id and propcontent_type='floor') as floorcnt,(select count(*) from pro_tbl_propcontent where property_id=a.property_id and propcontent_type='epc') as epc  FROM pro_tbl_property a 
left join easamoveadmin_tbl_propertytype b on (a.property_ptype=b.propertytype_id)
left join easamoveadmin_tbl_sitestatus c on (a.property_pstatus=c.sitestatus_id)
left join easamoveadmin_tbl_listingstatus d on (a.property_lstatus=d.listingstatus_id) 
left join pro_tbl_propimg e on (a.property_id=e.property_id) where property_is_deleted=0 and property_type=1 and agent_id=$aid $whr1 $whr2 group by a.property_id order by property_id desc");
        // echo $this->db->last_query();
        // exit();
        return $query->result();
    }


    //rent count
    public function record_rent_count(){
        $aid = $this->session->userdata['agentuser']['id'];
        $sel = $this->input->post('status');
        $text = $this->input->post('search');
        $whr1=$whr2="";
        if ($sel != '' && $sel != 'All') {
            $whr1 = "and lower(c.sitestatus_name)=lower('$sel') ";
        }

        if ($text != '' && $text != 'undefined') {
            $whr2 = "and Concat(property_name, '', property_name, '', property_sname,'',property_city,'',property_ownrefer, '',b.propertytype_name) like '%$text%'";
        }
        $query = $this->db->query("SELECT *,(select count(*) from pro_tbl_propimg where property_id=a.property_id) as imgcnt,(select count(*) from pro_tbl_propcontent where property_id=a.property_id and propcontent_type='floor') as floorcnt,(select count(*) from pro_tbl_propcontent where property_id=a.property_id and propcontent_type='epc') as epc  FROM pro_tbl_property a 
left join easamoveadmin_tbl_propertytype b on (a.property_ptype=b.propertytype_id)
left join easamoveadmin_tbl_sitestatus c on (a.property_pstatus=c.sitestatus_id)
left join easamoveadmin_tbl_listingstatus d on (a.property_lstatus=d.listingstatus_id) where property_is_deleted=0 and property_type=2 and agent_id=$aid $whr1 $whr2");
        return $query->num_rows();
    }



    public function rentproperty($limit, $start){
        $aid = $this->session->userdata['agentuser']['id'];
        $sel = $this->input->post('status');
        $text = $this->input->post('search');
        $whr1=$whr2="";
        if ($sel != '' && $sel != 'All') {
            $whr1 = "and lower(c.sitestatus_name)=lower('$sel') ";
        }

        if ($text != '' && $text != 'undefined') {
            $whr2 = "and Concat(property_name, '', property_name, '', property_sname,'',property_city,'',property_ownrefer, '',b.propertytype_name) like '%$text%'";
        }
        $query = $this->db->query("SELECT a.*,b.*,c.*,d.*,e.propimg_s,(select count(*) from pro_tbl_propimg where property_id=a.property_id) as imgcnt,(select count(*) from pro_tbl_propcontent where property_id=a.property_id and propcontent_type='floor') as floorcnt,(select count(*) from pro_tbl_propcontent where property_id=a.property_id and propcontent_type='epc') as epc  FROM pro_tbl_property a 
left join easamoveadmin_tbl_propertytype b on (a.property_ptype=b.propertytype_id)
left join easamoveadmin_tbl_sitestatus c on (a.property_pstatus=c.sitestatus_id)
left join easamoveadmin_tbl_listingstatus d on (a.property_lstatus=d.listingstatus_id) 
left join pro_tbl_propimg e on (a.property_id=e.property_id) where property_is_deleted=0 and  property_type=2 and agent_id=$aid $whr1 $whr2 group by a.property_id  order by property_id desc LIMIT $start,$limit");
        // echo $this->db->last_query();
        // exit();
        return $query->result();
    }

    //get count
    public function getUserCount(){
        $aid = $this->session->userdata['agentuser']['id'];
        $text = $this->input->post('search');
        $whr1="";

        if ($text != '' && $text != 'undefined') {
            $whr1 = "and Concat(agentuser_name, '', agentuser_email, '', agentuser_mobile,'',agentuser_userrole) like '%$text%'";
        }
        $query = $this->db->query("SELECT * FROM pro_tbl_agentuser where agentuser_is_deleted=0 and  agentuser_access!=1 and agent_id=$aid $whr1");
        return $query->num_rows();
    }

    //Get user details
    public function getUserDetails($limit, $start){
        $aid = $this->session->userdata['agentuser']['id'];
        $text = $this->input->post('search');
        $whr1="";

        if ($text != '' && $text != 'undefined') {
            $whr1 = "and Concat(agentuser_name, '', agentuser_email, '', agentuser_mobile,'',agentuser_userrole) like '%$text%'";
        }
        $query = $this->db->query("SELECT * FROM pro_tbl_agentuser where agentuser_is_deleted=0 and  agentuser_access!=1 and agent_id=$aid $whr1 LIMIT $start,$limit");
        return $query->result();
    }

    //agent brachs count
    public function agentBranchCount(){
        $aid = $this->session->userdata['agentuser']['id'];
        $text = $this->input->post('search');
        $whr1="";

        if ($text != '' && $text != 'undefined') {
            $whr1 = "and Concat(agent_refno, '', agent_name, '', email_id,'',phone_no,'',post_code) like '%$text%'";
        }
        $query = $this->db->query("SELECT * FROM easamoveadmin_tbl_agent where agent_is_deleted=0 and agent_head_office='$aid' $whr1");
        return $query->num_rows();
    }

    //agent details
    public function agentBranch($limit, $start){
        $aid = $this->session->userdata['agentuser']['id'];
        $text = $this->input->post('search');
        $whr1="";

        if ($text != '' && $text != 'undefined') {
            $whr1 = "and Concat(agent_refno, '', agent_name, '', email_id,'',phone_no,'',post_code) like '%$text%'";
        }
        $query = $this->db->query("SELECT * FROM easamoveadmin_tbl_agent where agent_is_deleted=0 and agent_head_office='$aid' $whr1 LIMIT $start,$limit");
        return $query->result();
    }

    //get sold house 
    public function getSoldHouse(){
        $aid = $this->session->userdata['agentuser']['id'];
        $query = $this->db->query("SELECT * FROM  pro_tbl_property where property_is_deleted=0 and property_type=4 and agent_id=$aid");
        return $query->num_rows();
    }

    public function getLeadcount(){
        $aid = $this->session->userdata['agentuser']['id'];
        $query = $this->db->query("SELECT count(*) FROM portal_tbl_contact_agent WHERE agent_id = '$aid'");
        return $query;
    }

}
