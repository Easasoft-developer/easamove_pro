<?php

class Lead_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    //search leads
    public function searchLeads(){
        $aid = $this->session->userdata['agentuser']['id'];
        $date=$this->input->post('date');
        $type=$this->input->post('type');
        $sept = explode("-", $date);
        $whr1=$whr2="";
        if($type == 'sale'){
            $whr1="AND b.property_lstatus in (5,6)";
        }else if($type=='rent'){
            $whr1="AND b.property_lstatus in (2,3)";
        }
        $datetrim1 = trim($sept[0]);
        $datetrim2 = trim($sept[1]);

        if($date!=""){
            if($sept[0]!="" && $sept[1]!=""){
                $whr2="AND a.created_on >= '$datetrim1' AND a.created_on <= '$datetrim2'";
            }
        }
        $query = $this->db->query("SELECT a.property_id,a.agent_id,a.user_name,a.user_email,a.user_phone,a.user_postcode,a.enquiry_status,a.user_message,a.user_status,a.created_on,b.property_name,b.property_sname,b.property_city,b.property_postalc,b.property_lstatus FROM  portal_tbl_contact_agent a LEFT JOIN pro_tbl_property b on (a.property_id=b.property_id) where a.agent_id=$aid AND a.property_id!=0 $whr1 $whr2");
        // echo $this->db->last_query();
        // exit();
        return $query->result();

    }

    //Get valuation details
    public function searchValuation(){
        $aid = $this->session->userdata['agentuser']['id'];
        $date=$this->input->post('date');
        $sept = explode("-", $date);
        $whr1=$whr2="";
        
        $datetrim1 = trim($sept[0]);
        $date1 = date('Y-m-d',strtotime($datetrim1));
        $datetrim2 = trim($sept[1]);
        $date2 = date('Y-m-d',strtotime($datetrim2));

        if($date!=""){
            if($sept[0]!="" && $sept[1]!=""){
                $whr2="AND created_on between '$date1' AND '$date2'";
            }
        }
        $query = $this->db->query("SELECT * FROM portal_tbl_agent_valuation WHERE FIND_IN_SET('$aid', appraisal_agent_id) $whr2");
        // echo $this->db->last_query();
        // exit();
        return $query->result();
    }

    //count of sale emails
    public function saleCount(){
        $aid = $this->session->userdata['agentuser']['id'];
        $query = $this->db->query("SELECT * FROM  portal_tbl_contact_agent a LEFT JOIN pro_tbl_property b on (a.property_id=b.property_id) where a.agent_id=$aid AND a.property_id!=0 AND b.property_lstatus in (5,6)");
        return $query->num_rows();
    }
    //count of rent emails
    public function rentCount(){
        $aid = $this->session->userdata['agentuser']['id'];
        $query = $this->db->query("SELECT * FROM  portal_tbl_contact_agent a LEFT JOIN pro_tbl_property b on (a.property_id=b.property_id) where a.agent_id=$aid AND a.property_id!=0 AND b.property_lstatus in (2,3)");
        return $query->num_rows();
    }
    //count of valuation emails
    public function valuationCount(){
        $aid = $this->session->userdata['agentuser']['id'];
        $query = $this->db->query("SELECT * FROM portal_tbl_agent_valuation WHERE FIND_IN_SET('$aid', appraisal_agent_id)");
        return $query->num_rows();
    }

    //letting information to get
    public function getAgentLettingInformation(){
        $aid = $this->session->userdata['agentuser']['id'];
        $query = $this->db->query("SELECT letting_information FROM easamoveadmin_tbl_agent WHERE agent_id=$aid");
        return $query->result();
    }

    public function updateLetting($data){
        $id = $this->session->userdata['agentuser']['id'];
        $this->db->where('agent_id', $id);
        $query = $this->db->update('easamoveadmin_tbl_agent', $data);
        if($query){
            return 1;
        } 
    }
    


}