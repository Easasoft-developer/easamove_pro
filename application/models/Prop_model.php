<?php

class Prop_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function addProp($data, $id) {
       
        $upload = $_FILES['fileupload'];
//        $contentupload = $_FILES['upload-file'];
        $config = array(
            'upload_path' => 'images/',
            'allowed_types' => 'jpg|gif|png|jpeg|pdf|JPEG|PNG|JPG|tiff|tif',
            'overwrite' => 1,
        );
        $this->load->library('upload', $config);
        $this->load->library('image_lib');

        if ($id == 0) {
            $query = $this->db->insert('pro_tbl_property', $data);
            $propid = $this->db->insert_id();
        } else {
// print_r($_POST);exit();
            //delete the old data in property image and content and special features

//            $propeditimg = $this->input->post('propimg');
//            $this->db->select('propimg_id');
//            $this->db->where('property_id', $id);
//            $q = $this->db->get('pro_tbl_propimg');
//            $datas = $q->result();
//            $imarr = array();
//            foreach ($datas as $d) {
//                array_push($imarr, $d->propimg_id);
//            }
//            //array diffrents for image delete
//            if (!empty($propeditimg)) {
//                $diff = array_diff($imarr, $propeditimg);
//                if (!empty($diff)) {
//                    $this->db->where_in('propimg_l', $diff);
//                    $this->db->delete('pro_tbl_propimg');
//                }
//            }
            $temp=array('0');
          $fid = $this->input->post('floorplanold');
          $bid = $this->input->post('broucherold');
          $vid = $this->input->post('virtualold'); 
          $eid = $this->input->post('epcold'); 

          if(!empty($fid)){
            $newArray=array_merge($fid,$temp);
          } if (!empty ($bid)) {
               $newArray= array_merge($bid,$newArray);
            } if (!empty ($vid)) {
                $newArray= array_merge($vid,$newArray);
            } if (!empty ($eid)) {
                $newArray=array_merge($eid,$newArray);
            }

            //delete the features
//            $this->db->where_not_in('propcontent_id', $ignore);
            $this->db->where('property_id', $id);
            $this->db->delete('pro_tbl_keyfeature');
            //delete the contents
//            print_r($temp);
//            print_r($newArray);exit();
            $this->db->where_not_in('propcontent_id', $newArray);
            $this->db->where('property_id', $id);
            $this->db->delete('pro_tbl_propcontent');
//            echo $this->db->last_query();exit();
            //update the property
            
            $this->db->where('property_id', $id);
            $query = $this->db->update('pro_tbl_property', $data);
            $propid = $id;
        }
        //feature adding
        $featArray = array();
        $feattitle = $this->input->post('key-feattitle');
        $featdesc = $this->input->post('key-featdesc');
//        print_r($feattitle);
        if (!empty($feattitle) && $feattitle != '') {

            foreach ($feattitle as $key => $title) {
                if ($title != '') {
                    $keyfeature = array(
                        'keyfeature_title' => $title,
                        'property_id' => $propid,
                        'keyfeature_created_on' => date('Y-m-d H:i:s'),
                        'keyfeature_created_by' => 1
                    );
                    array_push($featArray, $keyfeature);
                }
            }

            if (!empty($featArray)) {
                $result1 = $this->db->insert_batch('pro_tbl_keyfeature', $featArray);
            }
        }

        //content adding
        $contArray = array();
        
        $doc = $this->input->post('upload-doc');
        $udesc = $this->input->post('upload-desc');




        $flink = $this->input->post('floorplan-link');
        $blink = $this->input->post('broucher-link');
        $vlink = $this->input->post('vitural-link');
        if (!empty($flink)) {
            foreach ($flink as $key => $ftitle) {
                $uploadcontent = array(
                    'propcontent_type' => 'floor',
                    'propcontent_desc' => $ftitle,
                    'propcontent_image' => '',
                    'propcontent_link'=>1,
                    'property_id' => $propid,
                    'propcontent_created_on' => date('Y-m-d H:i:s'),
                    'propcontent_created_by' => 1
                );
                array_push($contArray, $uploadcontent);
            }
        }
        if (!empty($blink)) {
            foreach ($blink as $key => $btitle) {
                $uploadcontent = array(
                    'propcontent_type' => 'broucher',
                    'propcontent_desc' => $btitle,
                    'propcontent_image' => '',
                    'propcontent_link'=>1,
                    'property_id' => $propid,
                    'propcontent_created_on' => date('Y-m-d H:i:s'),
                    'propcontent_created_by' => 1
                );
                array_push($contArray, $uploadcontent);
            }
        }
        if (!empty($vlink)) {
            foreach ($vlink as $key => $vtitle) {
                $uploadcontent = array(
                    'propcontent_type' => 'virtual',
                    'propcontent_desc' => $vtitle,
                    'propcontent_image' => '',
                    'propcontent_link'=>1,
                    'property_id' => $propid,
                    'propcontent_created_on' => date('Y-m-d H:i:s'),
                    'propcontent_created_by' => 1
                );
                array_push($contArray, $uploadcontent);
            }
        }
        
        $repl=array(' ',',','_');
        if (!empty($_FILES['floorplan-file'])) {
           
            $flupload = $_FILES['floorplan-file'];
             
            foreach ($flupload['name'] as $key => $vtitle) {
                if($vtitle!=''){
                $_FILES['floorplan-file[]']['name'] = $_FILES['floorplan-file']['name'][$key];
                $_FILES['floorplan-file[]']['type'] = $_FILES['floorplan-file']['type'][$key];
                $_FILES['floorplan-file[]']['tmp_name'] = $_FILES['floorplan-file']['tmp_name'][$key];
                $_FILES['floorplan-file[]']['error'] = $_FILES['floorplan-file']['error'][$key][$key];
                $_FILES['floorplan-file[]']['size'] = $_FILES['floorplan-file']['size'][$key];
                $fileName1 = 'floorplan_'.$propid.'_'.str_replace(' ', '_', $vtitle);
                
                $config['file_name'] = $fileName1;
                $imgName = 'images/' . $fileName1;
                $this->upload->initialize($config);
                $this->upload->do_upload();

                if ($this->upload->do_upload('floorplan-file[]')) {
                    $image_data = $this->upload->data();
                }
                $uploadcontent = array(
                    'propcontent_type' => 'floor',
                    'propcontent_desc' => '',
                    'propcontent_image' => $fileName1,
                    'property_id' => $propid,
                    'propcontent_link'=>0,
                    'propcontent_created_on' => date('Y-m-d H:i:s'),
                    'propcontent_created_by' => 1
                );
                
                array_push($contArray, $uploadcontent);
                }
            }
          
        }
//        print_r($contArray);exit();
        if (!empty($_FILES['broucher-file'])) {
            $brupload = $_FILES['broucher-file'];
            foreach ($brupload['name'] as $key => $vtitle) {
                if($vtitle!=''){
                $_FILES['broucher-file[]']['name'] = $_FILES['broucher-file']['name'][$key];
                $_FILES['broucher-file[]']['type'] = $_FILES['broucher-file']['type'][$key];
                $_FILES['broucher-file[]']['tmp_name'] = $_FILES['broucher-file']['tmp_name'][$key];
                $_FILES['broucher-file[]']['error'] = $_FILES['broucher-file']['error'][$key];
                $_FILES['broucher-file[]']['size'] = $_FILES['broucher-file']['size'][$key];
                $fileName2 = 'broc_'.$propid.'_'.str_replace(' ', '_', $vtitle);

                $config['file_name'] = $fileName2;
                $imgName = 'images/' . $fileName2;
                $this->upload->initialize($config);
                $this->upload->do_upload();

                if ($this->upload->do_upload('broucher-file[]')) {
                    $image_data = $this->upload->data();
                }
                $uploadcontent = array(
                    'propcontent_type' => 'broucher',
                    'propcontent_desc' => '',
                    'propcontent_image' => $fileName2,
                    'property_id' => $propid,
                    'propcontent_link'=>0,
                    'propcontent_created_on' => date('Y-m-d H:i:s'),
                    'propcontent_created_by' => 1
                );
                array_push($contArray, $uploadcontent);
//                print_r($uploadcontent);
                }
            }
        }
//        print_r($contArray);
        if (!empty($_FILES['epc-file'])) {
             $epcupload = $_FILES['epc-file'];
            foreach ($epcupload['name'] as $key => $vtitle) {
                if($vtitle!=''){
                $_FILES['epc-file[]']['name'] = $_FILES['epc-file']['name'][$key];
                $_FILES['epc-file[]']['type'] = $_FILES['epc-file']['type'][$key];
                $_FILES['epc-file[]']['tmp_name'] = $_FILES['epc-file']['tmp_name'][$key];
                $_FILES['epc-file[]']['error'] = $_FILES['epc-file']['error'][$key];
                $_FILES['epc-file[]']['size'] = $_FILES['epc-file']['size'][$key];
                $fileName3 = 'epc_'.$propid.'_'.str_replace(' ', '_', $vtitle);

                $config['file_name'] = $fileName3;
                $imgName = 'images/' . $fileName3;
                $this->upload->initialize($config);
                $this->upload->do_upload();

                if ($this->upload->do_upload('epc-file[]')) {
                    $image_data = $this->upload->data();
                }
                $uploadcontent = array(
                    'propcontent_type' => 'epc',
                    'propcontent_desc' => '',
                    'propcontent_image' => $fileName3,
                    'property_id' => $propid,
                    'propcontent_link'=>0,
                    'propcontent_created_on' => date('Y-m-d H:i:s'),
                    'propcontent_created_by' => 1
                );
                array_push($contArray, $uploadcontent);
                }
            }
        }
        if (!empty($contArray)) {
//            print_r($contArray);exit();
            $result = $this->db->insert_batch('pro_tbl_propcontent', $contArray);
        }
        $this->db->where_in('property_id', $propid);
         $this->db->delete('pro_tbl_propimg');
        $imgArray=array();
        $propimg = $this->input->post('propimg');
        $propimgcapt = $this->input->post('caption');
        if (!empty($propimg)) {
            foreach ($propimg as $key => $btitle) {
                $repl=array(' ','-',':');
                $btitle= str_replace($repl, '_', $btitle);
                $bigimage = $btitle;
                $smallimage = 'thumb_' . $btitle;
                $mediumimage = 'medium_' . $btitle;
                $images = array(
                    'property_id' => $propid,
                    'propimg_s' => $smallimage,
                    'propimg_m' => $mediumimage,
                    'propimg_l' => $bigimage,
                    'propimg_caption' => $propimgcapt[$key],
                    'propimg_created_on' => date('Y-m-d H:i:s'),
                    'propimg_created_by' => 1
                );
                array_push($imgArray, $images);
            }
            $result = $this->db->insert_batch('pro_tbl_propimg', $imgArray);
        }
        $files = scandir("images/tempimg/");
// Identify directories
            $source = "images/tempimg/";
            $destination = "images/mainimg/";
// Cycle through all source files
            foreach ($files as $file) {
                if (in_array($file, array(".", "..")))
                    continue;
                // If we copied this successfully, mark it for deletion
                if (copy($source . $file, $destination . $file)) {
                    $delete[] = $source . $file;
                }
            }
// Delete all successfully-copied files
            foreach ($delete as $file) {
                unlink($file);
            }
        return true;

    }

    public function getProperty() {
        $agentid = $this->session->userdata['agentuser']['id'];
        $row = $whr = $text = $whr1 = $whr2 = '';
        $row = $this->input->post('row');
        $text = $this->input->post('text');
        $offset = $this->input->post('offset');
        $sel = $this->input->post('sel');
        if ($offset == 1) {
            $offset = 0 * $row;
        } else {
            $offset = ($offset - 1) * $row;
        }
        if ($row != 0) {
            $whr = "limit $row";
        }
        if ($sel != '0') {
            $whr2 = "and lower(c.sitestatus_name)=lower('$sel') ";
        }
        if ($text != '' && $text != 'undefined') {
            $whr1 = "and Concat(property_name, '', property_name, '', property_sname,'',property_city,'',property_ownrefer) like '%$text%'";
        }


        $query = $this->db->query("SELECT *,(select count(*) from pro_tbl_propimg where property_id=a.property_id) as imgcnt,(select count(*) from pro_tbl_propcontent where property_id=a.property_id and propcontent_type='floor') as floorcnt,(select count(*) from pro_tbl_propcontent where property_id=a.property_id and propcontent_type='epc') as epc  FROM pro_tbl_property a 
left join easamoveadmin_tbl_propertytype b on (a.property_ptype=b.propertytype_id)
left join easamoveadmin_tbl_sitestatus c on (a.property_pstatus=c.sitestatus_id)
left join easamoveadmin_tbl_listingstatus d on (a.property_lstatus=d.listingstatus_id) where property_is_deleted=0 and property_type=1 and agent_id=$agentid $whr2 $whr1 $whr OFFSET $offset");
        return $query->result();
    }

    public function getRentProperty() {
        $agentid = $this->session->userdata['agentuser']['id'];
        $row = $whr = $text = $whr1 = $whr2 = '';
        $row = $this->input->post('row');
        $text = $this->input->post('text');
        $offset = $this->input->post('offset');
        $sel = $this->input->post('sel');
        if ($offset == 1) {
            $offset = 0 * $row;
        } else {
            $offset = ($offset - 1) * $row;
        }
        if ($row != 0) {
            $whr = "limit $row";
        }
        if ($sel != '0') {
            $whr2 = "and lower(c.sitestatus_name)=lower('$sel') ";
        }
        if ($text != '' && $text != 'undefined') {
            $whr1 = "and Concat(property_name, '', property_name, '', property_sname,'',property_city,'',property_ownrefer) like '%$text%'";
        }


        $query = $this->db->query("SELECT *,(select count(*) from pro_tbl_propimg where property_id=a.property_id) as imgcnt,(select count(*) from pro_tbl_propcontent where property_id=a.property_id and propcontent_type='floor') as floorcnt,(select count(*) from pro_tbl_propcontent where property_id=a.property_id and propcontent_type='epc') as epc FROM pro_tbl_property a 
left join easamoveadmin_tbl_propertytype b on (a.property_ptype=b.propertytype_id)
left join easamoveadmin_tbl_sitestatus c on (a.property_pstatus=c.sitestatus_id)
left join easamoveadmin_tbl_listingstatus d on (a.property_lstatus=d.listingstatus_id) where property_is_deleted=0 and property_type=2 and agent_id=$agentid $whr2 $whr1 $whr  OFFSET $offset");
        return $query->result();
    }

    public function getPropertybyid($id) {
        $query = $this->db->query("SELECT * from pro_tbl_property where property_is_deleted=0 and property_id=$id");
        return $query->result();
    }

    public function getKeyfeature($id) {
        $query = $this->db->query("SELECT * from pro_tbl_keyfeature where property_id=$id");
        return $query->result();
    }

    public function getImages($id) {
        $query = $this->db->query("SELECT * from pro_tbl_propimg where property_id=$id");
        return $query->result();
    }

    public function getContents($id) {
        $query = $this->db->query("SELECT * from pro_tbl_propcontent where property_id=$id");
        return $query->result();
    }

    public function getAllProperty() {
        $agentid = $this->session->userdata['agentuser']['id'];
        $query = $this->db->query("SELECT * from pro_tbl_property where agent_id=$agentid order by property_id desc limit 10");
        return $query->result();
    }

    public function getmarker($pid) {
        $agentid = $this->session->userdata['agentuser']['id'];
        $query = $this->db->query("SELECT property_id,property_name,property_sname,property_lat,property_long,(select propimg_m from pro_tbl_propimg where property_id=a.property_id limit 1) as property_image from pro_tbl_property as a where property_type=$pid and agent_id=$agentid");
        return $query->result();
    }

}



